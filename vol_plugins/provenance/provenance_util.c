/*
 * test.c
 *
 *  Created on: Nov 14, 2018
 *      Author: tonglin
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct entry_sample{
    char time[64];
    char user_name[20];
    char data_array[20];
    int pid;
    int tid;
    char func_name[20];
    unsigned long duration;
}record;

int record_append(FILE* f, void* rec_ptr, unsigned int rec_size){
    fwrite(rec_ptr, rec_size, 1, f);

    return -1;
}

int record_read_next(FILE* f, void* rec_ptr_out, unsigned int rec_size){
    record tmp;
    fread(&tmp, rec_size, 1, f);//rec_ptr_out
    printf("record_read_next(): num = %d, str = %s\n", tmp.num, tmp.data_array);
    return -1;
}

int record_read_all(FILE* f, void** rec_arrary_ptr){
    return -1;
}
// read all recs, read next, write all rec, append a rec to a file
int main(int argc, char*argv[]){
    record e1, e2;
    e1.num = 1;

    e1.data_array[0] = 't';
    e1.data_array[1] = 'e';
    e1.data_array[2] = 's';
    e1.data_array[3] = 't';

    printf("1: sizeof(entry) = %lu, sizeof(e1) = %lu, sizeof(int) = %lu, sizeof(char*) = %lu\n", sizeof(record), sizeof(e1), sizeof(int), sizeof(char*));
//
//    printf("2: sizeof(entry) = %lu, sizeof(e1) = %lu\n", sizeof(record), sizeof(e1));
//
//    printf("3: sizeof(entry) = %lu, sizeof(e1) = %lu\n", sizeof(record), sizeof(e1));


    FILE *fwp, *frp;
    fwp = fopen("./test_data.dat", "ab");
//    fwrite(&e1, sizeof(e1), 1, fwp);
//    fclose(fwp);

//    frp = fopen("./test_data.dat", "rb");
//    fread(&e2,sizeof(struct record),1,frp);
//    printf("e2.a = %d, e2.data_array = %s\n", e2.num,  e2.data_array );

    int rec_num = 50;
    record rec_list_1[rec_num]; //write
    record rec_list_2[rec_num];//read
    char* str = "attached data.";

    //fill records
    for(int i = 0; i < rec_num; i++){
        rec_list_1[i].num = i;
        memset(rec_list_1[i].data_array, '\0', 20);
        memcpy(rec_list_1[i].data_array, str, strlen(str));
    }

    //write records
    for(int i = 0; i < rec_num; i++){
        record_append(fwp, (void*)&(rec_list_1[i]), sizeof(record));
    }
    fclose(fwp);

    frp = fopen("./test_data.dat", "rb");

    for(int i = 0; i < rec_num; i++){
        memset(rec_list_2[i].data_array, '\0', 20);
        record_read_next(frp, (void*)&(rec_list_2[i]), sizeof(record));
        printf("Read record i = %d: e.num = %d, str = %s \n ", i, rec_list_2[i].num, rec_list_2[i].data_array);
    }

    fclose(frp);
    return 0;
}
