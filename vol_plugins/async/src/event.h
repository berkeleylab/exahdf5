#ifndef  ASYNC_EVENT_H
#define ASYNC_EVENT_H

#define EVENT_INITIAL_ALLOC 64

typedef enum H5ES_status_t {
    H5ES_STATUS_IN_PROGRESS,
    H5ES_STATUS_SUCCEED,
    H5ES_STATUS_FAIL,
    H5ES_STATUS_CANCEL
}
typedef struct event_set_t {
    int n_event;
    int n_alloc;
    void **events;
} event_set_t;
/*
 * Signature:      hid_t H5EScreate(void)
 * Purpose:        Create a new, empty, event set
 * Return:         The ID of a new event set on success, H5I_INVALID_ID on failure
 * Description:    H5EScreate creates an event set to manage and monitor the events  
 *                 associated  with asynchronous operation execution. Multiple event 
 *                 sets can be created and used concurrently in a program. Events 
 *                 added to an event set are tracked together. The identifier for an 
 *                 event set is used in asynchronous function calls, which add the 
 *                 event associated with the functions execution into the event set. 
 *                 The order in which asynchronous functions execute and complete is 
 *                 independent of their associated events location in an event set.
 */
hid_t  H5EScreate(void);

/*
 * Signature:      herr_t H5ESget_count(hid_t es_id, unsigned *count)
 * Purpose:        Query the number of outstanding asynchronous operations in an 
 *                 event set
 * Return:         0 on success, <0 on failure
 * Description:    H5ESget_count retrieves the number of events in the event set 
 *                 specified by es_id.
 */
herr_t H5ESget_count(hid_t es_id, unsigned *count);


herr_t H5EStest(hid_t es_id, H5ES_status_t *status);

herr_t H5ESwait(hid_t es_id, H5ES_status_t *status);

herr_t H5EScancel(hid_t es_id, H5ES_status_t *status);

herr_t H5ESclose(hid_t es_id);


extern H5ESget_event_status(void *event);
extern H5ESwait_event(void *event);
#endif
