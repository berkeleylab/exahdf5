#include <stdio.h>
#include <stdlib.h>

#include "hdf5.h"

H5I_type_t event_type_g = H5I_BADID;
int        n_es_g       = 0;

static herr_t 
event_type_free(void* type_id)
{
    if (NULL != type_id) 
        free(type_id);
    else
        return -1;
    return 0;
}

hid_t
H5EScreate(void)
{
    hid_t es_id;
    event_set_t *event_set;

    /* Register event type only once */
    if (H5I_BADID == event_type_g) {
        if ((event_type_g = H5Iregister_type(EVENT_INITIAL_ALLOC, 0, event_type_free)) < 0) {
            fprintf(stderr, "%s: error with H5Iregister_type\n", __func__);
            return -1;
        }
    }

    if ((event_set = (event_set_t*)calloc(1, sizeof(event_set_t))) == NULL) {
        fprintf(stderr, "%s: error with event set memory alloc\n", __func__);
        return -1;
    }
    event_set->n_alloc = EVENT_INITIAL_ALLOC;
    event_set->events  = (void**)calloc(event_set->n_alloc, sizeof(void*));
    
    if((es_id = H5Iregister(event_type_g, event_set)) < 0) {
        fprintf(stderr, "%s: error with H5Iregister\n", __func__);
        return es_id;
    }
    n_es_g++;

    return es_id;
}

herr_t
H5ESget_count(hid_t es_id, unsigned *count)
{
    if (NULL == count) {
        fprintf(stderr, "%s: error with input, count is NULL\n", __func__);
        return -1;
    }

    event_set_t *event_set = H5Iobject_verify(es_id, event_type_g);
    if (NULL == event_set) {
        fprintf(stderr, "%s: error with H5Iobject_verify\n", __func__);
        return -1;
    }

    *count = event_set->n_event;
    return 0;
}

herr_t
H5EStest(hid_t es_id, H5ES_status_t *status)
{
    int i;
    if (NULL == status) {
        fprintf(stderr, "%s: error with input, status is NULL\n", __func__);
        return -1;
    }

    event_set_t *event_set = H5Iobject_verify(es_id, event_type_g);
    if (NULL == event_set) {
        fprintf(stderr, "%s: error with H5Iobject_verify\n", __func__);
        return -1;
    }

    for (i = 0; i < event_set->n_event; i++) {
        /* need async test function */
    }

}

herr_t
H5ESwait(hid_t es_id, H5ES_status_t *status)
{
    if (NULL == status) {
        fprintf(stderr, "%s: error with input, status is NULL\n", __func__);
        return -1;
    }

    event_set_t *event_set = H5Iobject_verify(es_id, event_type_g);
    if (NULL == event_set) {
        fprintf(stderr, "%s: error with H5Iobject_verify\n", __func__);
        return -1;
    }


}

herr_t
H5EScancel(hid_t es_id, H5ES_status_t *status)
{

}

herr_t
H5ESclose(hid_t es_id)
{
    H5ES_status_t status;

    if (H5EStest(es_id, &status) < ) {
        fprintf(stderr, "%s: error with H5EStest\n", __func__);
        return -1;
    }

    if (H5ES_STATUS_SUCCEED != status) 
        return -1;

    if (H5Idec_ref(es_id) < 0) {
        fprintf(stderr, "%s: error with H5Idec_ref\n", __func__);
        return -1;
    }
    n_es_g--;
    if (0 == n_es_g) {
        if (H5Idestroy_type(event_type_g) < 0) {
            fprintf(stderr, "%s: error with H5Idestroy_type\n", __func__);
            return -1;
        }
        event_type_g = H5I_BADID;
    }

    return 0;
}
