typedef struct async_attr_create_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    type_id;
    hid_t                    space_id;
    hid_t                    acpl_id;
    hid_t                    aapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_create_args_t;

typedef struct async_attr_open_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    aapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_open_args_t;

typedef struct async_attr_read_args_t {
    void                     *attr;
    hid_t                    mem_type_id;
    void                     *buf;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_read_args_t;

typedef struct async_attr_write_args_t {
    void                     *attr;
    hid_t                    mem_type_id;
    void                     *buf;
    hid_t                    dxpl_id;
    void                     **req;
    hbool_t                   buf_free;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_write_args_t;

typedef struct async_attr_get_args_t {
    void                     *obj;
    H5VL_attr_get_t          get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_get_args_t;

typedef struct async_attr_specific_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5VL_attr_specific_t     specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_specific_args_t;

typedef struct async_attr_optional_args_t {
    void                     *obj;
    H5VL_attr_optional_t     opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_optional_args_t;

typedef struct async_attr_close_args_t {
    void                     *attr;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_attr_close_args_t;

typedef struct async_dataset_create_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    lcpl_id;
    hid_t                    type_id;
    hid_t                    space_id;
    hid_t                    dcpl_id;
    hid_t                    dapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_create_args_t;

typedef struct async_dataset_open_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    dapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_open_args_t;

typedef struct async_dataset_read_args_t {
    void                     *dset;
    hid_t                    mem_type_id;
    hid_t                    mem_space_id;
    hid_t                    file_space_id;
    hid_t                    plist_id;
    void                     *buf;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_read_args_t;

typedef struct async_dataset_write_args_t {
    void                     *dset;
    hid_t                    mem_type_id;
    hid_t                    mem_space_id;
    hid_t                    file_space_id;
    hid_t                    plist_id;
    void                     *buf;
    void                     **req;
    hbool_t                   buf_free;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_write_args_t;

typedef struct async_dataset_get_args_t {
    void                     *dset;
    H5VL_dataset_get_t       get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_get_args_t;

typedef struct async_dataset_specific_args_t {
    void                     *obj;
    H5VL_dataset_specific_t  specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_specific_args_t;

typedef struct async_dataset_optional_args_t {
    void                     *obj;
    H5VL_dataset_optional_t  opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_optional_args_t;

typedef struct async_dataset_close_args_t {
    void                     *dset;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_dataset_close_args_t;

typedef struct async_datatype_commit_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    type_id;
    hid_t                    lcpl_id;
    hid_t                    tcpl_id;
    hid_t                    tapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_commit_args_t;

typedef struct async_datatype_open_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    tapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_open_args_t;

typedef struct async_datatype_get_args_t {
    void                     *dt;
    H5VL_datatype_get_t      get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_get_args_t;

typedef struct async_datatype_specific_args_t {
    void                     *obj;
    H5VL_datatype_specific_t specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_specific_args_t;

typedef struct async_datatype_optional_args_t {
    void                     *obj;
    H5VL_datatype_optional_t opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_optional_args_t;

typedef struct async_datatype_close_args_t {
    void                     *dt;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_datatype_close_args_t;

typedef struct async_file_create_args_t {
    char                     *name;
    unsigned                 flags;
    hid_t                    fcpl_id;
    hid_t                    fapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_create_args_t;

typedef struct async_file_open_args_t {
    char                     *name;
    unsigned                 flags;
    hid_t                    fapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_open_args_t;

typedef struct async_file_get_args_t {
    void                     *file;
    H5VL_file_get_t          get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_get_args_t;

typedef struct async_file_specific_args_t {
    void                     *file;
    H5VL_file_specific_t     specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_specific_args_t;

typedef struct async_file_optional_args_t {
    void                     *file;
    H5VL_file_optional_t     opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_optional_args_t;

typedef struct async_file_close_args_t {
    void                     *file;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_file_close_args_t;

typedef struct async_group_create_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    lcpl_id;
    hid_t                    gcpl_id;
    hid_t                    gapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_create_args_t;

typedef struct async_group_open_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    char                     *name;
    hid_t                    gapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_open_args_t;

typedef struct async_group_get_args_t {
    void                     *obj;
    H5VL_group_get_t         get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_get_args_t;

typedef struct async_group_specific_args_t {
    void                     *obj;
    H5VL_group_specific_t    specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_specific_args_t;

typedef struct async_group_optional_args_t {
    void                     *obj;
    H5VL_group_optional_t    opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_optional_args_t;

typedef struct async_group_close_args_t {
    void                     *grp;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_group_close_args_t;

typedef struct async_link_create_args_t {
    H5VL_link_create_type_t  create_type;
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    hid_t                    lcpl_id;
    hid_t                    lapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_create_args_t;

typedef struct async_link_copy_args_t {
    void                     *src_obj;
    H5VL_loc_params_t        *loc_params1;
    void                     *dst_obj;
    H5VL_loc_params_t        *loc_params2;
    hid_t                    lcpl_id;
    hid_t                    lapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_copy_args_t;

typedef struct async_link_move_args_t {
    void                     *src_obj;
    H5VL_loc_params_t        *loc_params1;
    void                     *dst_obj;
    H5VL_loc_params_t        *loc_params2;
    hid_t                    lcpl_id;
    hid_t                    lapl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_move_args_t;

typedef struct async_link_get_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5VL_link_get_t          get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_get_args_t;

typedef struct async_link_specific_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5VL_link_specific_t     specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_specific_args_t;

typedef struct async_link_optional_args_t {
    void                     *obj;
    H5VL_link_optional_t     opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_link_optional_args_t;

typedef struct async_object_open_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5I_type_t               *opened_type;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_object_open_args_t;

typedef struct async_object_copy_args_t {
    void                     *src_obj;
    H5VL_loc_params_t        *src_loc_params;
    char                     *src_name;
    void                     *dst_obj;
    H5VL_loc_params_t        *dst_loc_params;
    char                     *dst_name;
    hid_t                    ocpypl_id;
    hid_t                    lcpl_id;
    hid_t                    dxpl_id;
    void                     **req;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_object_copy_args_t;

typedef struct async_object_get_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5VL_object_get_t        get_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_object_get_args_t;

typedef struct async_object_specific_args_t {
    void                     *obj;
    H5VL_loc_params_t        *loc_params;
    H5VL_object_specific_t   specific_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_object_specific_args_t;

typedef struct async_object_optional_args_t {
    void                     *obj;
    H5VL_object_optional_t   opt_type;
    hid_t                    dxpl_id;
    void                     **req;
    va_list                  arguments;
    #ifdef ENABLE_TIMING
    struct timeval create_time;
    struct timeval start_time;
    #endif
} async_object_optional_args_t;

