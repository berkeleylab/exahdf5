/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 * Purpose:	The public header file for the pass-through VOL connector.
 */

#ifndef _H5VLpassthru_H
#define _H5VLpassthru_H

/* Public headers needed by this file */
#include "H5VLpublic.h"        /* Virtual Object Layer                 */

/* Identifier for the pass-through VOL connector */
#define H5VL_PASSTHRU	(H5VL_pass_through_register())

/* Characteristics of the pass-through VOL connector */
#define H5VL_PASSTHRU_NAME        "pass_through"
#define H5VL_PASSTHRU_VALUE       707           /* VOL connector ID */
#define H5VL_PASSTHRU_VERSION     0
/* ====INSERT==== 0_include.h */

/* Pass-through VOL connector info */
typedef struct H5VL_pass_through_info_t {
    hid_t under_vol_id;         /* VOL ID for under VOL */
    void *under_vol_info;       /* VOL info for under VOL */
} H5VL_pass_through_info_t;

typedef void * H5RQ_token_t;

#ifdef __cplusplus
extern "C" {
#endif

H5_DLL hid_t H5VL_pass_through_register(void);

herr_t H5Pset_vol_async(hid_t fapl_id);
herr_t H5Pget_dxpl_async(hid_t dxpl, hbool_t *is_async);
herr_t H5Pset_dxpl_async(hid_t dxpl, hbool_t is_async);
herr_t H5Pget_dxpl_async_cp_limit(hid_t dxpl, hsize_t *size);
herr_t H5Pset_dxpl_async_cp_limit(hid_t dxpl, hsize_t size);
herr_t H5Dwait(hid_t dset);
herr_t H5Fwait(hid_t file);
void   H5VLasync_waitall();
void   H5VLasync_finalize();

/* Experimental token APIs */
herr_t H5RQ_token_check(H5RQ_token_t token, int *status);
herr_t H5RQ_token_wait(H5RQ_token_t token);
herr_t H5RQ_token_free(H5RQ_token_t token);
/* H5RQ_token_t *H5RQ_new_token(void); */

#ifdef __cplusplus
}
#endif

#endif /* _H5VLpassthru_H */

