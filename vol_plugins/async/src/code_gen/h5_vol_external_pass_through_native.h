#ifndef H5_VOL_EXTERNAL_PASS_THROUGH_NATIVE_H
#define H5_VOL_EXTERNAL_PASS_THROUGH_NATIVE_H

typedef struct H5VL_pass_through_info_t {
    hid_t under_vol_id;         /* VOL ID for under VOL */
    void *under_vol_info;       /* VOL info for under VOL */
} H5VL_pass_through_info_t;


hid_t  register_passthrough_vol(void);
herr_t H5Pset_vol_passthrough(hid_t fapl_id, int n_thread);
herr_t H5VLasync_init(hid_t vipl_id);
herr_t H5VLasync_term(void);
#endif
