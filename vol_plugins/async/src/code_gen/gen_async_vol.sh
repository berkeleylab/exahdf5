#!/bin/bash

make

TEMPLATE=H5VLpassthru
TARGETC=h5_vol_external_async_native.c
TARGETH=h5_vol_external_async_native.h

# Step 1 - Copy file and rename to async
cp ${TEMPLATE}.c  $TARGETC
cp ${TEMPLATE}.h  $TARGETH

sed -i "s/PASS_THRU/ASYNC/g" $TARGETH
sed -i "s/PASS THRU/ASYNC/g" $TARGETH
sed -i "s/PASSTHRU/ASYNC/g"  $TARGETH
sed -i "s/pass_thru/async/g" $TARGETH
sed -i "s/pass through/async/g" $TARGETH
sed -i "s/pass-through/async/g" $TARGETH
sed -i "s/passthru/async/g"  $TARGETH

sed -i "s/PASS_THRU/ASYNC/g" $TARGETC
sed -i "s/PASS THRU/ASYNC/g" $TARGETC
sed -i "s/PASSTHRU/ASYNC/g"  $TARGETC
sed -i "s/pass through/async/g" $TARGETC
sed -i "s/Pass through/async/g" $TARGETC
sed -i "s/pass-through/async/g" $TARGETC
sed -i "s/pass_thru/async/g" $TARGETC
sed -i "s/passthru/async/g"  $TARGETC


sed -i "s/PASS_THROUGH/ASYNC/g" $TARGETH
sed -i "s/PASS THROUGH/ASYNC/g" $TARGETH
sed -i "s/pass_through/async/g" $TARGETH
sed -i "s/passthrough/async/g"  $TARGETH

sed -i "s/PASS_THROUGH/ASYNC/g" $TARGETC
sed -i "s/PASS THROUGH/ASYNC/g" $TARGETC
sed -i "s/pass_through/async/g" $TARGETC
sed -i "s/passthrough/async/g"  $TARGETC

sed -i "s/\"H5VLasync/\"h5_vol_external_async_native/g"  $TARGETC


# Step 2 - Generate Argobots functions, structures, and headers: 
#          abt_gen_out.c, abt_gen_out.h, and abt_gen_out_def.h
./abt_gen.exe func_input.txt


# Step 3 - Find and delete existing functions that are to be replaced
del_start=$(awk '/====DELETE==== START1/{print NR}' $TARGETC)
del_end=$(awk '/====DELETE==== END1/{print NR}' $TARGETC)
sed -i "$del_start, ${del_end}d" $TARGETC

del_start=$(awk '/====DELETE==== START2/{print NR}' $TARGETC)
del_end=$(awk '/====DELETE==== END2/{print NR}' $TARGETC)
sed -i "$del_start, ${del_end}d" $TARGETC


# Step 4 - Insert code snippets to place holders
sed -i -e '/====INSERT==== 0_include.c/r 0_include.c'               $TARGETC 
sed -i -e '/====INSERT==== 0_include.h/r 0_include.h'               $TARGETH 
sed -i -e '/====INSERT==== 1_struct_def.h/r 1_struct_def.h'         $TARGETC
sed -i -e '/====INSERT==== out_abt_gen_def.h/r out_abt_gen_def.h'   $TARGETC 
sed -i -e '/====INSERT==== 2_global_var.h/r 2_global_var.h'         $TARGETC
sed -i -e '/====INSERT==== 3_abt_func.c/r 3_abt_func.c'             $TARGETC
sed -i -e '/====INSERT==== out_abt_gen.c/r out_abt_gen.c'           $TARGETC


# Step 5 - Replace native routines with async routines
NEWTARGETC=${TARGETC}_new.c
./abt_replace_func.exe $TARGETC
# cp $TARGETC $NEWTARGETC  # leave this only if previous line is commented

# Step 6 - Remove place holder lines
sed -i '/====INSERT====/d' $NEWTARGETC
sed -i '/====DELETE====/d' $NEWTARGETC
sed -i '/REPLACE_START/d'  $NEWTARGETC
sed -i '/====INSERT====/d' $TARGETH


# Step 7 - Copy to upper directory
cp ${NEWTARGETC} ../$TARGETC
cp $TARGETH ../
