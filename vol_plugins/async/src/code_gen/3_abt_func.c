H5PL_type_t H5PLget_plugin_type(void) {return H5PL_TYPE_VOL;}
const void *H5PLget_plugin_info(void) {return &H5VL_async_g;}
    
static herr_t 
async_init(hid_t vipl_id)
{
    herr_t ret_val = 1;
    int abt_ret;

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] Success with async vol registration\n");
#endif

    /* Only init argobots once */
    if (ABT_SUCCESS != ABT_initialized()) {
        abt_ret = ABT_init(0, NULL);
        if (ABT_SUCCESS != abt_ret) {
            fprintf(stderr, "  [ASYNC VOL ERROR] with Argobots init\n");
            ret_val = -1;
            goto done;
        }
        #ifdef ENABLE_LOG
        else
            fprintf(stderr, "  [ASYNC VOL DBG] Success with Argobots init\n");
        #endif

        /* Create a mutex for argobots I/O instance */
        abt_ret = ABT_mutex_create(&async_instance_mutex_g);
        if (ABT_SUCCESS != abt_ret) {
            fprintf(stderr, "  [ASYNC VOL ERROR] with mutex create\n");
            ret_val = -1;
            goto done;
        }
    }

done:
    return ret_val;
}

herr_t 
async_instance_finalize(void)
{
    int abt_ret, ret_val = 1;
    int i;

    if (NULL == async_instance_g) 
        return 0;

    abt_ret = ABT_mutex_lock(async_instance_mutex_g);
    if (abt_ret != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_mutex_lock\n");
        ret_val = -1;
    }

    abt_ret = ABT_mutex_free(&async_instance_g->qhead.head_mutex);
    if (ABT_SUCCESS != abt_ret) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with head_mutex free\n", __func__);
        ret_val = -1;
    }

    if (async_instance_g->num_xstreams) {
        for (i = 0; i < async_instance_g->num_xstreams; i++) {
            ABT_xstream_join(async_instance_g->xstreams[i]);
            ABT_xstream_free(&async_instance_g->xstreams[i]);
        }
        free(async_instance_g->xstreams);
        /* Pool gets implicitly freed */
    }

    free(async_instance_g);
    async_instance_g = NULL;

    abt_ret = ABT_mutex_unlock(async_instance_mutex_g);
    if (abt_ret != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_mutex_unlock\n");
        ret_val = -1;
    }

#ifdef ENABLE_LOG
    fprintf(stderr, "  [ASYNC VOL DBG] Success with async_instance_finalize\n");
#endif

    return ret_val;
} // End async_instance_finalize

static herr_t 
async_term(void)
{
    herr_t ret_val = 1;
    int    abt_ret;

    ret_val = async_instance_finalize();
    if (ret_val < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] with async_instance_finalize\n");
        return -1;
    }

    /* Free the mutex */
    if (async_instance_mutex_g) {
        abt_ret = ABT_mutex_free(&async_instance_mutex_g);
        if (ABT_SUCCESS != abt_ret) {
            fprintf(stderr, "  [ASYNC VOL ERROR] with mutex free\n");
            ret_val = -1;
            goto done;
        }
        async_instance_mutex_g = NULL;
    }

    /* abt_ret = ABT_finalize(); */
    /* if (ABT_SUCCESS != abt_ret) { */
    /*     fprintf(stderr, "  [ASYNC VOL ERROR] with finalize argobots\n"); */
    /*     ret_val = -1; */
    /*     goto done; */
    /* } */
    /* #ifdef ENABLE_DBG_MSG */
    /* else */
    /*     fprintf(stderr, "  [ASYNC VOL DBG] Success with Argobots finalize\n"); */
    /* #endif */

done:
    return ret_val;
}

/* Init Argobots for async IO */
herr_t 
async_instance_init(int backing_thread_count)
{
    herr_t hg_ret = 0;
    async_instance_t *aid;
    ABT_pool pool;
    ABT_xstream self_xstream;
    ABT_xstream *progress_xstreams = NULL;
    ABT_sched *progress_scheds = NULL;
    int abt_ret, i;
    const char *env_var;

    if (backing_thread_count < 0) return -1;

    if (NULL != async_instance_g) 
        return 1;

    async_init(H5P_DEFAULT);

    #ifdef ENABLE_DBG_MSG
        fprintf(stderr, "  [ASYNC VOL DBG] Init Argobots with %d threads\n", backing_thread_count);
    #endif
    
    /* Use mutex to guarentee there is only one Argobots IO instance (singleton) */
    abt_ret = ABT_mutex_lock(async_instance_mutex_g);
    if (abt_ret != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_mutex_lock\n");
        return -1;
    }

    aid = (async_instance_t*)calloc(1, sizeof(*aid));
    if (aid == NULL) { hg_ret = -1; goto done; }

    abt_ret = ABT_mutex_create(&aid->qhead.head_mutex);
    if (ABT_SUCCESS != abt_ret) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with head_mutex create\n", __func__);
        free(aid);
        return -1;
    }

    if (backing_thread_count == 0) {
        aid->num_xstreams = 0;
        abt_ret = ABT_xstream_self(&self_xstream);
        if (abt_ret != ABT_SUCCESS) { free(aid); hg_ret = -1; goto done; }
        abt_ret = ABT_xstream_get_main_pools(self_xstream, 1, &pool);
        if (abt_ret != ABT_SUCCESS) { free(aid); hg_ret = -1; goto done; }
    }
    else {
        progress_xstreams = (ABT_xstream *)calloc(backing_thread_count, sizeof(ABT_xstream));
        if (progress_xstreams == NULL) {
            free(aid);
            hg_ret = -1;
            goto done;
        }

        progress_scheds = (ABT_sched*)calloc(backing_thread_count, sizeof(ABT_sched));
        if (progress_scheds == NULL) {
            free(progress_xstreams);
            free(aid);
            hg_ret = -1;
            goto done;
        }

        /* All xstreams share one pool */
        abt_ret = ABT_pool_create_basic(ABT_POOL_FIFO_WAIT, ABT_POOL_ACCESS_MPMC, ABT_TRUE, &pool);
        if(abt_ret != ABT_SUCCESS) {
            free(progress_xstreams);
            free(progress_scheds);
            free(aid);
            hg_ret = -1;
            goto done;
        }

        for(i = 0; i < backing_thread_count; i++) {
            /* abt_ret = ABT_sched_create_basic(ABT_SCHED_BASIC, 1, &pool, */
            abt_ret = ABT_sched_create_basic(ABT_SCHED_BASIC_WAIT, 1, &pool,
               ABT_SCHED_CONFIG_NULL, &progress_scheds[i]);
            if (abt_ret != ABT_SUCCESS) {
                free(progress_xstreams);
                free(progress_scheds);
                free(aid);
                hg_ret = -1;
                goto done;
            }
            abt_ret = ABT_xstream_create(progress_scheds[i], &progress_xstreams[i]);
            if (abt_ret != ABT_SUCCESS) {
                free(progress_xstreams);
                free(progress_scheds);
                free(aid);
                hg_ret = -1;
                goto done;
            }
        } // end for
    } // end else

    aid->pool         = pool;
    aid->xstreams     = progress_xstreams;
    aid->num_xstreams = backing_thread_count;
    aid->nfopen       = 0;
    aid->env_async    = false;
    aid->ex_delay     = true;
    aid->ex_fclose    = true;
    aid->ex_gclose    = false;
    aid->ex_dclose    = false;
    aid->start_abt_push = false;

    env_var = getenv("HDF5_VOL_CONNECTOR");
    if (env_var && *env_var && strstr(env_var, "async") != NULL) {
        aid->ex_delay  = true;
        aid->env_async = true;
    }

    // Default start at fclose
    env_var = getenv("HDF5_ASYNC_EXE_FCLOSE");
    if (env_var && *env_var && atoi(env_var) > 0 )  {
        aid->ex_delay  = true;
        aid->ex_fclose = true;
    }

    env_var = getenv("HDF5_ASYNC_EXE_GCLOSE");
    if (env_var && *env_var && atoi(env_var) > 0 )  {
        aid->ex_delay  = true;
        aid->ex_gclose = true;
    }

    env_var = getenv("HDF5_ASYNC_EXE_DCLOSE");
    if (env_var && *env_var && atoi(env_var) > 0 )  {
        aid->ex_delay  = true;
        aid->ex_dclose = true;
    }

    async_instance_g = aid;

done:
    abt_ret = ABT_mutex_unlock(async_instance_mutex_g);
    if (abt_ret != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_mutex_unlock\n");
        if (progress_xstreams) free(progress_xstreams);
        if (progress_scheds)   free(progress_scheds);
        if (aid)               free(aid);
        return -1;
    }

#ifdef ENABLE_DBG_MSG
    fprintf(stderr, "  [ASYNC VOL DBG] Success with async_instance_init\n");
#endif
    return hg_ret;
} // End async_instance_init

hid_t
H5VL_async_register(void)
{
    int n_thread = ASYNC_VOL_DEFAULT_NTHREAD;
    /* Initialize the Argobots I/O instance */
    if (NULL == async_instance_g) {
        if (async_instance_init(n_thread) < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] with async_instance_init\n");
            return -1;
        }
    }

    /* Singleton register the pass-through VOL connector ID */
    if(H5I_VOL != H5Iget_type(H5VL_ASYNC_g)) {
        H5VL_ASYNC_g = H5VLregister_connector(&H5VL_async_g, H5P_DEFAULT);
        if (H5VL_ASYNC_g <= 0) {
            fprintf(stderr, "  [ASYNC VOL ERROR] with H5VLregister_connector\n");
            return -1;
        }
    }

    return H5VL_ASYNC_g;
} 
    
static herr_t 
H5VL_async_init(hid_t vipl_id)
{
    vipl_id = vipl_id;

    /* Initialize the Argobots I/O instance */
    int n_thread = ASYNC_VOL_DEFAULT_NTHREAD;
    if (NULL == async_instance_g) {
        if (async_instance_init(n_thread) < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] with async_instance_init\n");
            return -1;
        }
    }

    return 0;
}

herr_t 
H5Pset_vol_async(hid_t fapl_id)
{
    H5VL_async_info_t async_vol_info;
    hid_t under_vol_id;
    void *under_vol_info;
    herr_t status;
    /* int n_thread = ASYNC_VOL_DEFAULT_NTHREAD; */

    /* /1* Initialize the Argobots I/O instance *1/ */
    /* if (NULL == async_instance_g) { */
    /*     if (async_instance_init(n_thread) < 0) { */
    /*         fprintf(stderr,"  [ASYNC VOL ERROR] with async_instance_init\n"); */
    /*         return -1; */
    /*     } */
    /* } */

    if (H5VLis_connector_registered_by_name(H5VL_ASYNC_NAME) == 0) {
        if (H5VL_async_register() < 0) {
            fprintf(stderr, "  [ASYNC VOL ERROR] H5Pset_vol_async: H5VL_async_register\n");
            goto done;
        }
    }

    status = H5Pget_vol_id(fapl_id, &under_vol_id);
    assert(status >= 0);
    assert(under_vol_id > 0);

    status = H5Pget_vol_info(fapl_id, &under_vol_info);
    assert(status >= 0);

    async_vol_info.under_vol_id   = under_vol_id;
    async_vol_info.under_vol_info = under_vol_info;
    
    if (H5Pset_vol(fapl_id, H5VL_ASYNC_g, &async_vol_info) < 0) {
        fprintf(stderr, "  [ASYNC VOL ERROR] with H5Pset_vol\n");
        goto done;
    }

done:
    return 1;
}


static herr_t 
H5VL_async_term(void)
{
    herr_t ret_val = 0;

#ifdef ENABLE_LOG 
    fprintf(stderr,"  [ASYNC VOL LOG] ASYNC VOL terminate\n");
#endif
    H5VLasync_finalize();

    async_term();
    H5VL_ASYNC_g = H5I_INVALID_HID;

    return ret_val;
}

static void 
free_async_task(async_task_t *task)
{
    if (ABT_mutex_free(&task->task_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_free\n", __func__);
        return;
    }

    if (task->token) {
        task->token->task = NULL;
    }

    if (task->args) free(task->args);

    if (ABT_eventual_free(&task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_eventual_free\n", __func__);
        return;
    }
    /* if (task->children) free(task->children); */
    if (task->dep_tasks) free(task->dep_tasks);

    return;
}

/* // Debug only */
/* static void */
/* check_tasks_object_valid(H5VL_async_t *async_obj) */
/* { */
/*     async_task_t *task_iter, *tmp; */
/*     assert(async_obj); */

/*     if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__); */
/*         return; */
/*     } */

/*     DL_FOREACH_SAFE2(async_obj->file_task_list_head, task_iter, tmp, file_list_next) { */
/*         if (task_iter->async_obj->magic != ASYNC_MAGIC) { */
/*             printf("Error with magic number\n"); */
/*         } */
/*     } */

/*     if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__); */
/*         return; */
/*     } */
/* } */


/* static void */
/* remove_tasks_of_closed_object(H5VL_async_t *async_obj) */
/* { */
/*     async_task_t *task_iter, *tmp; */
/*     assert(async_obj); */

/*     if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__); */
/*         return; */
/*     } */

/*     DL_FOREACH_SAFE2(async_obj->file_async_obj->file_task_list_head, task_iter, tmp, file_list_next) { */
/*         if (task_iter->async_obj == async_obj && task_iter->is_done == 1) { */
/*             DL_DELETE2(async_obj->file_async_obj->file_task_list_head, task_iter, file_list_prev, file_list_next); */
/*             free_async_task(task_iter); */
/*             free(task_iter); */
/*         } */
/*     } */

/*     if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__); */
/*         return; */
/*     } */
/* } */


/* static void */
/* free_file_async_obj(H5VL_async_t *file) */
/* { */
/*     async_task_t *task_iter, *tmp; */

/*     assert(file); */

/*     if (ABT_mutex_lock(file->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__); */
/*         return; */
/*     } */

/*     DL_FOREACH_SAFE2(file->file_async_obj->file_task_list_head, task_iter, tmp, file_list_next) { */
/*         DL_DELETE2(file->file_async_obj->file_task_list_head, task_iter, file_list_prev, file_list_next); */
/*         free_async_task(task_iter); */
/*         free(task_iter); */
/*     } */

/*     if (ABT_mutex_unlock(file->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__); */
/*         return; */
/*     } */

/*     if (ABT_mutex_free(&file->obj_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_free\n", __func__); */
/*         return; */
/*     } */
/*     if (ABT_mutex_free(&file->file_task_list_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_free\n", __func__); */
/*         return; */
/*     } */
/*     free(file); */
/* } */

static herr_t
add_to_dep_task(async_task_t *task, async_task_t *dep_task)
{
    assert(task);
    assert(dep_task);

    if (task->n_dep_alloc == 0 || task->dep_tasks == NULL) {
        // Initial alloc
        task->dep_tasks = (async_task_t**)calloc(ALLOC_INITIAL_SIZE, sizeof(async_task_t*));
        if (NULL == task->dep_tasks) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s calloc failed\n", __func__);
            return -1;
        }
        task->n_dep_alloc  = ALLOC_INITIAL_SIZE;
        task->n_dep        = 0;
    }
    else if (task->n_dep == task->n_dep_alloc) {
        // Need to expand alloc
        task->dep_tasks = (async_task_t**)realloc(task->dep_tasks, task->n_dep_alloc * 2 * sizeof(async_task_t*));
        if (task->dep_tasks== NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s realloc failed\n", __func__);
            return -1;
        }
        task->n_dep_alloc *= 2;
    }

    task->dep_tasks[task->n_dep] = dep_task;
    task->n_dep++;

    return 1;
}

static herr_t
push_task_to_abt_pool(async_qhead_t *qhead, ABT_pool pool)
{
    int               i, is_dep_done;
    /* ABT_task_state    task_state; */
    ABT_thread_state  thread_state;
    /* ABT_thread        my_thread; */
    async_task_t      *task_elt, *task_tmp, *task_list;
    /* async_task_list_t *tmp; */

    assert(qhead);

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] entering %s \n", __func__);
    #endif

    if (ABT_mutex_lock(qhead->head_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        return -1;
    }

    if (NULL == qhead->queue) 
        goto done;

    task_list = qhead->queue->task_list;
    DL_FOREACH_SAFE(task_list, task_elt, task_tmp) {
        is_dep_done = 1;
        if (qhead->queue->type  == DEPENDENT) {
            // Check if depenent tasks are finished
            for (i = 0; i < task_elt->n_dep; i++) {
                if (NULL != task_elt->dep_tasks[i]->abt_thread) {
                    /* ABT_thread_self(&my_thread); */
                    /* if (task_elt->dep_tasks[i]->abt_thread == my_thread) { */
                    /*     continue; */
                    /* } */
                    if (ABT_thread_get_state(task_elt->dep_tasks[i]->abt_thread, &thread_state) != ABT_SUCCESS) {
                        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_thread_get_state\n", __func__);
                        return -1;
                    }
                    /* if (ABT_task_get_state(task_elt->dep_tasks[i]->abt_task, &task_state) != ABT_SUCCESS) { */
                    /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_task_get_state\n", __func__); */
                    /*     return -1; */
                    /* } */
                    if (thread_state != ABT_THREAD_STATE_TERMINATED && thread_state != ABT_THREAD_STATE_RUNNING) {
                        is_dep_done = 0;
                        break;
                    }
                }
            }
        }

        if (is_dep_done == 0) 
            continue;

        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] push task [%p] to Argobots pool\n", task_elt->func);
        #endif

        if (ABT_thread_create(pool, task_elt->func, task_elt, ABT_THREAD_ATTR_NULL, &task_elt->abt_thread) != ABT_SUCCESS) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_thread_create failed for %p\n", __func__, task_elt->func);
            break;
        }
        /* if (ABT_task_create(pool, task_elt->func, task_elt, &task_elt->abt_task) != ABT_SUCCESS) { */
        /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_task_create failed for %p\n", __func__, task_elt->func); */
        /*     break; */
        /* } */
        task_elt->in_abt_pool = 1;

        DL_DELETE(qhead->queue->task_list, task_elt);
        task_elt->prev = NULL;
        task_elt->next = NULL;
        break;
    }

    // Remove head if all its tasks have been pushed to Argobots pool
    if (qhead->queue->task_list == NULL) {
        /* tmp = qhead->queue; */
        DL_DELETE(qhead->queue, qhead->queue);
        /* qhead->queue->prev = qhead->queue->next->prev; */
        /* qhead->queue = qhead->queue->next; */
        /* free(tmp); */
    }

done:
    if (ABT_mutex_unlock(qhead->head_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        return -1;
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

    return 1;
} // End push_task_to_abt_pool

int get_n_running_task_in_queue(async_task_t *task)
{
    int remaining_task = 0;
    ABT_thread_state thread_state;
    async_task_t *task_elt;

    /* if (ABT_mutex_lock(task->async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
    /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__); */
    /*     return -1; */
    /* } */

    DL_FOREACH2(task->async_obj->file_task_list_head, task_elt, file_list_next) {
        if (task_elt->abt_thread != NULL) {
            ABT_thread_get_state(task_elt->abt_thread, &thread_state);
            if (thread_state == ABT_THREAD_STATE_RUNNING){
                remaining_task++;
            }
        }
    }

    /* if (ABT_mutex_unlock(task->async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) { */
    /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__); */
    /*     return -1; */
    /* } */

    return remaining_task;
}


int get_n_running_task_in_queue_obj(H5VL_async_t *async_obj)
{
    int remaining_task = 0;
    ABT_thread_state thread_state;
    async_task_t *task_elt;

    DL_FOREACH2(async_obj->file_task_list_head, task_elt, file_list_next) {
        if (task_elt->abt_thread != NULL) {
            ABT_thread_get_state(task_elt->abt_thread, &thread_state);
            if (thread_state == ABT_THREAD_STATE_RUNNING){
                remaining_task++;
            }
        }
    }

    return remaining_task;
}

/* 
 * Any read/write operation must be executed after a prior write operation of same object.
 * Any write operation must be executed after a prior read operation of same object.
 * Any collective operation must be executed in same order with regards to other collective operations.
 * There can only be 1 collective operation in execution at any time (amongst all the threads on a process).
 */
static herr_t
add_task_to_queue(async_qhead_t *qhead, async_task_t *task, task_list_qtype task_type)
{
    int is_end, is_end2;
    /* int is_dep = 0; */
    async_task_list_t *tail_list, *task_list_elt;
    async_task_t      *task_elt, *tail_task;

    assert(qhead);
    assert(task);

    /* 1. Check for collective operation
     *       If collective, create a new CTL, or append it to an existing tail CTL.
     * 2. Check for object dependency.
     *       E.g. a group open depends on its file open/create.
     *       (Rule 1) Any read/write operation depends on a prior write operation of same object.
     *       (Rule 2) Any write operation depends on a prior read operation of same object.
     *       If satisfies any of the above, create a new DTL and insert to it.
     * 3. If the async task is not the above 2, create or insert it to tail RTL.
     *       If current RTL is also head, add to Argobots pool.
     * 4. Any time an async task has completed, push all tasks in the head into Argobots pool.  
     */

    tail_list = qhead->queue == NULL ? NULL : qhead->queue->prev;

    // Need to depend on the object's createion (create/open) task to finish
    if (task_type != COLLECTIVE) {
        if (task->parent_obj && task->parent_obj->is_obj_valid != 1) {
            /* is_dep = 1; */
            task_type = DEPENDENT;
            if (add_to_dep_task(task, task->parent_obj->create_task) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s add_to_dep_task failed\n", __func__);
                return -1;
            }
        }

        if (task != task->async_obj->create_task && task->async_obj->is_obj_valid != 1) {
            /* is_dep = 1; */
            task_type = DEPENDENT;
            if (add_to_dep_task(task, task->async_obj->create_task) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s add_to_dep_task failed\n", __func__);
                return -1;
            }
        }

        /* Any read/write operation must be executed after a prior write operation of same object. */
        /* Any write operation must be executed after a prior read operation of same object. */
        is_end = 0;
        DL_FOREACH2(tail_list, task_list_elt, prev) {
            tail_task = task_list_elt->task_list == NULL ? NULL : task_list_elt->task_list->prev;
            is_end2 = 0;
            DL_FOREACH2(tail_task, task_elt, prev) {
                if (task_elt->async_obj && task_elt->async_obj == task->async_obj && 
                        !(task->op == READ && task_elt->op == READ)) {
                    task_type = DEPENDENT;
                    if (add_to_dep_task(task, task_elt) < 0) {
                        fprintf(stderr,"  [ASYNC VOL ERROR] %s add_to_dep_task failed\n", __func__);
                        return -1;
                    }
                    /* is_dep = 1; */
                    /* break; */
                }
                if (is_end2 == 1 && tail_task == task_elt) 
                    break;
                is_end2 = 1;
            }
            if (is_end == 1 && tail_list == task_list_elt) 
                break;
            is_end = 1;
            /* if (is_dep == 1) { break; } */
        }

        if (ABT_mutex_lock(task->async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
            return -1;
        }
        DL_FOREACH2(task->async_obj->file_task_list_head, task_elt, file_list_next) {
            if (task_elt->in_abt_pool == 1 && task_elt->async_obj && task_elt->async_obj == task->async_obj && 
                    !(task->op == READ && task_elt->op == READ)) {
                task_type = DEPENDENT;
                if (add_to_dep_task(task, task_elt) < 0) {
                    fprintf(stderr,"  [ASYNC VOL ERROR] %s add_to_dep_task failed\n", __func__);
                    return -1;
                }
            }
        }

        if (ABT_mutex_unlock(task->async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
            return -1;
        }
    }

    /* // If regular task, add to Argobots pool for execution directly */
    /* if (task_type == REGULAR) { */
    /*     if (ABT_thread_create(*(task->async_obj->pool_ptr), task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) { */
    /*         fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func); */
    /*         return -1; */
    /*     } */
    /*     return 1; */
    /* } */

    if (ABT_mutex_lock(qhead->head_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        return -1;
    }

    // Check if the tail is of the same type, append to it if so
    if (qhead->queue && qhead->queue->prev->type == task_type && task_type != COLLECTIVE) {
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] append [%p] to %s task list\n", 
                task->func, qtype_names_g[task_type]);
        #endif
        DL_APPEND(qhead->queue->prev->task_list, task);
    }
    else {
        // Create a new task list in queue and add the current task to it
        async_task_list_t *new_list = (async_task_list_t*)calloc(1, sizeof(async_task_list_t));
        new_list->type = task_type;
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] create and append [%p] to new %s task list\n", 
                task->func, qtype_names_g[task_type]);
        #endif
        DL_APPEND(new_list->task_list, task);
        DL_APPEND(qhead->queue, new_list);
    }


    if (ABT_mutex_unlock(qhead->head_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        return -1;
    }

    /* if (get_n_running_task_in_queue(task) == 0) */
    /*     push_task_to_abt_pool(qhead, *(task->async_obj->pool_ptr)); */

    return 1;
} // add_task_to_queue

void dup_loc_param(H5VL_loc_params_t *dest, H5VL_loc_params_t const *loc_params)
{
    size_t ref_size;

    assert(dest);
    assert(loc_params);

    memcpy(dest, loc_params, sizeof(*loc_params));

    if (loc_params->type == H5VL_OBJECT_BY_NAME) {
        dest->loc_data.loc_by_name.name    =  strdup(loc_params->loc_data.loc_by_name.name);
        dest->loc_data.loc_by_name.lapl_id = H5Pcopy(loc_params->loc_data.loc_by_name.lapl_id);
    }
    else if (loc_params->type == H5VL_OBJECT_BY_IDX) {
        dest->loc_data.loc_by_idx.name    =  strdup(loc_params->loc_data.loc_by_idx.name);
        dest->loc_data.loc_by_idx.lapl_id = H5Pcopy(loc_params->loc_data.loc_by_idx.lapl_id);
    }
    else if (loc_params->type == H5VL_OBJECT_BY_TOKEN) {
        ref_size = 16; // taken from H5VLnative_object.c
        dest->loc_data.loc_by_token.token = malloc(ref_size);
        memcpy((void*)(dest->loc_data.loc_by_token.token), loc_params->loc_data.loc_by_token.token, ref_size);
    }

}

void free_loc_param(H5VL_loc_params_t *loc_params)
{
    assert(loc_params);

    if (loc_params->type == H5VL_OBJECT_BY_NAME) {
        free    ((void*)loc_params->loc_data.loc_by_name.name);
        H5Pclose(loc_params->loc_data.loc_by_name.lapl_id);
    }
    else if (loc_params->type == H5VL_OBJECT_BY_IDX) {
        free    ((void*)loc_params->loc_data.loc_by_idx.name);
        H5Pclose(loc_params->loc_data.loc_by_idx.lapl_id);
    }
    /* else if (loc_params->type == H5VL_OBJECT_BY_ADDR) { */
    /* } */
    else if (loc_params->type == H5VL_OBJECT_BY_TOKEN) {
        free    ((void*)loc_params->loc_data.loc_by_token.token);
    }
}


/* herr_t */ 
/* H5Pget_async_token(hid_t dxpl, H5ES_token *token) */
/* { */


/* } */

/* herr_t */ 
/* H5Gget_async_token(hid_t gid, H5ES_token *token) */
/* { */


/* } */

/* herr_t */ 
/* H5Dtest(hid_t dset, int *completed) */
/* { */
/*     herr_t ret_value; */
/*     H5VL_async_t *async_obj; */
/*     async_task_t *task_iter; */
/*     ABT_task_state state; */

/*     if (NULL == (async_obj = (H5VL_async_t*)H5VL_async_get_object((void*)dset))) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s H5I_object_verify failed\n", __func__); */
/*         goto done; */
/*     } */

/*     if (ABT_mutex_lock(async_obj->obj_mutex) != ABT_SUCCESS) { */
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */
/*         goto done; */
/*     } */

/*     // Check for all tasks on this dset of a file */
/*     DL_FOREACH(async_obj->file_task_list_head, task_iter) { */
/*         if (task_iter->async_obj == async_obj) { */
/*             ABT_task_get_state (task_iter->abt_task, &state); */
/*             if (ABT_TASK_STATE_RUNNING == state || ABT_TASK_STATE_READY == state) { */
/*                 *completed = 0; */
/*                 goto done; */
/*             } */
/*             else */
/*                 *completed = 1; */
/*         } */
/*     } */

/* done: */
/*     if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) */ 
/*         fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */
    
/*     return ret_value; */
/* } */

herr_t H5VL_async_dataset_wait(H5VL_async_t *async_obj)
{
    /* herr_t ret_value; */
    async_task_t *task_iter;
    /* ABT_task_state state; */
    /* ABT_thread_state thread_state; */
    hbool_t acquired = false;

    async_instance_g->start_abt_push = true;

    if (get_n_running_task_in_queue_obj(async_obj) == 0 )
	push_task_to_abt_pool(&async_instance_g->qhead, *async_obj->pool_ptr);

    if (H5TSmutex_release() < 0) 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5TSmutex_release\n", __func__);

    // Check for all tasks on this dset of a file

    if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        return -1;
    }
    DL_FOREACH2(async_obj->file_task_list_head, task_iter, file_list_next) {
        /* if (ABT_mutex_lock(async_obj->obj_mutex) != ABT_SUCCESS) { */
        /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */
        /*     return -1; */
        /* } */

        if (task_iter->async_obj == async_obj) {
            if (task_iter->is_done != 1) {
                ABT_eventual_wait(task_iter->eventual, NULL);
            }
        }
        /* if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) */ 
        /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */

    }

    if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        return -1;
    }
    while (false == acquired) {
        if (H5TSmutex_acquire(&acquired) < 0)
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5TSmutex_acquire\n", __func__);
    }

    return 0;
}

herr_t H5VL_async_file_wait(H5VL_async_t *async_obj)
{
    /* herr_t ret_value; */
    async_task_t *task_iter;
    /* ABT_task_state state; */
    /* ABT_thread_state thread_state; */
    hbool_t acquired = false;

    async_instance_g->start_abt_push = true;

    if (get_n_running_task_in_queue_obj(async_obj) == 0 )
	push_task_to_abt_pool(&async_instance_g->qhead, *async_obj->pool_ptr);

    if (H5TSmutex_release() < 0) 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5TSmutex_release\n", __func__);

    if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        return -1;
    }
    // Check for all tasks on this dset of a file
    DL_FOREACH2(async_obj->file_task_list_head, task_iter, file_list_next) {
        /* if (ABT_mutex_lock(async_obj->obj_mutex) != ABT_SUCCESS) { */
        /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */
        /*     return -1; */
        /* } */

        if (task_iter->is_done != 1) {
            ABT_eventual_wait(task_iter->eventual, NULL);
        }
        /* if (NULL != task_iter->abt_thread) { */
        /* if (NULL != task_iter->abt_task) { */
            /* ABT_thread_get_state(task_iter->abt_thread, &thread_state); */
            /* if (ABT_THREAD_STATE_TERMINATED != thread_state) */ 
                /* ABT_eventual_wait(task_iter->eventual, NULL); */
            /* ABT_task_get_state (task_iter->abt_task, &state); */
            /* if (ABT_TASK_STATE_TERMINATED != state) */ 
            /*     ABT_eventual_wait(task_iter->eventual, NULL); */
        /* } */
        /* if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) */ 
        /*     fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_lock failed\n", __func__); */
    }

    if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        return -1;
    }

    while (false == acquired) {
        if (H5TSmutex_acquire(&acquired) < 0)
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5TSmutex_acquire\n", __func__);
    }

    return 0;
}

/* static void */
/* execute_parent_task_recursive(async_task_t *task) */
/* { */
/*     if (task == NULL ) */ 
/*         return; */

/*     if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) */
/*          fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_mutex_unlock failed\n", __func__); */

/*     if (task->parent_obj != NULL) */
/*         execute_parent_task_recursive(task->parent_obj->create_task); */

/* #ifdef ENABLE_DBG_MSG */
/*     fprintf(stderr,"  [ASYNC VOL DBG] %s: cancel argobots task and execute now \n", __func__); */
/*     fflush(stderr); */
/* #endif */


/*     // Execute the task in current thread */
/*     task->func(task); */

/*     // Cancel the task already in the pool */
/*     /1* ABT_pool_remove(*task->async_obj->pool_ptr, task); *1/ */
/*     /1* ABT_thread_cancel(task); *1/ */
/*     ABT_task_cancel(task); */

/* #ifdef ENABLE_DBG_MSG */
/*     fprintf(stderr,"  [ASYNC VOL DBG] %s: finished executing task \n", __func__); */
/* #endif */
/* } */

void 
H5VL_async_start()
{
    async_instance_g->start_abt_push = true;
    if (NULL != async_instance_g->qhead.queue)
       push_task_to_abt_pool(&async_instance_g->qhead, async_instance_g->pool);
}

herr_t 
H5Pget_dxpl_async(hid_t dxpl, hbool_t *is_async)
{
    herr_t ret;
    assert(is_async);

    if ((ret = H5Pexist(dxpl, ASYNC_VOL_PROP_NAME)) <= 0) {
        if (ret < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pexist failed\n", __func__);
            return ret;
        }
        else {
            *is_async = false;
        }
    }
    else{
        if ((ret = H5Pget(dxpl, ASYNC_VOL_PROP_NAME, (void*)is_async)) < 0) 
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pget failed\n", __func__);
    }

    return ret;
}

herr_t 
H5Pset_dxpl_async(hid_t dxpl, hbool_t is_async)
{
    herr_t ret;
    size_t len = sizeof(hbool_t);

    if ((ret = H5Pinsert(dxpl, ASYNC_VOL_PROP_NAME, len, (void*)&is_async, NULL, NULL, NULL, NULL, NULL, NULL)) < 0)
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pinsert failed\n", __func__);

    return ret;
}

herr_t 
H5Pget_dxpl_async_cp_limit(hid_t dxpl, hsize_t *size)
{
    herr_t ret;
    assert(size);

    if ((ret = H5Pexist(dxpl, ASYNC_VOL_CP_SIZE_LIMIT_NAME)) <= 0) {
        if (ret < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pexist failed\n", __func__);
            return ret;
        }
        else {
            *size = 0;
        }
    }
    else{
        if ((ret = H5Pget(dxpl, ASYNC_VOL_CP_SIZE_LIMIT_NAME, (void*)size)) < 0) 
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pget failed\n", __func__);
    }

    return ret;
}

herr_t 
H5Pset_dxpl_async_cp_limit(hid_t dxpl, hsize_t size)
{
    herr_t ret;
    size_t len = sizeof(hsize_t);

    if ((ret = H5Pinsert(dxpl, ASYNC_VOL_CP_SIZE_LIMIT_NAME, len, (void*)&size, NULL, NULL, NULL, NULL, NULL, NULL)) < 0)
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Pinsert failed\n", __func__);

    return ret;

}

static herr_t
dataset_get_wrapper(void *dset, hid_t driver_id, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, ...)
{
    herr_t ret;
    va_list args;
    va_start(args, req);
    ret = H5VLdataset_get(dset, driver_id, get_type, dxpl_id, req, args);
    va_end(args);
    return ret;
}

herr_t
H5VLasync_get_data_size(void *dset, hid_t space, hid_t connector_id, hsize_t *size)
{
    herr_t ret;
    hid_t dset_type, dset_space;

    if ((ret = dataset_get_wrapper(dset, connector_id, H5VL_DATASET_GET_TYPE, H5P_DATASET_XFER_DEFAULT, NULL, (void*)&dset_type)) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s dataset_get_wrapper failed\n", __func__);
        goto done;
    }

    *size = H5Tget_size(dset_type);
    if ((ret = H5Tclose(dset_type)) < 0)
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Tclose failed\n", __func__);

    if (H5S_ALL == space) {
        if ((ret = dataset_get_wrapper(dset, connector_id, H5VL_DATASET_GET_SPACE, 
                                       H5P_DATASET_XFER_DEFAULT, NULL, (void*)&dset_space)) < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s dataset_get_wrapper failed\n", __func__);
            goto done;
        }
        (*size) *= H5Sget_simple_extent_npoints(dset_space);

        if ((ret = H5Sclose(dset_space)) < 0)
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Sclose failed\n", __func__);
    }
    else
        (*size) *= H5Sget_select_npoints(space);


done:
    return ret;
}

herr_t
H5VLasync_get_data_nelem(void *dset, hid_t space, hid_t connector_id, hsize_t *size)
{
    herr_t ret = 0;
    hid_t dset_space;

    if (H5S_ALL == space) {
        if ((ret = dataset_get_wrapper(dset, connector_id, H5VL_DATASET_GET_SPACE, 
                                       H5P_DATASET_XFER_DEFAULT, NULL, (void*)&dset_space)) < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s dataset_get_wrapper failed\n", __func__);
            goto done;
        }
        (*size) = H5Sget_simple_extent_npoints(dset_space);

        if ((ret = H5Sclose(dset_space)) < 0)
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5Sclose failed\n", __func__);
    }
    else
        (*size) = H5Sget_select_npoints(space);

done:
    return ret;
}


double get_elapsed_time(struct timeval *tstart, struct timeval *tend)
{
    return (double)(((tend->tv_sec-tstart->tv_sec)*1000000LL + tend->tv_usec-tstart->tv_usec) / 1000000.0);
}

void H5VLasync_waitall()
{
    int sleeptime = 100000;
    size_t size = 1;

    while(async_instance_g && (async_instance_g->nfopen > 0 || size > 0)) {

        usleep(sleeptime);

        ABT_pool_get_size(async_instance_g->pool, &size);
        /* printf("H5VLasync_finalize: pool size is %lu\n", size); */

        if (size == 0) {
            if (async_instance_g->nfopen == 0) 
                break;
        }
    }

    return;
}


void H5VLasync_finalize()
{
    H5VLasync_waitall();
}


/* static void print_cpuset(int rank, int cpuset_size, int *cpuset) */
/* { */
/*     char *cpuset_str = NULL; */
/*     int i; */
/*     size_t pos = 0, len; */

/*     cpuset_str = (char *)calloc(cpuset_size * 5, sizeof(char)); */
/*     assert(cpuset_str); */

/*     for (i = 0; i < cpuset_size; i++) { */
/*         if (i) { */
/*             sprintf(&cpuset_str[pos], ",%d", cpuset[i]); */
/*         } else { */
/*             sprintf(&cpuset_str[pos], "%d", cpuset[i]); */
/*         } */
/*         len = strlen(&cpuset_str[pos]); */
/*         pos += len; */
/*     } */
/*     fprintf(stderr, "[E%d] CPU set (%d): {%s}\n", rank, cpuset_size, cpuset_str); */

/*     free(cpuset_str); */
/*     fflush(stderr); */
/* } */

/* static void async_set_affinity() */
/* { */
/*     int ret, j; */
/*     cpu_set_t cpuset; */

/*     CPU_ZERO(&cpuset); */
/*     for (j = 0; j < 2; j++) */
/*         CPU_SET(j, &cpuset); */
/*     ret = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset); */
/*     if (ret != 0) { */
/*         fprintf(stderr, "  [ASYNC VOL ERROR] with pthread_setaffinity_np: %d\n", ret); */
/*         return; */
/*     } */
/* } */


/* static void async_get_affinity() */
/* { */
/*     int ret, j; */
/*     cpu_set_t cpuset; */
/*     int cpu; */

/*     cpu = sched_getcpu(); */
/*     fprintf(stderr, "Argobots thread is running on CPU %d\n", cpu); */

/*     /1* ret = pthread_getaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset); *1/ */
/*     /1* if (ret != 0) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with pthread_getaffinity_np: %d\n", ret); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */
/*     /1* fprintf(stderr, "Set returned by pthread_getaffinity_np() contained:\n"); *1/ */
/*     /1* for (j = 0; j < CPU_SETSIZE; j++) *1/ */
/*     /1*     if (CPU_ISSET(j, &cpuset)) *1/ */
/*     /1*         fprintf(stderr, "    CPU %d\n", j); *1/ */
/* } */

/* static void test_affinity() */
/* { */
/*     ABT_xstream xstream; */
/*     int i, rank, ret; */
/*     int cpuid = -1, new_cpuid; */
/*     int cpuset_size, num_cpus = -1; */
/*     int *cpuset = NULL; */
/*     int num_xstreams = 1; */

/*     ret = ABT_xstream_self(&xstream); */
/*     if (ret != ABT_SUCCESS) { */
/*         fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_self\n"); */
/*         return; */
/*     } */

/*     ret = ABT_xstream_get_rank(xstream, &rank); */
/*     if (ret != ABT_SUCCESS) { */
/*         fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_rank\n"); */
/*         return; */
/*     } */

/*     /1* ret = ABT_xstream_get_cpubind(xstream, &cpuid); *1/ */
/*     /1* if (ret != ABT_SUCCESS) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_cpubind\n"); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */
/*     /1* fprintf(stderr, "[E%d] CPU bind: %d\n", rank, cpuid); *1/ */

/*     /1* new_cpuid = (cpuid + 1) % num_xstreams; *1/ */
/*     /1* fprintf(stderr, "[E%d] change binding: %d -> %d\n", rank, cpuid, new_cpuid); *1/ */

/*     /1* ret = ABT_xstream_set_cpubind(xstream, new_cpuid); *1/ */
/*     /1* if (ret != ABT_SUCCESS) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_set_cpubind\n"); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */
/*     /1* ret = ABT_xstream_get_cpubind(xstream, &cpuid); *1/ */
/*     /1* if (ret != ABT_SUCCESS) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_cpubind\n"); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */

/*     /1* /2* assert(cpuid == new_cpuid); *2/ *1/ */
/*     /1* fprintf(stderr, "[E%d] CPU bind: %d\n", rank, cpuid); *1/ */

/*     ret = ABT_xstream_get_affinity(xstream, 0, NULL, &num_cpus); */
/*     if (ret != ABT_SUCCESS) { */
/*         fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_affinity\n"); */
/*         return; */
/*     } */

/*     fprintf(stderr, "[E%d] num_cpus=%d\n", rank, num_cpus); */
/*     if (num_cpus > 0) { */
/*         cpuset_size = num_cpus; */
/*         cpuset = (int *)malloc(cpuset_size * sizeof(int)); */
/*         /1* assert(cpuset); *1/ */

/*         num_cpus = 0; */
/*         ret = ABT_xstream_get_affinity(xstream, cpuset_size, cpuset, &num_cpus); */
/*         if (ret != ABT_SUCCESS) { */
/*             fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_affinity\n"); */
/*             return; */
/*         } */
/*         /1* assert(num_cpus == cpuset_size); *1/ */
/*         print_cpuset(rank, cpuset_size, cpuset); */

/*         free(cpuset); */
/*     } */

/*     /1* cpuset = (int *)malloc(num_xstreams * sizeof(int)); *1/ */
/*     /1* /2* assert(cpuset); *2/ *1/ */
/*     /1* for (i = 0; i < num_xstreams; i++) { *1/ */
/*     /1*     cpuset[i] = i; *1/ */
/*     /1* } *1/ */
/*     /1* ret = ABT_xstream_set_affinity(xstream, num_xstreams, cpuset); *1/ */
/*     /1* if (ret != ABT_SUCCESS) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_set_affinity\n"); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */
/*     /1* ret = ABT_xstream_get_affinity(xstream, num_xstreams, cpuset, &num_cpus); *1/ */
/*     /1* if (ret != ABT_SUCCESS) { *1/ */
/*     /1*     fprintf(stderr, "  [ASYNC VOL ERROR] with ABT_xstream_get_affinity\n"); *1/ */
/*     /1*     return; *1/ */
/*     /1* } *1/ */
/*     /1* /2* assert(num_cpus == num_xstreams); *2/ *1/ */
/*     /1* print_cpuset(rank, num_xstreams, cpuset); *1/ */
/*     /1* free(cpuset); *1/ */
/* } */

H5RQ_token_int_t *
H5RQ__new_token(void)
{
    H5RQ_token_int_t *ret = (H5RQ_token_int_t*)calloc(1, sizeof(H5RQ_token_int_t));

    return ret;
}

herr_t 
H5RQ_token_check(H5RQ_token_t token, int *status)
{
    H5RQ_token_int_t *in_token;

    if (NULL == token || NULL == status) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s input NULL!\n", __func__);
        return -1;
    }

    in_token = (H5RQ_token_int_t*)(token);

    if (in_token->task) 
        *status = in_token->task->is_done;
    else 
        *status = 1;

    return 1;
}

herr_t 
H5RQ_token_wait(H5RQ_token_t token)
{
    /* hbool_t acquired = false; */
    H5RQ_token_int_t *in_token;

    if (NULL == token) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s input NULL!\n", __func__);
        return -1;
    }

    in_token = (H5RQ_token_int_t*)(token);

    if (NULL == in_token->task) 
        return 1;
    
    if (in_token->task->is_done != 1) {

        if(NULL == in_token->task->async_obj) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with task async object\n", __func__);
            return -1;
        }

        ABT_eventual_wait(in_token->task->eventual, NULL);

    }

    return 1;
}

herr_t 
H5RQ_token_free(H5RQ_token_t token)
{
    H5RQ_token_int_t *in_token;

    if (NULL == token) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s input NULL!\n", __func__);
        return -1;
    }

    in_token = (H5RQ_token_int_t*)(token);

    if (in_token->task) 
        in_token->task->token = NULL;
    
    free(in_token);

    return 1;
}
