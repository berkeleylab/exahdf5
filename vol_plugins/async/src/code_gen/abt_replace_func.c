#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, const char *argv[])
{
    FILE *stream, *fout_c;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int i, tmp;

    const char *in_file = argv[1];
    char out_file[128];

    if (in_file == NULL) {
        printf("No input file! Exiting...\n");
        goto done;
    }
 
    stream = fopen(in_file, "r");
    if (stream == NULL) {
        printf("No input file! Exiting...\n");
        goto done;
    }

    sprintf(out_file ,"%s_new.c", in_file);
    fout_c = fopen(out_file, "w");
    if (fout_c == NULL) {
        printf("Output file failed! Exiting...\n");
        goto done;
    }
 
 
    int is_close = 0;
    int start;
    char ptr_name[128]={0};
    char alloc_name[128] = {0};
    int is_start = 0, is_end = 0;
    int nline = 0;
    int is_func_start = 0;
    int skip_under = 0;
    while ((read = getline(&line, &len, stream)) != -1) {


        nline++;
        if (is_start == 0 ) {
            fprintf(fout_c, "%s", line);
            if (strstr(line, "REPLACE_START") != NULL) {
                is_start = 1;
            }
            continue;
        }

        // Skip under part
        if (strstr(line, "void *under;") != NULL) {
            continue;
        }

        if (strstr(line, "REPLACE_END") != NULL) 
            is_end = 1;

        if (is_end == 1) {
            fprintf(fout_c, "%s", line);
            continue;
        }

        if (strstr(line, "set_write") != NULL) {
            int debug=1;
        }

        if (strstr(line, "if(ret && *ret)") != NULL || strstr(line, "if(req && *req)") != NULL || strstr(line, "new_obj(") != NULL) {
            line[strlen(line)-1] = 0;
            fprintf(fout_c, "/* %s */\n", line);
            continue;
        }

        // Clear alloc name at the start of each function
        if (strstr(line, "static") != NULL) {
            memset(alloc_name, 0, 128);
        }

        if (strstr(line, "Release our wrapper,") != NULL) {
            read = getline(&line, &len, stream);
            read = getline(&line, &len, stream);
            read = getline(&line, &len, stream);
            continue;
        }

        // Get alloc name, e.g. group
        if (strstr(line, "alloc") != NULL) {
            i = 0;
            while (i < strlen(line) && line[i] == ' ') {i++;}
            start = i;
            while (i < strlen(line) && line[i] != '=') {i++;}
            snprintf(alloc_name, i- start, "%s", &line[start]);
        }

        // get the H5VL_async_t pointer name, could be "file" or "f"
        /* H5VL_async_t *f = (H5VL_async_t *)file; */
        if (strstr(line, "H5VL_async_t ") != NULL  && strstr(line, "alloc") == NULL && strstr(line, "=") == NULL) {
            memset(ptr_name, 0, 128);
            i = 0;
            while (i < strlen(line) && line[i] != '*') {i++;}
            i++;
            start = i;
            for (; i < strlen(line); i++) {
                if (line[i] == ' ' || line[i] == ';') {
                    tmp = i;
                    snprintf(ptr_name, tmp - start+1, "%s", &line[start]);
                    break;
                }
            }
        }

        // Find lines with H5VL_* calls
        int is_create = 0;
        int is_close = 0;
        if (strstr(line, ");") != NULL && strstr(line, "H5VL") != NULL) {


            // Skip under_object in structure def and error checking
            if (strstr(line, "alloc") != NULL || strstr(line, "info_free") != NULL || strstr(line, "request") != NULL || 
                    strstr(line, "LINK") != NULL || strstr(line, "link") != NULL || strstr(line, "H5VLget_wrap_ctx") || 
                    strstr(line, "H5VLget_object") != NULL || strstr(line, "va_arg") != NULL || 
                    strstr(line, "file_specific") != NULL || strstr(line, "new_obj") != NULL || 
                    strstr(line, "file_wait") != NULL || strstr(line, "free_obj") != NULL) {
                fprintf(fout_c, "%s", line);
                continue;
            }


            if (strstr(line, "create") != NULL || strstr(line, "commit") != NULL || strstr(line, "open") != NULL) 
                is_create = 1;
            else if (strstr(line, "close") != NULL)
                is_close = 1;


            // Get the name of the object, "under" or "ret_value"
            char obj_char[16];
            int k=0, l;
            for (k = 0; k < strlen(line); k++)  {
                if (line[k] == '-') {
                    break;
                }
            }
            l=k;
            while(line[k] != ' '&& line[k] != '(') {k--;};
            k++;
            strncpy(obj_char, &line[k], l-k);
            obj_char[l-k] = 0;


            if (strstr(line, "dataset_specific") != NULL) {
                fprintf(fout_c, "    // For H5Dwait\n");
                fprintf(fout_c, "    if (H5VL_DATASET_WAIT == specific_type) \n");
                fprintf(fout_c, "        return (H5VL_async_dataset_wait(%s));\n\n", obj_char);
            }
            /* else if (strstr(line, "file_specific") != NULL) { */
            /*     fprintf(fout_c, "    // For H5Fwait\n"); */
            /*     fprintf(fout_c, "    if (H5VL_FILE_WAIT == specific_type) \n"); */
            /*     fprintf(fout_c, "        return (H5VL_async_file_wait(%s));\n\n", obj_char); */
            /* } */


            if (is_create != 1) {
                fprintf(fout_c, "    if ((");
            }

            // Get the name of the H5VL*
            char new_name[128];
            for (i = 0; i < strlen(line); i++) {
                if (line[i] == ' ') 
                    continue;
                if (line[i] == 'H' && line[i+1] == '5' ) {
                    tmp = i;
                    snprintf(new_name, i-2-4, "%s", line+4);
                    break;
                }
            }
            if (is_create) 
                fprintf(fout_c, "    %s", ptr_name);
            else
                fprintf(fout_c, "%s", new_name);

            // skip H5VL and replace with "async" and the rest
            i += 4;
            tmp = i;
            fprintf(fout_c, " = async_");


            char tmp_name[128];
            for (; i < strlen(line); i++) {
                if (line[i] == '(') {
                    snprintf(tmp_name, i-tmp+1, "%s", &line[tmp]);
                    break;
                }
            }
            fprintf(fout_c, "%s", tmp_name);
            // Now we are up to '(' of line

            // If line starts with "ret_value", it means it is not returning an object pointer,
            // which would be used immediately after, thus can use non-blocking async call
            /* if (strstr(line, "create") == NULL && strstr(line, "open") == NULL && \ */
            /*     strstr(line, "get") == NULL  && strstr(line, "datatype") == NULL) { */
            if (strstr(line, "get") != NULL || strstr(line, "specific") != NULL  || strstr(line, "optional") != NULL || strstr(line, "set_open") != NULL || strstr(line, "set_open") != NULL ) {
            /* if (strstr(line, "get") != NULL || strstr(line, "specific") != NULL || strstr(line, "attr_read") != NULL || strstr(line, "optional") != NULL) { */
                // If not the above operations, it can go async
                fprintf(fout_c, "(1, ");
                /* fprintf(fout_c, "(0, "); */
            }
            else {
                fprintf(fout_c, "(0, ");
                /* fprintf(fout_c, "(1, "); */
            }

            fprintf(fout_c, "async_instance_g, ");
            /* fprintf(fout_c, "async_instance_g, ", alloc_name); */
            /* if (alloc_name[1] != 0) */ 
            /*     fprintf(fout_c, "%s, ", alloc_name); */
            i++; // skip '('


            /* if (strstr(line, "H5VLfile_create") != NULL || strstr(line, "H5VLfile_open") != NULL) { */
            /*     fprintf(fout_c, "file, "); */
            /* } */

            // for link create
            int is_link_create = 0;
            if (strstr(line, "link_create") != NULL) {
                is_link_create = 1;
            }

            // Print all parameters of H5VL*
            int j, is_under_vol = 0;
            for (; i < strlen(line); i++) {
                is_under_vol = 0;
                for (j = i; j < strlen(line); j++) {
                    if (line[j] == ',') 
                        break;
                    if (line[j] == '-' && line[j+8] == 'v') 
                        is_under_vol = 1;
                }

                if (is_under_vol == 1) {
                    while(i<strlen(line) && line[i]!=',') {i++;}
                    i++;
                }

                /* // skip native_connector_id_g as we don't need it */
                /* if (line[i] == 'n' && line[i+1] == 'a' && line[i+2] == 't') { */
                /*     while(line[i] != ',') {i++;} */
                /*     i+=1; */
                /* } */
                /* else if (line[i] == 'f' && line[i+1] == 'a') { */
                /*     // replace "fapl_id" with "under_fapl_id" */
                /*     while(i<strlen(line) && line[i]!=',') {i++;} */
                /*     fprintf(fout_c, "under_fapl_id"); */
                /* } */
                if (line[i] == ')') 
                    break;
                if (line[i] == '-') {
                    fprintf(fout_c, ",");
                    while(i<strlen(line) && line[i]!=',') {i++;}
                }
                else
                    fprintf(fout_c, "%c", line[i]);
            }

            if (is_create == 1) {
                fprintf(fout_c, ");\n");
            }
            else {
                fprintf(fout_c, ")) < 0 ) {\n");
                fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] with async_%s\\n\");\n", tmp_name);
                /* if (alloc_name[1] != 0) */ 
                /*     fprintf(fout_c, "        free(%s);\n", alloc_name); */
                
                /* if (is_create != 1) */ 
                /*     fprintf(fout_c, "        return -1;\n"); */
                /* else */
                /*     fprintf(fout_c, "        return NULL;\n"); */
                fprintf(fout_c, "    }\n");
            }

            fprintf(fout_c, "\n");
            /* fprintf(fout_c, "    /1* Wait for previous async request on this dataset before add the next *1/\n"); */
            /* fprintf(fout_c, "    if (NULL != %s->status) \n", obj_char); */
            /* fprintf(fout_c, "        H5VLasync_wait(%s->status);\n", obj_char); */
            /* fprintf(fout_c, "    \n"); */
            /* fprintf(fout_c, "    %s->status = status;\n", obj_char); */

            // Skip the next line due to line break
            while (read = getline(&line, &len, stream) <= 2) {
                if(read==-1) break;
            }
                 
            // Skip under part
            if (strstr(line, "if(under) {") != NULL) {
                while ((read = getline(&line, &len, stream)) != -1) {
                    if (strstr(line, "else") != NULL) 
                        break;
                }
                read = getline(&line, &len, stream);
                continue;
            }

       

        } // If line includes "under_object"

        // Skip the assert for datatype close
        if (strstr(line, "assert(type->under_object)") != NULL) {
            continue;
        }

        if (strstr(line, "free(f)") != NULL) {
            fprintf(fout_c, "    free_file_async_obj(f);\n");
            continue;
        }

        fprintf(fout_c, "%s", line);


    } // End while read line
 
    free(line);
    fclose(stream);
    fclose(fout_c);


done:
    return 0;
}
