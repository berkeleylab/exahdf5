
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "hdf5.h"
#include "h5_vol_external_pass_through_native.h"

#define PASS_THROUGH_VOL 505
/* #define ENABLE_VOL_LOG 1 */

/* ====INSERT==== 0_include.c */

/* ====INSERT==== 1_struct_def.h */

/* ====INSERT==== out_abt_gen_def.h */

/* ====INSERT==== 2_global_var.h */


static herr_t H5VL_pass_through_init(hid_t vipl_id);
static herr_t H5VL_pass_through_term(void);
static void *H5VL_pass_through_info_copy(const void *info);
static int H5VL_pass_through_info_cmp(const void *info1, const void *info2);
static herr_t H5VL_pass_through_info_free(void *info);
static void *H5VL_pass_through_get_object(const void *obj);
static herr_t H5VL_pass_through_get_wrap_ctx(const void *obj, void **wrap_ctx);
static herr_t H5VL_pass_through_free_wrap_ctx(void *obj);
static void *H5VL_pass_through_wrap_object(void *obj, void *wrap_ctx);

/* Attribute callbacks */
static void *H5VL_pass_through_attr_create(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t acpl_id, hid_t aapl_id, hid_t dxpl_id, void **req);
static void *H5VL_pass_through_attr_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t aapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_attr_read(void *attr, hid_t mem_type_id, void *buf, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_attr_write(void *attr, hid_t mem_type_id, const void *buf, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_attr_get(void *obj, H5VL_attr_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_attr_specific(void *obj, H5VL_loc_params_t loc_params, H5VL_attr_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_attr_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_attr_close(void *attr, hid_t dxpl_id, void **req); 

/* Dataset callbacks */
static void *H5VL_pass_through_dataset_create(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t dcpl_id, hid_t dapl_id, hid_t dxpl_id, void **req);
static void *H5VL_pass_through_dataset_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t dapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_dataset_read(void *dset, hid_t mem_type_id, hid_t mem_space_id,
                                    hid_t file_space_id, hid_t plist_id, void *buf, void **req);
static herr_t H5VL_pass_through_dataset_write(void *dset, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t plist_id, const void *buf, void **req);
static herr_t H5VL_pass_through_dataset_get(void *dset, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_dataset_specific(void *obj, H5VL_dataset_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_dataset_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_dataset_close(void *dset, hid_t dxpl_id, void **req);

/* Datatype callbacks */
static void *H5VL_pass_through_datatype_commit(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t type_id, hid_t lcpl_id, hid_t tcpl_id, hid_t tapl_id, hid_t dxpl_id, void **req);
static void *H5VL_pass_through_datatype_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t tapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_datatype_get(void *dt, H5VL_datatype_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_datatype_specific(void *obj, H5VL_datatype_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_datatype_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_datatype_close(void *dt, hid_t dxpl_id, void **req);

/* File callbacks */
static void *H5VL_pass_through_file_create(const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id, hid_t dxpl_id, void **req);
static void *H5VL_pass_through_file_open(const char *name, unsigned flags, hid_t fapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_file_get(void *file, H5VL_file_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_file_specific(void *file, H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_file_optional(void *file, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_file_close(void *file, hid_t dxpl_id, void **req);

/* Group callbacks */
static void *H5VL_pass_through_group_create(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t gcpl_id, hid_t gapl_id, hid_t dxpl_id, void **req);
static void *H5VL_pass_through_group_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t gapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_group_get(void *obj, H5VL_group_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_group_specific(void *obj, H5VL_group_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_group_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_group_close(void *grp, hid_t dxpl_id, void **req);

/* Link callbacks */
static herr_t H5VL_pass_through_link_create(H5VL_link_create_type_t create_type, void *obj, H5VL_loc_params_t loc_params, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_link_copy(void *src_obj, H5VL_loc_params_t loc_params1, void *dst_obj, H5VL_loc_params_t loc_params2, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_link_move(void *src_obj, H5VL_loc_params_t loc_params1, void *dst_obj, H5VL_loc_params_t loc_params2, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_link_get(void *obj, H5VL_loc_params_t loc_params, H5VL_link_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_link_specific(void *obj, H5VL_loc_params_t loc_params, H5VL_link_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_link_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);

/* Object callbacks */
static void *H5VL_pass_through_object_open(void *obj, H5VL_loc_params_t loc_params, H5I_type_t *opened_type, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_object_copy(void *src_obj, H5VL_loc_params_t src_loc_params, const char *src_name, void *dst_obj, H5VL_loc_params_t dst_loc_params, const char *dst_name, hid_t ocpypl_id, hid_t lcpl_id, hid_t dxpl_id, void **req);
static herr_t H5VL_pass_through_object_get(void *obj, H5VL_loc_params_t loc_params, H5VL_object_get_t get_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_object_specific(void *obj, H5VL_loc_params_t loc_params, H5VL_object_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments);
static herr_t H5VL_pass_through_object_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments);

/* Async request callbacks */
static herr_t H5VL_pass_through_request_wait(void *req, uint64_t timeout, H5ES_status_t *status);
static herr_t H5VL_pass_through_request_cancel(void *req);
static herr_t H5VL_pass_through_request_specific(void *req, H5VL_request_specific_t specific_type, va_list arguments);
static herr_t H5VL_pass_through_request_optional(void *req, va_list arguments);
static herr_t H5VL_pass_through_request_free(void *req);

static const H5VL_class_t H5VL_pass_through_g = {
    0,                                              /* version      */
    PASS_THROUGH_VOL,                               /* value        */
    "pass_through",				    /* name         */
    0,                                              /* capability flags */
    H5VL_pass_through_init,                         /* initialize   */
    H5VL_pass_through_term,                         /* terminate    */
    sizeof(hid_t),                                  /* info size    */
    H5VL_pass_through_info_copy,                    /* info copy    */
    H5VL_pass_through_info_cmp,                     /* info compare */
    H5VL_pass_through_info_free,                    /* info free    */
    H5VL_pass_through_get_object,                   /* get_object   */
    H5VL_pass_through_get_wrap_ctx,                 /* get_wrap_ctx */
    H5VL_pass_through_free_wrap_ctx,                /* free_wrap_ctx */
    H5VL_pass_through_wrap_object,                  /* wrap_object  */
    {                                           /* attribute_cls */
        H5VL_pass_through_attr_create,                       /* create */
        H5VL_pass_through_attr_open,                         /* open */
        H5VL_pass_through_attr_read,                         /* read */
        H5VL_pass_through_attr_write,                        /* write */
        H5VL_pass_through_attr_get,                          /* get */
        H5VL_pass_through_attr_specific,                     /* specific */
        H5VL_pass_through_attr_optional,                     /* optional */
        H5VL_pass_through_attr_close                         /* close */
    },
    {                                           /* dataset_cls */
        H5VL_pass_through_dataset_create,                    /* create */
        H5VL_pass_through_dataset_open,                      /* open */
        H5VL_pass_through_dataset_read,                      /* read */
        H5VL_pass_through_dataset_write,                     /* write */
        H5VL_pass_through_dataset_get,                       /* get */
        H5VL_pass_through_dataset_specific,                  /* specific */
        H5VL_pass_through_dataset_optional,                  /* optional */
        H5VL_pass_through_dataset_close                      /* close */
    },
    {                                               /* datatype_cls */
        H5VL_pass_through_datatype_commit,                   /* commit */
        H5VL_pass_through_datatype_open,                     /* open */
        H5VL_pass_through_datatype_get,                      /* get_size */
        H5VL_pass_through_datatype_specific,                 /* specific */
        H5VL_pass_through_datatype_optional,                 /* optional */
        H5VL_pass_through_datatype_close                     /* close */
    },
    {                                           /* file_cls */
        H5VL_pass_through_file_create,                       /* create */
        H5VL_pass_through_file_open,                         /* open */
        H5VL_pass_through_file_get,                          /* get */
        H5VL_pass_through_file_specific,                     /* specific */
        H5VL_pass_through_file_optional,                     /* optional */
        H5VL_pass_through_file_close                         /* close */
    },
    {                                           /* group_cls */
        H5VL_pass_through_group_create,                      /* create */
        H5VL_pass_through_group_open,                        /* open */
        H5VL_pass_through_group_get,                         /* get */
        H5VL_pass_through_group_specific,                    /* specific */
        H5VL_pass_through_group_optional,                    /* optional */
        H5VL_pass_through_group_close                        /* close */
    },
    {                                           /* link_cls */
        H5VL_pass_through_link_create,                       /* create */
        H5VL_pass_through_link_copy,                         /* copy */
        H5VL_pass_through_link_move,                         /* move */
        H5VL_pass_through_link_get,                          /* get */
        H5VL_pass_through_link_specific,                     /* specific */
        H5VL_pass_through_link_optional,                     /* optional */
    },
    {                                           /* object_cls */
        H5VL_pass_through_object_open,                       /* open */
        H5VL_pass_through_object_copy,                       /* copy */
        H5VL_pass_through_object_get,                        /* get */
        H5VL_pass_through_object_specific,                   /* specific */
        H5VL_pass_through_object_optional,                   /* optional */
    },
    {                                           /* request_cls */
        H5VL_pass_through_request_wait,                      /* wait */
        H5VL_pass_through_request_cancel,                    /* cancel */
        H5VL_pass_through_request_specific,                  /* specific */
        H5VL_pass_through_request_optional,                  /* optional */
        H5VL_pass_through_request_free                       /* free */
    },
    NULL                                        /* optional */
};

typedef struct H5VL_pass_through_wrap_ctx_t {
    hid_t under_vol_id;         /* VOL ID for under VOL */
    void *under_wrap_ctx;       /* Object wrapping context for under VOL */
} H5VL_pass_through_wrap_ctx_t;


/* ====INSERT==== 3_abt_func.c */
/* ====DELETE==== START */

typedef struct H5VL_pass_through_t {
    void   *under_object;
    hid_t  under_vol_id;
} H5VL_pass_through_t;

hid_t register_passthrough_vol(void)
{
    hid_t vol_id;
    
    if(H5VLis_connector_registered("pass_through") == 1) {
        vol_id = H5VLget_connector_id("pass_through");
        return vol_id;
    }

    vol_id = H5VLregister_connector(&H5VL_pass_through_g, H5P_DEFAULT);
    assert(vol_id > 0);
    assert(H5VLis_connector_registered("pass_through") == 1);

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL Registered!\n");
#endif
    
    return vol_id;
}


static herr_t H5VL_pass_through_init(hid_t vipl_id)
{
#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL INIT\n");
#endif

    return 0;
}

static herr_t H5VL_pass_through_term(void)
{
#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL TERM\n");
#endif
    return 0;
}

/* ====DELETE==== END */

/* ====INSERT==== out_abt_gen.c*/
static void *
H5VL_pass_through_info_copy(const void *_info)
{
    const H5VL_pass_through_info_t *info = (const H5VL_pass_through_info_t *)_info;
    H5VL_pass_through_info_t *new_info;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL INFO Copy\n");
#endif

    /* Allocate new VOL info struct for the pass through connector */
    new_info = (H5VL_pass_through_info_t *)calloc(1, sizeof(H5VL_pass_through_info_t));

    /* Increment reference count on underlying VOL ID, and copy the VOL info */
    new_info->under_vol_id = info->under_vol_id;
    H5Iinc_ref(new_info->under_vol_id);
    if(info->under_vol_info)
        H5VLcopy_connector_info(new_info->under_vol_id, &(new_info->under_vol_info), info->under_vol_info);

    return new_info;
}

static int
H5VL_pass_through_info_cmp(const void *_info1, const void *_info2)
{
    const H5VL_pass_through_info_t *info1 = (const H5VL_pass_through_info_t *)_info1;
    const H5VL_pass_through_info_t *info2 = (const H5VL_pass_through_info_t *)_info2;
    int cmp_value = 0;          /* Value from comparison */

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL INFO Compare\n");
#endif

    if(info1 == NULL && info2 != NULL)
        return(-1);
    if(info1 != NULL && info2 == NULL)
        return(1);
    
    /* Compare under VOL connector classes */
    H5VLcmp_connector_cls(&cmp_value, info1->under_vol_id, info2->under_vol_id);
    if(cmp_value != 0)
        return(cmp_value);

    /* Compare under VOL connector info objects */
    H5VLcmp_connector_info(&cmp_value, info1->under_vol_id, info1->under_vol_info, info2->under_vol_info);
    if(cmp_value != 0)
        return(cmp_value);

    return 0;
}

static herr_t
H5VL_pass_through_info_free(void *_info)
{
    H5VL_pass_through_info_t *info = (H5VL_pass_through_info_t *)_info;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL INFO Free\n");
#endif

    /* Release underlying VOL ID and info */
    if(info->under_vol_info)
        H5VLfree_connector_info(info->under_vol_id, info->under_vol_info);
    H5Idec_ref(info->under_vol_id);

    /* Free pass through info object itself */
    free(info);

    return 0;
}

static void *
H5VL_pass_through_get_object(const void *obj)
{
    const H5VL_pass_through_t *o = (const H5VL_pass_through_t *)obj;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL Get object\n");
#endif

    return H5VLget_object(o->under_object, o->under_vol_id);
}

static herr_t
H5VL_pass_through_get_wrap_ctx(const void *obj, void **wrap_ctx)
{
    const H5VL_pass_through_t *o = (const H5VL_pass_through_t *)obj;
    H5VL_pass_through_wrap_ctx_t *new_wrap_ctx;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL WRAP CTX Get\n");
#endif

    /* Allocate new VOL object wrapping context for the pass through connector */
    new_wrap_ctx = (H5VL_pass_through_wrap_ctx_t *)calloc(1, sizeof(H5VL_pass_through_wrap_ctx_t));

    /* Increment reference count on underlying VOL ID, and copy the VOL info */
    new_wrap_ctx->under_vol_id = o->under_vol_id;
    H5Iinc_ref(new_wrap_ctx->under_vol_id);
    H5VLget_wrap_ctx(o->under_object, o->under_vol_id, &new_wrap_ctx->under_wrap_ctx);

    /* Set wrap context to return */
    *wrap_ctx = new_wrap_ctx;

    return 0;
}

static herr_t
H5VL_pass_through_free_wrap_ctx(void *_wrap_ctx)
{
    H5VL_pass_through_wrap_ctx_t *wrap_ctx = (H5VL_pass_through_wrap_ctx_t *)_wrap_ctx;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL WRAP CTX Free\n");
#endif

    /* Release underlying VOL ID and wrap context */
    if(wrap_ctx->under_wrap_ctx)
        H5VLfree_wrap_ctx(wrap_ctx->under_wrap_ctx, wrap_ctx->under_vol_id);
    H5Idec_ref(wrap_ctx->under_vol_id);

    /* Free pass through wrap context object itself */
    free(wrap_ctx);

    return 0;
}

static void *
H5VL_pass_through_wrap_object(void *obj, void *_wrap_ctx)
{
    H5VL_pass_through_wrap_ctx_t *wrap_ctx = (H5VL_pass_through_wrap_ctx_t *)_wrap_ctx;
    H5VL_pass_through_t *new_obj;
    void *under;

#ifdef ENABLE_VOL_LOG
    printf("------- PASS THROUGH VOL WRAP Object\n");
#endif

    /* Wrap the object with the underlying VOL */
    under = H5VLwrap_object(obj, wrap_ctx->under_vol_id, wrap_ctx->under_wrap_ctx);
    if(under) {
        new_obj = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        new_obj->under_object = under;
        new_obj->under_vol_id = wrap_ctx->under_vol_id;
        H5Iinc_ref(new_obj->under_vol_id);
    } /* end if */
    else
        new_obj = NULL;

    return new_obj;
}

/* ====REPLACE_START==== */
static void *
H5VL_pass_through_attr_create(void *obj, H5VL_loc_params_t loc_params, const char *name,
        hid_t acpl_id, hid_t aapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *attr;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    attr = H5VLattr_create(o->under_object, loc_params, o->under_vol_id, name, acpl_id, aapl_id, dxpl_id, req);
    if(attr) {
        /* attr = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* attr->under_object = under; */
        attr->under_vol_id = o->under_vol_id;
        H5Iinc_ref(attr->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        attr = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Create\n");
#endif
    return (void*)attr;
}

static void *
H5VL_pass_through_attr_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t aapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *attr;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    attr = H5VLattr_open(o->under_object, loc_params, o->under_vol_id, name, aapl_id, dxpl_id, req);
    if(attr) {
        /* attr = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* attr->under_object = under; */
        attr->under_vol_id = o->under_vol_id;
        H5Iinc_ref(attr->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        attr = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Open\n");
#endif
    return (void*)attr;
}

static herr_t 
H5VL_pass_through_attr_read(void *attr, hid_t mem_type_id, void *buf, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)attr;
    herr_t ret_value;

    ret_value = H5VLattr_read(o->under_object, o->under_vol_id, mem_type_id, buf, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Read\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_attr_write(void *attr, hid_t mem_type_id, const void *buf, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)attr;
    herr_t ret_value;

    ret_value = H5VLattr_write(o->under_object, o->under_vol_id, mem_type_id, buf, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Write\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_attr_get(void *obj, H5VL_attr_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLattr_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Get\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_attr_specific(void *obj, H5VL_loc_params_t loc_params, H5VL_attr_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLattr_specific(o->under_object, loc_params, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Specific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_attr_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLattr_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Optional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_attr_close(void *attr, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)attr;
    herr_t ret_value;

    ret_value = H5VLattr_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Attr Close\n");
#endif
    return ret_value;
}

static void *
H5VL_pass_through_dataset_create(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t dcpl_id, hid_t dapl_id, hid_t dxpl_id, void **req) 
{
    H5VL_pass_through_t *dset;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    dset = H5VLdataset_create(o->under_object, loc_params, o->under_vol_id, name, dcpl_id,  dapl_id, dxpl_id, req);
    if(dset) {
        /* dset = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* dset->under_object = under; */
        dset->under_vol_id = o->under_vol_id;
        H5Iinc_ref(dset->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        dset = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dcreate\n");
#endif
    return (void *)dset;
}

static void *
H5VL_pass_through_dataset_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t dapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *dset;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    dset = H5VLdataset_open(o->under_object, loc_params, o->under_vol_id, name, dapl_id, dxpl_id, req);
    if(dset) {
        /* dset = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* dset->under_object = under; */
        dset->under_vol_id = o->under_vol_id;
        H5Iinc_ref(dset->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        dset = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dopen\n");
#endif
    return (void *)dset;
}

static herr_t 
H5VL_pass_through_dataset_read(void *dset, hid_t mem_type_id, hid_t mem_space_id,
    hid_t file_space_id, hid_t plist_id, void *buf, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dset;
    herr_t ret_value;

    ret_value = H5VLdataset_read(o->under_object, o->under_vol_id, mem_type_id, mem_space_id, file_space_id, plist_id, buf, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dread\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_dataset_write(void *dset, hid_t mem_type_id, hid_t mem_space_id,
    hid_t file_space_id, hid_t plist_id, const void *buf, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dset;
    herr_t ret_value;

    ret_value = H5VLdataset_write(o->under_object, o->under_vol_id, mem_type_id, mem_space_id, file_space_id, plist_id, buf, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dwrite\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_dataset_get(void *dset, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dset;
    herr_t ret_value;

    ret_value = H5VLdataset_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dget\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_dataset_specific(void *obj, H5VL_dataset_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLdataset_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dspecific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_dataset_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLdataset_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Doptional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_dataset_close(void *dset, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dset;
    herr_t ret_value;

    ret_value = H5VLdataset_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Dclose\n");
#endif
    return ret_value;
}

static void *
H5VL_pass_through_datatype_commit(void *obj, H5VL_loc_params_t loc_params, const char *name, 
                         hid_t type_id, hid_t lcpl_id, hid_t tcpl_id, hid_t tapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *dt;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    dt = H5VLdatatype_commit(o->under_object, loc_params, o->under_vol_id, name, type_id, lcpl_id, tcpl_id, tapl_id, dxpl_id, req);
    if(dt) {
        /* dt = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* dt->under_object = under; */
        dt->under_vol_id = o->under_vol_id;
        H5Iinc_ref(dt->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        dt = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Tcommit\n");
#endif
    return dt;
}

static void *
H5VL_pass_through_datatype_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t tapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *dt;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;  
    /* void *under; */

    dt = H5VLdatatype_open(o->under_object, loc_params, o->under_vol_id, name, tapl_id, dxpl_id, req);
    if(dt) {
        /* dt = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* dt->under_object = under; */
        dt->under_vol_id = o->under_vol_id;
        H5Iinc_ref(dt->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        dt = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Topen\n");
#endif
    return (void *)dt;
}

static herr_t 
H5VL_pass_through_datatype_get(void *dt, H5VL_datatype_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dt;
    herr_t ret_value;

    ret_value = H5VLdatatype_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL datatype get\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_datatype_specific(void *obj, H5VL_datatype_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLdatatype_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL datatype specific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_datatype_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLdatatype_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL datatype optional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_datatype_close(void *dt, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)dt;
    herr_t ret_value;

    assert(o->under_object);

    ret_value = H5VLdatatype_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Tclose\n");
#endif
    return ret_value;
}

static void *
H5VL_pass_through_file_create(const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_info_t *info;
    H5VL_pass_through_t *file;
    hid_t under_fapl_id;
    /* void *under; */

    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(fapl_id, (void **)&info);

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);

    /* Open the file with the underlying VOL connector */
    file = H5VLfile_create(name, flags, fcpl_id, under_fapl_id, dxpl_id, req);
    if(file) {
        /* file = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* file->under_object = under; */
        file->under_vol_id = info->under_vol_id;
        H5Iinc_ref(file->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = info->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        file = NULL;

    /* Close underlying FAPL */
    H5Pclose(under_fapl_id);

    /* Release copy of our VOL info */
    H5VL_pass_through_info_free(info);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Fcreate\n");
#endif
    return (void *)file;
}

static void *
H5VL_pass_through_file_open(const char *name, unsigned flags, hid_t fapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_info_t *info;
    H5VL_pass_through_t *file;
    hid_t under_fapl_id;
    /* void *under; */

    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(fapl_id, (void **)&info);

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);

    /* Open the file with the underlying VOL connector */
    file = H5VLfile_open(name, flags, under_fapl_id, dxpl_id, req);
    if(file) {
        /* file = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* file->under_object = under; */
        file->under_vol_id = info->under_vol_id;
        H5Iinc_ref(file->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = info->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        file = NULL;

    /* Close underlying FAPL */
    H5Pclose(under_fapl_id);

    /* Release copy of our VOL info */
    H5VL_pass_through_info_free(info);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Fopen\n");
#endif
    return (void *)file;
}

static herr_t 
H5VL_pass_through_file_get(void *file, H5VL_file_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)file;
    herr_t ret_value;

    ret_value = H5VLfile_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Fget %d\n", get_type);
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_file_specific_reissue(void *obj, hid_t connector_id,
    H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, ...)
{
    va_list arguments;
    herr_t ret_value;

    va_start(arguments, req);
    ret_value = H5VLfile_specific(obj, connector_id, specific_type, dxpl_id, req, arguments);
    va_end(arguments);

    return ret_value;
}

static herr_t 
H5VL_pass_through_file_specific(void *file, H5VL_file_specific_t specific_type,
    hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)file;
    hid_t under_vol_id = -1;
    herr_t ret_value;

    /* Unpack arguments to get at the child file pointer when mounting a file */
    if(specific_type == H5VL_FILE_MOUNT) {
        H5I_type_t loc_type;
        const char *name;
        H5VL_pass_through_t *child_file;
        hid_t plist_id;

        /* Retrieve parameters for 'mount' operation, so we can unwrap the child file */
        loc_type = va_arg(arguments, H5I_type_t);
        name = va_arg(arguments, const char *);
        child_file = (H5VL_pass_through_t *)va_arg(arguments, void *);
        plist_id = va_arg(arguments, hid_t);

        /* Re-issue 'file specific' call, using the unwrapped pieces */
        under_vol_id = o->under_vol_id;
        ret_value = H5VL_pass_through_file_specific_reissue(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, loc_type, name, child_file->under_object, plist_id);
    } /* end if */
    else if(specific_type == H5VL_FILE_IS_ACCESSIBLE) {
        H5VL_pass_through_info_t *info;
        hid_t fapl_id, under_fapl_id;
        const char *name;
        htri_t *ret;

        /* Get the arguments for the 'is accessible' check */
        fapl_id = va_arg(arguments, hid_t);
        name    = va_arg(arguments, const char *);
        ret     = va_arg(arguments, htri_t *);

        /* Get copy of our VOL info from FAPL */
        H5Pget_vol_info(fapl_id, (void **)&info);

        /* Copy the FAPL */
        under_fapl_id = H5Pcopy(fapl_id);

        /* Set the VOL ID and info for the underlying FAPL */
        H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);

        /* Re-issue 'file specific' call */
        under_vol_id = info->under_vol_id;
        ret_value = H5VL_pass_through_file_specific_reissue(NULL, info->under_vol_id, specific_type, dxpl_id, req, under_fapl_id, name, ret);

        /* Close underlying FAPL */
        H5Pclose(under_fapl_id);

        /* Release copy of our VOL info */
        H5VL_pass_through_info_free(info);
    } /* end else-if */
    else {
        va_list my_arguments;

        /* Make a copy of the argument list for later, if reopening */
        if(specific_type == H5VL_FILE_REOPEN)
            va_copy(my_arguments, arguments);

        under_vol_id = o->under_vol_id;
        ret_value = H5VLfile_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

        /* Wrap file struct pointer, if we reopened one */
        if(specific_type == H5VL_FILE_REOPEN) {
            if(ret_value >= 0) {
                void      **ret = va_arg(my_arguments, void **);

                if(ret && *ret) {
                    H5VL_pass_through_t *new_file;

                    new_file = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t));
                    new_file->under_object = *ret;
                    new_file->under_vol_id = o->under_vol_id;
                    H5Iinc_ref(new_file->under_vol_id);

                    *ret = new_file;
                } /* end if */
            } /* end if */

            /* Finish use of copied vararg list */
            va_end(my_arguments);
        } /* end if */
    } /* end else */

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        assert(under_vol_id);
        r->under_vol_id = under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Fspecific %d\n", specific_type);
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_file_optional(void *file, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)file;
    herr_t ret_value;

    ret_value = H5VLfile_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL File Optional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_file_close(void *file, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)file;
    herr_t ret_value;

    ret_value = H5VLfile_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Fclose\n");
#endif
    return ret_value;
}

static void *
H5VL_pass_through_group_create(void *obj, H5VL_loc_params_t loc_params, const char *name, 
                      hid_t gcpl_id, hid_t gapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *group;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    group = H5VLgroup_create(o->under_object, loc_params, o->under_vol_id, name, gcpl_id,  gapl_id, dxpl_id, req);
    if(group) {
        /* group = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* group->under_object = under; */
        group->under_vol_id = o->under_vol_id;
        H5Iinc_ref(group->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        group = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Gcreate\n");
#endif
    return (void *)group;
}

static void *
H5VL_pass_through_group_open(void *obj, H5VL_loc_params_t loc_params, const char *name, hid_t gapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *group;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    group = H5VLgroup_open(o->under_object, loc_params, o->under_vol_id, name, gapl_id, dxpl_id, req);
    if(group) {
        /* group = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* group->under_object = under; */
        group->under_vol_id = o->under_vol_id;
        H5Iinc_ref(group->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        group = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Gopen\n");
#endif
    return (void *)group;
}

static herr_t 
H5VL_pass_through_group_get(void *obj, H5VL_group_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLgroup_get(o->under_object, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Gget\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_group_specific(void *obj, H5VL_group_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLgroup_specific(o->under_object, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Gspecific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_group_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLgroup_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Goptional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_group_close(void *grp, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)grp;
    herr_t ret_value;

    ret_value = H5VLgroup_close(o->under_object, o->under_vol_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Gclose\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_link_create(H5VL_link_create_type_t create_type, void *obj, H5VL_loc_params_t loc_params, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    hid_t under_vol_id = -1;
    herr_t ret_value;

    /* Try to retrieve the "under" VOL id */
    if(o)
        under_vol_id = o->under_vol_id;

    /* Fix up the link target object for hard link creation */
    if(H5VL_LINK_CREATE_HARD == create_type) {
        void         *cur_obj;

        /* Retrieve the object for the link target */
        H5Pget(lcpl_id, H5VL_PROP_LINK_TARGET, &cur_obj);

        /* If it's a non-NULL pointer, find the 'under object' and re-set the property */
        if(cur_obj) {
            /* Check if we still need the "under" VOL ID */
            if(under_vol_id < 0)
                under_vol_id = ((H5VL_pass_through_t *)cur_obj)->under_vol_id;

            /* Set the object for the link target */
            H5Pset(lcpl_id, H5VL_PROP_LINK_TARGET, &(((H5VL_pass_through_t *)cur_obj)->under_object));
        } /* end if */
    } /* end if */

    ret_value = H5VLlink_create(create_type, (o ? o->under_object : NULL), loc_params, under_vol_id, lcpl_id, lapl_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Create\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_link_copy(void *src_obj, H5VL_loc_params_t loc_params1, void *dst_obj, H5VL_loc_params_t loc_params2, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o_src = (H5VL_pass_through_t *)src_obj;
    H5VL_pass_through_t *o_dst = (H5VL_pass_through_t *)dst_obj;
    hid_t under_vol_id = -1;
    herr_t ret_value;

    /* Retrieve the "under" VOL id */
    if(o_src)
        under_vol_id = o_src->under_vol_id;
    else if(o_dst)
        under_vol_id = o_dst->under_vol_id;
    assert(under_vol_id > 0);

    ret_value = H5VLlink_copy((o_src ? o_src->under_object : NULL), loc_params1, (o_dst ? o_dst->under_object : NULL), loc_params2, under_vol_id, lcpl_id, lapl_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }
            
#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Copy\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_link_move(void *src_obj, H5VL_loc_params_t loc_params1,
    void *dst_obj, H5VL_loc_params_t loc_params2, hid_t lcpl_id, hid_t lapl_id,
    hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o_src = (H5VL_pass_through_t *)src_obj;
    H5VL_pass_through_t *o_dst = (H5VL_pass_through_t *)dst_obj;
    hid_t under_vol_id = -1;
    herr_t ret_value;

    /* Retrieve the "under" VOL id */
    if(o_src)
        under_vol_id = o_src->under_vol_id;
    else if(o_dst)
        under_vol_id = o_dst->under_vol_id;
    assert(under_vol_id > 0);

    ret_value = H5VLlink_move((o_src ? o_src->under_object : NULL), loc_params1, (o_dst ? o_dst->under_object : NULL), loc_params2, under_vol_id, lcpl_id, lapl_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Move\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_link_get(void *obj, H5VL_loc_params_t loc_params,
    H5VL_link_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLlink_get(o->under_object, loc_params, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }
            
#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Get\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_link_specific(void *obj, H5VL_loc_params_t loc_params, 
    H5VL_link_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLlink_specific(o->under_object, loc_params, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Specific\n");
#endif
    return ret_value;
}

static herr_t
H5VL_pass_through_link_optional(void *obj, hid_t dxpl_id,
    void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLlink_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Link Optional\n");
#endif
    return ret_value;
}

static void *
H5VL_pass_through_object_open(void *obj, H5VL_loc_params_t loc_params, H5I_type_t *opened_type, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *new_obj;
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    /* void *under; */

    new_obj = H5VLobject_open(o->under_object, loc_params, o->under_vol_id, opened_type, dxpl_id, req);
    if(new_obj) {
        /* new_obj = (H5VL_pass_through_t *)calloc(1, sizeof(H5VL_pass_through_t)); */
        /* new_obj->under_object = under; */
        new_obj->under_vol_id = o->under_vol_id;
        H5Iinc_ref(new_obj->under_vol_id);

        /* Check for async request */
        if(req && *req) {
            H5VL_pass_through_t *r;

            r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
            r->under_object = *req;
            r->under_vol_id = o->under_vol_id;
            H5Iinc_ref(r->under_vol_id);

            *req = r;
        }
    }
    else
        new_obj = NULL;

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL H5Oopen\n");
#endif
    return (void *)new_obj;
}

static herr_t 
H5VL_pass_through_object_copy(void *src_obj, H5VL_loc_params_t src_loc_params, const char *src_name, void *dst_obj, H5VL_loc_params_t dst_loc_params, const char *dst_name, hid_t ocpypl_id, hid_t lcpl_id, hid_t dxpl_id, void **req)
{
    H5VL_pass_through_t *o_src = (H5VL_pass_through_t *)src_obj;
    H5VL_pass_through_t *o_dst = (H5VL_pass_through_t *)dst_obj;
    herr_t ret_value;

    ret_value = H5VLobject_copy(o_src->under_object, src_loc_params, src_name, o_dst->under_object, dst_loc_params, dst_name, o_src->under_vol_id, ocpypl_id, lcpl_id, dxpl_id, req);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o_src->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Object Copy\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_object_get(void *obj, H5VL_loc_params_t loc_params, H5VL_object_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLobject_get(o->under_object, loc_params, o->under_vol_id, get_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Object Get\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_object_specific(void *obj, H5VL_loc_params_t loc_params, H5VL_object_specific_t specific_type, 
                         hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLobject_specific(o->under_object, loc_params, o->under_vol_id, specific_type, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Object Specific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_object_optional(void *obj, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLobject_optional(o->under_object, o->under_vol_id, dxpl_id, req, arguments);

    /* Check for async request */
    if(req && *req) {
        H5VL_pass_through_t *r;

        r = (H5VL_pass_through_t*)calloc(1, sizeof(H5VL_pass_through_t));
        r->under_object = *req;
        r->under_vol_id = o->under_vol_id;
        H5Iinc_ref(r->under_vol_id);

        *req = r;
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Object Optional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_request_wait(void *obj, uint64_t timeout, H5ES_status_t *status)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLrequest_wait(o->under_object, o->under_vol_id, timeout, status);

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Async Wait\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_request_cancel(void *obj)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLrequest_cancel(o->under_object, o->under_vol_id);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Async Cancel\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_request_specific_reissue(void *obj, hid_t connector_id,
    H5VL_request_specific_t specific_type, ...)
{
    va_list arguments;
    herr_t ret_value;

    va_start(arguments, specific_type);
    ret_value = H5VLrequest_specific(obj, connector_id, specific_type, arguments);
    va_end(arguments);

    return ret_value;
}

static herr_t 
H5VL_pass_through_request_specific(void *obj, H5VL_request_specific_t specific_type, 
    va_list arguments)
{
    herr_t ret_value = -1;

    if(H5VL_REQUEST_WAITANY == specific_type ||
            H5VL_REQUEST_WAITSOME == specific_type ||
            H5VL_REQUEST_WAITALL == specific_type) {
        va_list tmp_arguments;
        size_t req_count;

        /* Sanity check */
        assert(obj == NULL);

        /* Get enough info to call the underlying connector */
        va_copy(tmp_arguments, arguments);
        req_count = va_arg(tmp_arguments, size_t);

        /* Can only get a request to invoke the underlying VOL connector when there's >0 requests */
        if(req_count > 0) {
            void **req_array;
            void **under_req_array;
            uint64_t timeout;
            H5VL_pass_through_t *o;
            size_t u;               /* Local index variable */

            /* Get the request array */
            req_array = va_arg(tmp_arguments, void **);

            /* Get a request to use for determining the underlying VOL connector */
            o = (H5VL_pass_through_t *)req_array[0];

            /* Create array of underlying VOL requests */
            under_req_array = (void **)malloc(req_count * sizeof(void **));
            for(u = 0; u < req_count; u++)
                under_req_array[u] = ((H5VL_pass_through_t *)req_array[u])->under_object;

            /* Remove the timeout value from the vararg list (it's used in all the calls below) */
            timeout = va_arg(tmp_arguments, uint64_t);

            /* Release requests that have completed */
            if(H5VL_REQUEST_WAITANY == specific_type) {
                H5VL_pass_through_t *tmp_o;
                size_t *index;          /* Pointer to the index of completed request */
                H5ES_status_t *status;  /* Pointer to the request's status */

                /* Retrieve the remaining arguments */
                index = va_arg(tmp_arguments, size_t *);
                assert(*index <= req_count);
                status = va_arg(tmp_arguments, H5ES_status_t *);

                /* Reissue the WAITANY 'request specific' call */
                ret_value = H5VL_pass_through_request_specific_reissue(o->under_object, o->under_vol_id, specific_type, req_count, under_req_array, timeout, index, status);

                /* Release the completed request */
                tmp_o = (H5VL_pass_through_t *)req_array[*index];
                H5Idec_ref(tmp_o->under_vol_id);
                free(tmp_o);
            }
            else if(H5VL_REQUEST_WAITSOME == specific_type) {
                size_t *outcount;               /* # of completed requests */
                unsigned *array_of_indices;     /* Array of indices for completed requests */
                H5ES_status_t *array_of_statuses; /* Array of statuses for completed requests */

                /* Retrieve the remaining arguments */
                outcount = va_arg(tmp_arguments, size_t *);
                assert(*outcount <= req_count);
                array_of_indices = va_arg(tmp_arguments, unsigned *);
                array_of_statuses = va_arg(tmp_arguments, H5ES_status_t *);

                /* Reissue the WAITSOME 'request specific' call */
                ret_value = H5VL_pass_through_request_specific_reissue(o->under_object, o->under_vol_id, specific_type, req_count, under_req_array, timeout, outcount, array_of_indices, array_of_statuses);

                /* If any requests completed, release them */
                if(*outcount > 0) {
                    unsigned *idx_array;    /* Array of indices of completed requests */

                    /* Retrieve the array of completed request indices */
                    idx_array = va_arg(tmp_arguments, unsigned *);

                    /* Release the completed requests */
                    for(u = 0; u < *outcount; u++) {
                        H5VL_pass_through_t *tmp_o;

                        tmp_o = (H5VL_pass_through_t *)req_array[idx_array[u]];
                        H5Idec_ref(tmp_o->under_vol_id);
                        free(tmp_o);
                    }
                }
            }
            else {      /* H5VL_REQUEST_WAITALL == specific_type */
                H5ES_status_t *array_of_statuses; /* Array of statuses for completed requests */

                /* Retrieve the remaining arguments */
                array_of_statuses = va_arg(tmp_arguments, H5ES_status_t *);

                /* Reissue the WAITALL 'request specific' call */
                ret_value = H5VL_pass_through_request_specific_reissue(o->under_object, o->under_vol_id, specific_type, req_count, under_req_array, timeout, array_of_statuses);

                /* Release the completed requests */
                for(u = 0; u < req_count; u++) {
                    H5VL_pass_through_t *tmp_o;

                    tmp_o = (H5VL_pass_through_t *)req_array[u];
                    H5Idec_ref(tmp_o->under_vol_id);
                    free(tmp_o);
                }
            }
        }

        /* Finish use of copied vararg list */
        va_end(tmp_arguments);
    }

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Async Specific\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_request_optional(void *obj, va_list arguments)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLrequest_optional(o->under_object, o->under_vol_id, arguments);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Async Optional\n");
#endif
    return ret_value;
}

static herr_t 
H5VL_pass_through_request_free(void *obj)
{
    H5VL_pass_through_t *o = (H5VL_pass_through_t *)obj;
    herr_t ret_value;

    ret_value = H5VLrequest_free(o->under_object, o->under_vol_id);

    H5Idec_ref(o->under_vol_id);
    free(o);

#ifdef ENABLE_VOL_LOG 
    printf("------- PASS THROUGH VOL Async Free\n");
#endif
    return ret_value;
}

