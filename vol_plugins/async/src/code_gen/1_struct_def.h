
typedef enum {QTYPE_NONE, REGULAR, DEPENDENT, COLLECTIVE} task_list_qtype;
typedef enum {OP_NONE, READ, WRITE} obj_op_type;

const char* qtype_names_g[4] = {"QTYPE_NONE", "REGULAR", "DEPENDENT", "COLLECTIVE"};

typedef struct async_task_t {
    ABT_mutex           task_mutex;
    void                *h5_state;
    void                (*func)(void *);
    void                *args;
    obj_op_type         op;
    hid_t               under_vol_id;
    struct H5VL_async_t *async_obj;         
    ABT_eventual        eventual;
    int                 in_abt_pool;
    int                 is_done;
    ABT_task            abt_task;
    ABT_thread          abt_thread;

    int                 n_dep;
    int                 n_dep_alloc;
    struct async_task_t **dep_tasks;

    struct H5VL_async_t *parent_obj;            /* pointer back to the parent async object */

    struct H5RQ_token_int_t *token;

    struct async_task_t *prev;
    struct async_task_t *next;
    struct async_task_t *file_list_prev;
    struct async_task_t *file_list_next;
} async_task_t; 

typedef struct H5VL_async_t {
    int                 magic;
    void                *under_object;
    hid_t               under_vol_id;
    int                 is_obj_valid;
    async_task_t        *create_task;           /* task that creates the object */
    async_task_t        *file_task_list_head;
    ABT_mutex           file_task_list_mutex;
    struct H5VL_async_t *file_async_obj;
    int                 task_cnt;
    int                 attempt_check_cnt;
    ABT_mutex           obj_mutex;
    ABT_pool            *pool_ptr;
    hbool_t             is_col_meta;
    hbool_t             is_raw_io_delay;
    // To make dset write not requiring dset create to complete (still requries dset open to complete)
    hbool_t             has_dset_size;
    hsize_t             dtype_size;
    hsize_t             dset_size;
} H5VL_async_t;

typedef struct async_task_list_t {
    task_list_qtype     type;
    async_task_t        *task_list;

    struct async_task_list_t *prev;
    struct async_task_list_t *next;
} async_task_list_t;

typedef struct async_qhead_t {
    ABT_mutex           head_mutex;
    async_task_list_t   *queue;
} async_qhead_t;

typedef struct async_instance_t {
    async_qhead_t       qhead;
    ABT_pool            pool;
    int                 num_xstreams;
    ABT_xstream         *xstreams;
    ABT_xstream         *scheds;
    int                 nfopen;
    bool                env_async;           /* Async VOL is enabled by env var */
    bool                ex_delay;            /* Delay background thread execution */
    bool                ex_fclose;           /* Delay background thread execution until file close */
    bool                ex_gclose;           /* Delay background thread execution until group close */
    bool                ex_dclose;           /* Delay background thread execution until dset close */
    bool                start_abt_push;      /* Start pushing tasks to Argobots pool */
} async_instance_t;

typedef struct H5RQ_token_int_t {
    struct async_task_t *task;
} H5RQ_token_int_t;
