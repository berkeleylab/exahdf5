static void
async_attr_create_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_create_args_t *args = (async_attr_create_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLattr_create(args->obj, args->loc_params, task->under_vol_id, args->name, args->type_id, args->space_id, args->acpl_id, args->aapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_create failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->type_id > 0)    H5Tclose(args->type_id);
    if(args->space_id > 0)    H5Sclose(args->space_id);
    if(args->acpl_id > 0)    H5Pclose(args->acpl_id);
    if(args->aapl_id > 0)    H5Pclose(args->aapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_create_fn

static H5VL_async_t*
async_attr_create(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t type_id, hid_t space_id, hid_t acpl_id, hid_t aapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_create_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_create_args_t*)calloc(1, sizeof(async_attr_create_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(type_id > 0)
        args->type_id = H5Tcopy(type_id);
    if(space_id > 0)
        args->space_id = H5Scopy(space_id);
    if(acpl_id > 0)
        args->acpl_id = H5Pcopy(acpl_id);
    if(aapl_id > 0)
        args->aapl_id = H5Pcopy(aapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_create_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_attr_create



static void
async_attr_open_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_open_args_t *args = (async_attr_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLattr_open(args->obj, args->loc_params, task->under_vol_id, args->name, args->aapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->aapl_id > 0)    H5Pclose(args->aapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_open_fn

static H5VL_async_t*
async_attr_open(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t aapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_open_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_open_args_t*)calloc(1, sizeof(async_attr_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(aapl_id > 0)
        args->aapl_id = H5Pcopy(aapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_attr_open



static void
async_attr_read_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_read_args_t *args = (async_attr_read_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->attr) {
        if (NULL != task->parent_obj->under_object) {
            args->attr = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLattr_read(args->attr, task->under_vol_id, args->mem_type_id, args->buf, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_read failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->mem_type_id > 0)    H5Tclose(args->mem_type_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_read_fn

static herr_t
async_attr_read(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t mem_type_id, void *buf, hid_t dxpl_id, void **req)
{
    // For implicit mode (env var), make all read to be blocking
    if(aid->env_async) is_blocking = 1;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_read_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_read_args_t*)calloc(1, sizeof(async_attr_read_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->attr             = parent_obj->under_object;
    if(mem_type_id > 0)
        args->mem_type_id = H5Tcopy(mem_type_id);
    args->buf              = buf;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_read_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_read



static void
async_attr_write_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_write_args_t *args = (async_attr_write_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->attr) {
        if (NULL != task->parent_obj->under_object) {
            args->attr = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLattr_write(args->attr, task->under_vol_id, args->mem_type_id, args->buf, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_write failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->mem_type_id > 0)    H5Tclose(args->mem_type_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    free(args->buf);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_write_fn

static herr_t
async_attr_write(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t mem_type_id, const void *buf, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_write_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_write_args_t*)calloc(1, sizeof(async_attr_write_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->attr             = parent_obj->under_object;
    if(mem_type_id > 0)
        args->mem_type_id = H5Tcopy(mem_type_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }

    if (NULL == (args->buf = malloc(ASYNC_VOL_ATTR_CP_SIZE_LIMIT))) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s malloc failed!\n", __func__);
        goto done;
    }
    memcpy(args->buf, buf, ASYNC_VOL_ATTR_CP_SIZE_LIMIT);

    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_write_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_write



static void
async_attr_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_get_args_t *args = (async_attr_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLattr_get(args->obj, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_get_fn

static herr_t
async_attr_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_attr_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_get_args_t*)calloc(1, sizeof(async_attr_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_get



static void
async_attr_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_specific_args_t *args = (async_attr_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLattr_specific(args->obj, args->loc_params, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_specific_fn

static herr_t
async_attr_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5VL_attr_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_specific_args_t*)calloc(1, sizeof(async_attr_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_specific



static void
async_attr_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_optional_args_t *args = (async_attr_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLattr_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_optional_fn

static herr_t
async_attr_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_attr_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_optional_args_t*)calloc(1, sizeof(async_attr_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_optional



static void
async_attr_close_fn(void *foo)
{
    herr_t ret_value;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_attr_close_args_t *args = (async_attr_close_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->attr) {
        if (NULL != task->parent_obj->under_object) {
            args->attr = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( (ret_value = H5VLattr_close(args->attr, task->under_vol_id, args->dxpl_id, args->req)) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLattr_close failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_attr_close_fn

static herr_t
async_attr_close(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_attr_close_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_attr_close_args_t*)calloc(1, sizeof(async_attr_close_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->attr             = parent_obj->under_object;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_attr_close_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    else {
            if (get_n_running_task_in_queue(async_task) == 0)
                push_task_to_abt_pool(&aid->qhead, aid->pool);

    }

    aid->start_abt_push = true;
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_attr_close



static void
async_dataset_create_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_create_args_t *args = (async_dataset_create_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLdataset_create(args->obj, args->loc_params, task->under_vol_id, args->name, args->lcpl_id, args->type_id, args->space_id, args->dcpl_id, args->dapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_create failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->type_id > 0)    H5Tclose(args->type_id);
    if(args->space_id > 0)    H5Sclose(args->space_id);
    if(args->dcpl_id > 0)    H5Pclose(args->dcpl_id);
    if(args->dapl_id > 0)    H5Pclose(args->dapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_create_fn

static H5VL_async_t*
async_dataset_create(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t lcpl_id, hid_t type_id, hid_t space_id, hid_t dcpl_id, hid_t dapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_create_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_create_args_t*)calloc(1, sizeof(async_dataset_create_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(type_id > 0)
        args->type_id = H5Tcopy(type_id);
    if(space_id > 0)
        args->space_id = H5Scopy(space_id);
    if(dcpl_id > 0)
        args->dcpl_id = H5Pcopy(dcpl_id);
    if(dapl_id > 0)
        args->dapl_id = H5Pcopy(dapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_create_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;
    async_obj->dtype_size = H5Tget_size(type_id);
    async_obj->dset_size = async_obj->dtype_size * H5Sget_simple_extent_npoints(space_id);
    if (async_obj->dset_size > 0)
        async_obj->has_dset_size = true;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_dataset_create



static void
async_dataset_open_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_open_args_t *args = (async_dataset_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLdataset_open(args->obj, args->loc_params, task->under_vol_id, args->name, args->dapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->dapl_id > 0)    H5Pclose(args->dapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_open_fn

static H5VL_async_t*
async_dataset_open(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t dapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_open_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_open_args_t*)calloc(1, sizeof(async_dataset_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(dapl_id > 0)
        args->dapl_id = H5Pcopy(dapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_dataset_open



static void
async_dataset_read_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_read_args_t *args = (async_dataset_read_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dset) {
        if (NULL != task->parent_obj->under_object) {
            args->dset = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdataset_read(args->dset, task->under_vol_id, args->mem_type_id, args->mem_space_id, args->file_space_id, args->plist_id, args->buf, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_read failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->mem_type_id > 0)    H5Tclose(args->mem_type_id);
    if(args->mem_space_id > 0)    H5Sclose(args->mem_space_id);
    if(args->file_space_id > 0)    H5Sclose(args->file_space_id);
    if(args->plist_id > 0)    H5Pclose(args->plist_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_read_fn

static herr_t
async_dataset_read(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t plist_id, void *buf, void **req)
{
    // For implicit mode (env var), make all read to be blocking
    if(aid->env_async) is_blocking = 1;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_read_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_read_args_t*)calloc(1, sizeof(async_dataset_read_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dset             = parent_obj->under_object;
    if(mem_type_id > 0)
        args->mem_type_id = H5Tcopy(mem_type_id);
    if(mem_space_id > 0)
        args->mem_space_id = H5Scopy(mem_space_id);
    if(file_space_id > 0)
        args->file_space_id = H5Scopy(file_space_id);
    if(plist_id > 0)
        args->plist_id = H5Pcopy(plist_id);
    args->buf              = buf;
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_read_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        H5FD_mpio_xfer_t xfer_mode;
        H5Pget_dxpl_mpio(plist_id, &xfer_mode);
        if (xfer_mode == H5FD_MPIO_COLLECTIVE) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_read



static void
async_dataset_write_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_write_args_t *args = (async_dataset_write_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dset) {
        if (NULL != task->parent_obj->under_object) {
            args->dset = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdataset_write(args->dset, task->under_vol_id, args->mem_type_id, args->mem_space_id, args->file_space_id, args->plist_id, args->buf, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_write failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->mem_type_id > 0)    H5Tclose(args->mem_type_id);
    if(args->mem_space_id > 0)    H5Sclose(args->mem_space_id);
    if(args->file_space_id > 0)    H5Sclose(args->file_space_id);
    if(args->plist_id > 0)    H5Pclose(args->plist_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    if (args->buf_free == true) free(args->buf);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_write_fn

static herr_t
async_dataset_write(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t mem_type_id, hid_t mem_space_id, hid_t file_space_id, hid_t plist_id, const void *buf, void **req)
{
    hbool_t enable_async = false;
    hsize_t buf_size, cp_size_limit = 0;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_write_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_write_args_t*)calloc(1, sizeof(async_dataset_write_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    if (H5Pget_dxpl_async(plist_id, &enable_async) < 0) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5Pget_dxpl_async\n", __func__);
        goto error;
    }
    if (H5Pget_dxpl_async_cp_limit(plist_id, &cp_size_limit) < 0) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with H5Pget_dxpl_async\n", __func__);
        goto error;
    }
    if (cp_size_limit > 0) { enable_async = true; }
    if (aid->env_async) enable_async = true;
    if (enable_async == true && cp_size_limit == 0) cp_size_limit = ULONG_MAX;

    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dset             = parent_obj->under_object;
    if(mem_type_id > 0)
        args->mem_type_id = H5Tcopy(mem_type_id);
    if(mem_space_id > 0)
        args->mem_space_id = H5Scopy(mem_space_id);
    if(file_space_id > 0)
        args->file_space_id = H5Scopy(file_space_id);
    if(plist_id > 0)
        args->plist_id = H5Pcopy(plist_id);
    args->buf              = (void*)buf;
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
            cp_size_limit = 0;
        }
    }

    if (cp_size_limit > 0) { 
        if (parent_obj->has_dset_size && args->file_space_id == H5S_ALL && parent_obj->dset_size > 0) { 
            buf_size = parent_obj->dset_size;
        }
        else { 
            if (parent_obj->dtype_size > 0) {
                if (H5VLasync_get_data_nelem(args->dset, args->file_space_id, parent_obj->under_vol_id, &buf_size) < 0) {
                    fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLasync_get_data_nelem failed\n", __func__);
                    goto done;
                }
                buf_size *= parent_obj->dtype_size;
            }
            else
                if (H5VLasync_get_data_size(args->dset, args->file_space_id, parent_obj->under_vol_id, &buf_size) < 0) {
                    fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLasync_get_data_size failed\n", __func__);
                    goto done;
                }
        }
        if (buf_size <= cp_size_limit) { 
            if (NULL == (args->buf = malloc(buf_size))) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s malloc failed!\n", __func__);
                goto done;
            }
            memcpy(args->buf, buf, buf_size);
            args->buf_free = true;
        }
        else { 
            enable_async = false;
            fprintf(stdout,"  [ASYNC VOL] %s buf size [%llu] is larger than cp_size_limit [%llu], using synchronous write\n", __func__, buf_size, cp_size_limit);
        } 
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_write_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        H5FD_mpio_xfer_t xfer_mode;
        H5Pget_dxpl_mpio(plist_id, &xfer_mode);
        if (xfer_mode == H5FD_MPIO_COLLECTIVE) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    if (is_blocking == 1 ||enable_async == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_write



static void
async_dataset_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_get_args_t *args = (async_dataset_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dset) {
        if (NULL != task->parent_obj->under_object) {
            args->dset = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdataset_get(args->dset, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_get_fn

static herr_t
async_dataset_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_dataset_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_get_args_t*)calloc(1, sizeof(async_dataset_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dset             = parent_obj->under_object;
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_get



static void
async_dataset_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_specific_args_t *args = (async_dataset_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdataset_specific(args->obj, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_specific_fn

static herr_t
async_dataset_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_dataset_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_specific_args_t*)calloc(1, sizeof(async_dataset_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_specific



static void
async_dataset_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_optional_args_t *args = (async_dataset_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdataset_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_optional_fn

static herr_t
async_dataset_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_dataset_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_optional_args_t*)calloc(1, sizeof(async_dataset_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_optional



static void
async_dataset_close_fn(void *foo)
{
    herr_t ret_value;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_dataset_close_args_t *args = (async_dataset_close_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dset) {
        if (NULL != task->parent_obj->under_object) {
            args->dset = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( (ret_value = H5VLdataset_close(args->dset, task->under_vol_id, args->dxpl_id, args->req)) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdataset_close failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_dataset_close_fn

static herr_t
async_dataset_close(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_dataset_close_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_dataset_close_args_t*)calloc(1, sizeof(async_dataset_close_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dset             = parent_obj->under_object;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_dataset_close_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    else {
        if (aid->ex_dclose) {
            if (get_n_running_task_in_queue(async_task) == 0)
                push_task_to_abt_pool(&aid->qhead, aid->pool);

        }
    }

    aid->start_abt_push = true;
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_dataset_close



static void
async_datatype_commit_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_commit_args_t *args = (async_datatype_commit_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdatatype_commit(args->obj, args->loc_params, task->under_vol_id, args->name, args->type_id, args->lcpl_id, args->tcpl_id, args->tapl_id, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_commit failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->type_id > 0)    H5Tclose(args->type_id);
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->tcpl_id > 0)    H5Pclose(args->tcpl_id);
    if(args->tapl_id > 0)    H5Pclose(args->tapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_commit_fn

static H5VL_async_t*
async_datatype_commit(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t type_id, hid_t lcpl_id, hid_t tcpl_id, hid_t tapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_commit_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_commit_args_t*)calloc(1, sizeof(async_datatype_commit_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(type_id > 0)
        args->type_id = H5Tcopy(type_id);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(tcpl_id > 0)
        args->tcpl_id = H5Pcopy(tcpl_id);
    if(tapl_id > 0)
        args->tapl_id = H5Pcopy(tapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_commit_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_datatype_commit



static void
async_datatype_open_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_open_args_t *args = (async_datatype_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLdatatype_open(args->obj, args->loc_params, task->under_vol_id, args->name, args->tapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->tapl_id > 0)    H5Pclose(args->tapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_open_fn

static H5VL_async_t*
async_datatype_open(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t tapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_open_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_open_args_t*)calloc(1, sizeof(async_datatype_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(tapl_id > 0)
        args->tapl_id = H5Pcopy(tapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_datatype_open



static void
async_datatype_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_get_args_t *args = (async_datatype_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dt) {
        if (NULL != task->parent_obj->under_object) {
            args->dt = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdatatype_get(args->dt, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_get_fn

static herr_t
async_datatype_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_datatype_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_get_args_t*)calloc(1, sizeof(async_datatype_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dt               = parent_obj->under_object;
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_datatype_get



static void
async_datatype_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_specific_args_t *args = (async_datatype_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdatatype_specific(args->obj, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_specific_fn

static herr_t
async_datatype_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_datatype_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_specific_args_t*)calloc(1, sizeof(async_datatype_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_datatype_specific



static void
async_datatype_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_optional_args_t *args = (async_datatype_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLdatatype_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_optional_fn

static herr_t
async_datatype_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_datatype_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_optional_args_t*)calloc(1, sizeof(async_datatype_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_datatype_optional



static void
async_datatype_close_fn(void *foo)
{
    herr_t ret_value;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_datatype_close_args_t *args = (async_datatype_close_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->dt) {
        if (NULL != task->parent_obj->under_object) {
            args->dt = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( (ret_value = H5VLdatatype_close(args->dt, task->under_vol_id, args->dxpl_id, args->req)) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLdatatype_close failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_datatype_close_fn

static herr_t
async_datatype_close(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_datatype_close_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_datatype_close_args_t*)calloc(1, sizeof(async_datatype_close_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->dt               = parent_obj->under_object;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_datatype_close_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    else {
            if (get_n_running_task_in_queue(async_task) == 0)
                push_task_to_abt_pool(&aid->qhead, aid->pool);

    }

    aid->start_abt_push = true;
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_datatype_close



static void
async_file_create_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    H5VL_async_info_t *info = NULL;
    hid_t under_fapl_id = -1;
    async_task_t *task = (async_task_t*)foo;
    async_file_create_args_t *args = (async_file_create_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    async_instance_g->start_abt_push = false;

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(args->fapl_id, (void **)&info);

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(args->fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    if (info) {
        H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);
    }
    else {
        hid_t under_vol_id;
        H5Pget_vol_id(args->fapl_id, &under_vol_id);
        H5Pset_vol(under_fapl_id, under_vol_id, NULL);
    }
    if ((obj = H5VLfile_create(args->name, args->flags, args->fcpl_id, under_fapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_create failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif

    // Increase file open ref count
    if (ABT_mutex_lock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_lock\n");
        goto done;
    };
    async_instance_g->nfopen++;
    if (ABT_mutex_unlock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_ulock\n");
        goto done;
    };

done:
    fflush(stdout);
    if(under_fapl_id > 0)    H5Pclose(under_fapl_id);
    if(NULL != info)         H5VL_async_info_free(info);
    free(args->name);
    args->name = NULL;
    if(args->fcpl_id > 0)    H5Pclose(args->fcpl_id);
    if(args->fapl_id > 0)    H5Pclose(args->fapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_create_fn

static H5VL_async_t*
async_file_create(int is_blocking, async_instance_t* aid, const char *name, unsigned flags, hid_t fcpl_id, hid_t fapl_id, hid_t dxpl_id, void **req)
{
    hid_t under_vol_id;
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_create_args_t *args = NULL;
    int lock_self;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);

    H5Pget_vol_id(fapl_id, &under_vol_id);

    if ((args = (async_file_create_args_t*)calloc(1, sizeof(async_file_create_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_async_obj      = async_obj;
    if (ABT_mutex_create(&(async_obj->file_task_list_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->name             = strdup(name);
    args->flags            = flags;
    if(fcpl_id > 0)
        args->fcpl_id = H5Pcopy(fcpl_id);
    if(fapl_id > 0)
        args->fapl_id = H5Pcopy(fapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_create_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = under_vol_id;
    async_task->async_obj  = async_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    /* Lock async_obj */
    while (1) {
        if (async_obj->obj_mutex && ABT_mutex_trylock(async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else 
            fprintf(stderr,"  [ASYNC VOL DBG] %s error with try_lock\n", __func__);
        usleep(1000);
    }
    lock_self = 1;

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(async_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    async_obj->task_cnt++;
    async_obj->pool_ptr = &aid->pool;
    H5Pget_coll_metadata_write(fapl_id, &async_obj->is_col_meta);
    add_task_to_queue(&aid->qhead, async_task, REGULAR);
    if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_self = 0;

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (get_n_running_task_in_queue(async_task) == 0)
        push_task_to_abt_pool(&aid->qhead, aid->pool);
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_self == 1) {
        if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL DBG] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_file_create



static void
async_file_open_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    H5VL_async_info_t *info = NULL;
    hid_t under_fapl_id = -1;
    async_task_t *task = (async_task_t*)foo;
    async_file_open_args_t *args = (async_file_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    async_instance_g->start_abt_push = false;

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    /* Get copy of our VOL info from FAPL */
    H5Pget_vol_info(args->fapl_id, (void **)&info);

    /* Copy the FAPL */
    under_fapl_id = H5Pcopy(args->fapl_id);

    /* Set the VOL ID and info for the underlying FAPL */
    if (info) {
        H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);
    }
    else {
        hid_t under_vol_id;
        H5Pget_vol_id(args->fapl_id, &under_vol_id);
        H5Pset_vol(under_fapl_id, under_vol_id, NULL);
    }
    if ((obj = H5VLfile_open(args->name, args->flags, under_fapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif

    // Increase file open ref count
    if (ABT_mutex_lock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_lock\n");
        goto done;
    };
    async_instance_g->nfopen++;
    if (ABT_mutex_unlock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_ulock\n");
        goto done;
    };

done:
    fflush(stdout);
    if(under_fapl_id > 0)    H5Pclose(under_fapl_id);
    if(NULL != info)         H5VL_async_info_free(info);
    free(args->name);
    args->name = NULL;
    if(args->fapl_id > 0)    H5Pclose(args->fapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_open_fn

static H5VL_async_t*
async_file_open(int is_blocking, async_instance_t* aid, const char *name, unsigned flags, hid_t fapl_id, hid_t dxpl_id, void **req)
{
    hid_t under_vol_id;
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_open_args_t *args = NULL;
    int lock_self;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);

    H5Pget_vol_id(fapl_id, &under_vol_id);

    if ((args = (async_file_open_args_t*)calloc(1, sizeof(async_file_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_async_obj      = async_obj;
    if (ABT_mutex_create(&(async_obj->file_task_list_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->name             = strdup(name);
    args->flags            = flags;
    if(fapl_id > 0)
        args->fapl_id = H5Pcopy(fapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = under_vol_id;
    async_task->async_obj  = async_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    /* Lock async_obj */
    while (1) {
        if (async_obj->obj_mutex && ABT_mutex_trylock(async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else 
            fprintf(stderr,"  [ASYNC VOL DBG] %s error with try_lock\n", __func__);
        usleep(1000);
    }
    lock_self = 1;

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(async_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    async_obj->task_cnt++;
    async_obj->pool_ptr = &aid->pool;
    H5Pget_coll_metadata_write(fapl_id, &async_obj->is_col_meta);
    add_task_to_queue(&aid->qhead, async_task, REGULAR);
    if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_self = 0;

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (get_n_running_task_in_queue(async_task) == 0)
        push_task_to_abt_pool(&aid->qhead, aid->pool);
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_self == 1) {
        if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL DBG] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_file_open



static void
async_file_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_file_get_args_t *args = (async_file_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->file) {
        if (NULL != task->parent_obj->under_object) {
            args->file = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLfile_get(args->file, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_get_fn

static herr_t
async_file_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_file_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_file_get_args_t*)calloc(1, sizeof(async_file_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->file             = parent_obj->under_object;
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_file_get



static void
async_file_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_file_specific_args_t *args = (async_file_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->file) {
        if (NULL != task->parent_obj->under_object) {
            args->file = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLfile_specific(args->file, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_specific_fn

static herr_t
async_file_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_file_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_file_specific_args_t*)calloc(1, sizeof(async_file_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->file             = parent_obj->under_object;
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_file_specific



static void
async_file_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_file_optional_args_t *args = (async_file_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->file) {
        if (NULL != task->parent_obj->under_object) {
            args->file = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLfile_optional(args->file, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_optional_fn

static herr_t
async_file_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_file_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_file_optional_args_t*)calloc(1, sizeof(async_file_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->file             = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (get_n_running_task_in_queue(async_task) == 0)
        push_task_to_abt_pool(&aid->qhead, aid->pool);
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_file_optional



static void
async_file_close_fn(void *foo)
{
    herr_t ret_value;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_file_close_args_t *args = (async_file_close_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->file) {
        if (NULL != task->parent_obj->under_object) {
            args->file = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( (ret_value = H5VLfile_close(args->file, task->under_vol_id, args->dxpl_id, args->req)) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfile_close failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif

    // Decrease file open ref count
    if (ABT_mutex_lock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_lock\n");
        goto done;
    };
    if (async_instance_g->nfopen > 0)  async_instance_g->nfopen--;
    if (ABT_mutex_unlock(async_instance_mutex_g) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC ABT ERROR] with ABT_mutex_ulock\n");
        goto done;
    };

done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_file_close_fn

static herr_t
async_file_close(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_file_close_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_file_close_args_t*)calloc(1, sizeof(async_file_close_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->file             = parent_obj->under_object;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_file_close_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    else {
            if (get_n_running_task_in_queue(async_task) == 0)
                push_task_to_abt_pool(&aid->qhead, aid->pool);

    }

    aid->start_abt_push = true;
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_file_close



static void
async_group_create_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_create_args_t *args = (async_group_create_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLgroup_create(args->obj, args->loc_params, task->under_vol_id, args->name, args->lcpl_id, args->gcpl_id, args->gapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_create failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->gcpl_id > 0)    H5Pclose(args->gcpl_id);
    if(args->gapl_id > 0)    H5Pclose(args->gapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_create_fn

static H5VL_async_t*
async_group_create(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t lcpl_id, hid_t gcpl_id, hid_t gapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_create_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_create_args_t*)calloc(1, sizeof(async_group_create_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(gcpl_id > 0)
        args->gcpl_id = H5Pcopy(gcpl_id);
    if(gapl_id > 0)
        args->gapl_id = H5Pcopy(gapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_create_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_group_create



static void
async_group_open_fn(void *foo)
{
    void *obj;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_open_args_t *args = (async_group_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ((obj = H5VLgroup_open(args->obj, args->loc_params, task->under_vol_id, args->name, args->gapl_id, args->dxpl_id, args->req)) == NULL ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif

    task->async_obj->under_object = obj;
    task->async_obj->is_obj_valid = 1;
    task->async_obj->create_task = NULL;




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    free(args->name);
    args->name = NULL;
    if(args->gapl_id > 0)    H5Pclose(args->gapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_open_fn

static H5VL_async_t*
async_group_open(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, const char *name, hid_t gapl_id, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_open_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_open_args_t*)calloc(1, sizeof(async_group_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->name             = strdup(name);
    if(gapl_id > 0)
        args->gapl_id = H5Pcopy(gapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = async_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_group_open



static void
async_group_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_get_args_t *args = (async_group_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLgroup_get(args->obj, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_get_fn

static herr_t
async_group_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_group_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_get_args_t*)calloc(1, sizeof(async_group_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_group_get



static void
async_group_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_specific_args_t *args = (async_group_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLgroup_specific(args->obj, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_specific_fn

static herr_t
async_group_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_group_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_specific_args_t*)calloc(1, sizeof(async_group_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_group_specific



static void
async_group_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_optional_args_t *args = (async_group_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLgroup_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_optional_fn

static herr_t
async_group_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_group_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_optional_args_t*)calloc(1, sizeof(async_group_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_group_optional



static void
async_group_close_fn(void *foo)
{
    herr_t ret_value;
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_group_close_args_t *args = (async_group_close_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->grp) {
        if (NULL != task->parent_obj->under_object) {
            args->grp = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( (ret_value = H5VLgroup_close(args->grp, task->under_vol_id, args->dxpl_id, args->req)) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLgroup_close failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_group_close_fn

static herr_t
async_group_close(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_group_close_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_group_close_args_t*)calloc(1, sizeof(async_group_close_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->grp              = parent_obj->under_object;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_group_close_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    else {
        if (aid->ex_gclose) {
            if (get_n_running_task_in_queue(async_task) == 0)
                push_task_to_abt_pool(&aid->qhead, aid->pool);

        }
    }

    aid->start_abt_push = true;
    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_group_close



static void
async_link_create_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_create_args_t *args = (async_link_create_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_create(args->create_type, args->obj, args->loc_params, task->under_vol_id, args->lcpl_id, args->lapl_id, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_create failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->lapl_id > 0)    H5Pclose(args->lapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_create_fn

static H5VL_async_t*
async_link_create(int is_blocking, async_instance_t* aid, H5VL_link_create_type_t create_type, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req, va_list arguments)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_create_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_create_args_t*)calloc(1, sizeof(async_link_create_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->create_type      = create_type;
    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(lapl_id > 0)
        args->lapl_id = H5Pcopy(lapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_create_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_link_create



static void
async_link_copy_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_copy_args_t *args = (async_link_copy_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->src_obj) {
        if (NULL != task->parent_obj->under_object) {
            args->src_obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_copy(args->src_obj, args->loc_params1, args->dst_obj, args->loc_params2, task->under_vol_id, args->lcpl_id, args->lapl_id, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_copy failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params1);
    free_loc_param((H5VL_loc_params_t*)args->loc_params2);
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->lapl_id > 0)    H5Pclose(args->lapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_copy_fn

static herr_t
async_link_copy(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params1, H5VL_async_t *parent_obj2, const H5VL_loc_params_t *loc_params2, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_copy_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_copy_args_t*)calloc(1, sizeof(async_link_copy_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->src_obj          = parent_obj->under_object;
    args->loc_params1 = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params1));
    dup_loc_param(args->loc_params1, loc_params1);
    args->dst_obj          = parent_obj2->under_object;
    args->loc_params2 = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params2));
    dup_loc_param(args->loc_params2, loc_params2);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(lapl_id > 0)
        args->lapl_id = H5Pcopy(lapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_copy_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_link_copy



static void
async_link_move_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_move_args_t *args = (async_link_move_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->src_obj) {
        if (NULL != task->parent_obj->under_object) {
            args->src_obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_move(args->src_obj, args->loc_params1, args->dst_obj, args->loc_params2, task->under_vol_id, args->lcpl_id, args->lapl_id, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_move failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params1);
    free_loc_param((H5VL_loc_params_t*)args->loc_params2);
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->lapl_id > 0)    H5Pclose(args->lapl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_move_fn

static herr_t
async_link_move(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params1, H5VL_async_t *parent_obj2, const H5VL_loc_params_t *loc_params2, hid_t lcpl_id, hid_t lapl_id, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_move_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_move_args_t*)calloc(1, sizeof(async_link_move_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->src_obj          = parent_obj->under_object;
    args->loc_params1 = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params1));
    dup_loc_param(args->loc_params1, loc_params1);
    args->dst_obj          = parent_obj2->under_object;
    args->loc_params2 = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params2));
    dup_loc_param(args->loc_params2, loc_params2);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(lapl_id > 0)
        args->lapl_id = H5Pcopy(lapl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_move_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_link_move



static void
async_link_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_get_args_t *args = (async_link_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_get(args->obj, args->loc_params, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_get_fn

static herr_t
async_link_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5VL_link_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_get_args_t*)calloc(1, sizeof(async_link_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_link_get



static void
async_link_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_specific_args_t *args = (async_link_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_specific(args->obj, args->loc_params, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_specific_fn

static herr_t
async_link_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5VL_link_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_specific_args_t*)calloc(1, sizeof(async_link_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_link_specific



static void
async_link_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_link_optional_args_t *args = (async_link_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLlink_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLlink_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_link_optional_fn

static herr_t
async_link_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_link_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_link_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_link_optional_args_t*)calloc(1, sizeof(async_link_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_link_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_link_optional



static void
async_object_open_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_object_open_args_t *args = (async_object_open_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    if (1 == task->async_obj->is_obj_valid) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        return;
    }
    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLobject_open(args->obj, args->loc_params, task->under_vol_id, args->opened_type, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLobject_open failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue )
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_object_open_fn

static H5VL_async_t*
async_object_open(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5I_type_t *opened_type, hid_t dxpl_id, void **req)
{
    H5VL_async_t *async_obj = NULL;
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_object_open_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_object_open_args_t*)calloc(1, sizeof(async_object_open_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new async object */ 
    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    async_obj->magic = ASYNC_MAGIC;
    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }
    async_obj->file_task_list_head = parent_obj->file_task_list_head;
    async_obj->file_async_obj      = parent_obj->file_async_obj;
    async_obj->is_col_meta = parent_obj->is_col_meta;
    async_obj->pool_ptr = &aid->pool;
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->opened_type      = opened_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_object_open_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }

    async_obj->create_task = async_task;
    async_obj->under_vol_id = async_task->under_vol_id;

    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return async_obj;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return NULL;
} // End async_object_open



static void
async_object_copy_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_object_copy_args_t *args = (async_object_copy_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->src_obj) {
        if (NULL != task->parent_obj->under_object) {
            args->src_obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLobject_copy(args->src_obj, args->src_loc_params, args->src_name, args->dst_obj, args->dst_loc_params, args->dst_name, args->ocpypl_id, task->under_vol_id, args->lcpl_id, args->dxpl_id, args->req) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLobject_copy failed\n", __func__);
        goto done;
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->src_loc_params);
    free(args->src_name);
    args->src_name = NULL;
    free_loc_param((H5VL_loc_params_t*)args->dst_loc_params);
    free(args->dst_name);
    args->dst_name = NULL;
    if(args->ocpypl_id > 0)    H5Pclose(args->ocpypl_id);
    if(args->lcpl_id > 0)    H5Pclose(args->lcpl_id);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_object_copy_fn

static herr_t
async_object_copy(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *src_loc_params, const char *src_name, H5VL_async_t *parent_obj2, const H5VL_loc_params_t *dst_loc_params, const char *dst_name, hid_t ocpypl_id, hid_t lcpl_id, hid_t dxpl_id, void **req)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_object_copy_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_object_copy_args_t*)calloc(1, sizeof(async_object_copy_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->src_obj          = parent_obj->under_object;
    args->src_loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*src_loc_params));
    dup_loc_param(args->src_loc_params, src_loc_params);
    args->src_name         = strdup(src_name);
    args->dst_obj          = parent_obj2->under_object;
    args->dst_loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*dst_loc_params));
    dup_loc_param(args->dst_loc_params, dst_loc_params);
    args->dst_name         = strdup(dst_name);
    if(ocpypl_id > 0)
        args->ocpypl_id = H5Pcopy(ocpypl_id);
    if(lcpl_id > 0)
        args->lcpl_id = H5Pcopy(lcpl_id);
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_object_copy_fn; 
    async_task->args       = args;
    async_task->op         = WRITE;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_object_copy



static void
async_object_get_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_object_get_args_t *args = (async_object_get_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLobject_get(args->obj, args->loc_params, task->under_vol_id, args->get_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLobject_get failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_object_get_fn

static herr_t
async_object_get(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5VL_object_get_t get_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_object_get_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_object_get_args_t*)calloc(1, sizeof(async_object_get_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->get_type         = get_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_object_get_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_object_get



static void
async_object_specific_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_object_specific_args_t *args = (async_object_specific_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLobject_specific(args->obj, args->loc_params, task->under_vol_id, args->specific_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLobject_specific failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    free_loc_param((H5VL_loc_params_t*)args->loc_params);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_object_specific_fn

static herr_t
async_object_specific(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, const H5VL_loc_params_t *loc_params, H5VL_object_specific_t specific_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_object_specific_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_object_specific_args_t*)calloc(1, sizeof(async_object_specific_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->loc_params = (H5VL_loc_params_t*)calloc(1, sizeof(*loc_params));
    dup_loc_param(args->loc_params, loc_params);
    args->specific_type    = specific_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_object_specific_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_object_specific



static void
async_object_optional_fn(void *foo)
{
    hbool_t acquired = false;
    int is_lock = 0, sleep_time = 500;
    unsigned int attempt_count, new_attempt_count;
    hbool_t is_lib_state_restored = false;
    ABT_pool *pool_ptr;
    async_task_t *task = (async_task_t*)foo;
    async_object_optional_args_t *args = (async_object_optional_args_t*)(task->args);

    #ifdef ENABLE_TIMING
    struct timeval now_time;
    struct timeval timer1;
    struct timeval timer2;
    struct timeval timer3;
    struct timeval timer4;
    struct timeval timer5;
    struct timeval timer6;
    struct timeval timer7;
    struct timeval timer8;
    struct timeval timer9;
    gettimeofday(&args->start_time, NULL);
    #endif

    #ifdef ENABLE_TIMING
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s start, time=%ld.%06ld\n", __func__, args->start_time.tv_sec, args->start_time.tv_usec);
    #endif
    #ifdef ENABLE_LOG
    fprintf(stdout,"  [ASYNC ABT LOG] entering %s\n", __func__);
    fflush(stdout);
    #endif
    assert(args);
    assert(task);
    assert(task->async_obj);
    assert(task->async_obj->magic == ASYNC_MAGIC);

    pool_ptr = task->async_obj->pool_ptr;

    /* Update the dependent parent object if it is NULL */
    if (NULL == args->obj) {
        if (NULL != task->parent_obj->under_object) {
            args->obj = task->parent_obj->under_object;
        }
        else {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT ERROR] %s parent object is NULL, re-insert to pool\n", __func__);
            #endif
            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_thread_create failed for %p\n", __func__, task->func);
            }

            return;
        }
    }

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: trying to aquire global lock\n", __func__);
    fflush(stderr);
    #endif
    #ifdef ENABLE_TIMING
    gettimeofday(&timer1, NULL);
    double time1 = get_elapsed_time(&args->start_time, &timer1);
    #endif

    while (acquired == false) {
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        #endif
        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
            goto done;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock count = %d, time=%ld.%06ld\n", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);
        #endif
        if (H5TSmutex_acquire(&acquired) < 0) {
            fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_acquire failed\n", __func__);
            goto done;
        }
        if (false == acquired) {
            #ifdef ENABLE_DBG_MSG
            fprintf(stderr,"  [ASYNC ABT DBG] %s lock NOT acquired, wait\n", __func__);
            #endif
            if(sleep_time > 0) usleep(sleep_time);
            continue;
        }
        #ifdef ENABLE_TIMING
        gettimeofday(&now_time, NULL);
        fprintf(stderr,"  [ASYNC ABT DBG] %s lock SUCCESSFULLY acquired, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
        #endif
        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt % ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {
            if(sleep_time > 0) usleep(sleep_time);
            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {
                fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_get_attempt_count failed\n", __func__);
                goto done;
            }
            #ifdef ENABLE_DBG_MSG
            #ifdef ENABLE_TIMING
            gettimeofday(&now_time, NULL);
            fprintf(stderr,"  [ASYNC ABT DBG] %s after wait lock count = %d, time=%ld.%06ld\n", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);
            #endif
            #endif
            if (new_attempt_count > attempt_count) {
                if (H5TSmutex_release() < 0) {
                    fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
                }
                #ifdef ENABLE_TIMING
                gettimeofday(&now_time, NULL);
                fprintf(stderr,"  [ASYNC ABT DBG] %s lock YIELD to main thread, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
                #endif
                acquired = false;
            }
            else {
                break;
            }
            attempt_count = new_attempt_count;
            task->async_obj->file_async_obj->attempt_check_cnt++;
            task->async_obj->file_async_obj->attempt_check_cnt %= ASYNC_ATTEMPT_CHECK_INTERVAL;
        }
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&timer2, NULL);
    double time2 = get_elapsed_time(&timer1, &timer2);
    #endif

    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s: global lock acquired\n", __func__);
    fflush(stderr);
    #endif

    /* Aquire async obj mutex and set the obj */
    assert(task->async_obj->obj_mutex);
    assert(task->async_obj->magic == ASYNC_MAGIC);
    while (1) {
        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        else {
            fprintf(stderr,"  [ASYNC ABT DBG] %s error with try_lock\n", __func__);
            break;
        }
        usleep(1000);
    }
    is_lock = 1;


    // Restore previous library state
    assert(task->h5_state);
    if (H5VLrestore_lib_state(task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLrestore_lib_state failed\n", __func__);
        goto done;
    }
    is_lib_state_restored = true;

    #ifdef ENABLE_TIMING
    gettimeofday(&timer3, NULL);
    double time3 = get_elapsed_time(&timer2, &timer3);
    #endif

    if ( H5VLobject_optional(args->obj, task->under_vol_id, args->opt_type, args->dxpl_id, args->req, args->arguments) < 0 ) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLobject_optional failed\n", __func__);
        goto done;
    }

    /* va_end is needed as arguments is copied previously */
    va_end(args->arguments);

    #ifdef ENABLE_TIMING
    gettimeofday(&timer4, NULL);
    double time4 = get_elapsed_time(&timer3, &timer4);
    #endif




    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC ABT LOG] Argobots execute %s success\n", __func__);
    #endif


done:
    fflush(stdout);
    if(args->dxpl_id > 0)    H5Pclose(args->dxpl_id);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer5, NULL);
    double time5 = get_elapsed_time(&timer4, &timer5);
    #endif

    if (is_lock == 1) {
        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr,"  [ASYNC ABT ERROR] %s ABT_mutex_unlock failed\n", __func__);
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&timer6, NULL);
    double time6 = get_elapsed_time(&timer5, &timer6);
    #endif

    ABT_eventual_set(task->eventual, NULL, 0);
    task->in_abt_pool = 0;
    task->is_done = 1;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer7, NULL);
    double time7 = get_elapsed_time(&timer6, &timer7);
    #endif

    if(is_lib_state_restored && H5VLreset_lib_state() < 0)
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLreset_lib_state failed\n", __func__);
    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) 
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5VLfree_lib_state failed\n", __func__);
    task->h5_state = NULL;
    #ifdef ENABLE_TIMING
    gettimeofday(&timer8, NULL);
    double time8 = get_elapsed_time(&timer7, &timer8);
    #endif

#ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC ABT DBG] %s releasing global lock\n", __func__);
#endif
    if (acquired == true && H5TSmutex_release() < 0) {
        fprintf(stderr,"  [ASYNC ABT ERROR] %s H5TSmutex_release failed\n", __func__);
    }
    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)
       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);
    #ifdef ENABLE_TIMING
    gettimeofday(&timer9, NULL);
    double exec_time   = get_elapsed_time(&args->start_time, &timer9);
    double total_time  = get_elapsed_time(&args->create_time, &timer9);
    double wait_time   = total_time - exec_time;
    printf("  [ASYNC ABT TIMING] %-24s \ttotal time      : %f\n", __func__, total_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  wait time     : %f\n", __func__, wait_time);
    printf("  [ASYNC ABT TIMING] %-24s \t  execute time  : %f\n", __func__, exec_time);
    printf("  [ASYNC ABT TIMING] %-24s \t    time2       : %f\n", __func__, time2);
    printf("  [ASYNC ABT TIMING] %-24s \t    time3       : %f\n", __func__, time3);
    printf("  [ASYNC ABT TIMING] %-24s \t    time4(n.vol): %f\n", __func__, time4);
    fflush(stdout);
    #endif
    return;
} // End async_object_optional_fn

static herr_t
async_object_optional(int is_blocking, async_instance_t* aid, H5VL_async_t *parent_obj, H5VL_object_optional_t opt_type, hid_t dxpl_id, void **req, va_list arguments)
{
    async_task_t *async_task = NULL;
    H5RQ_token_int_t *token = NULL;
    async_object_optional_args_t *args = NULL;
    int lock_parent;
    hbool_t acquired = false;

    #ifdef ENABLE_LOG
    fprintf(stderr,"  [ASYNC VOL LOG] entering %s\n", __func__);
    fflush(stderr);
    #endif

    assert(aid);
    assert(parent_obj);
    assert(parent_obj->magic == ASYNC_MAGIC);

    if ((args = (async_object_optional_args_t*)calloc(1, sizeof(async_object_optional_args_t))) == NULL) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    #ifdef ENABLE_TIMING
    gettimeofday(&args->create_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] entering %s, time=%ld.%06ld\n", __func__, args->create_time.tv_sec, args->create_time.tv_usec);
    fflush(stderr);
    #endif
    /* create a new task and insert into its file task list */ 
    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { 
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with calloc\n", __func__);
        goto error;
    }
    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_create\n", __func__);
        goto error;
    }

    args->obj              = parent_obj->under_object;
    args->opt_type         = opt_type;
    if(dxpl_id > 0)
        args->dxpl_id = H5Pcopy(dxpl_id);
    args->req              = req;
    va_copy(args->arguments, arguments);

    if (req) {
        token = H5RQ__new_token();
        if (token == NULL) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s token is NULL!\n", __func__);
        }
        else {
            token->task = async_task;
            async_task->token = token;
            *req = (void*)token;
        }
    }


    // Retrieve current library state
    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s H5VLretrieve_lib_state failed\n", __func__);
        goto done;
    }

    async_task->func       = async_object_optional_fn; 
    async_task->args       = args;
    async_task->op         = READ;
    async_task->under_vol_id  = parent_obj->under_vol_id;
    async_task->async_obj  = parent_obj;
    async_task->parent_obj = parent_obj;
    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s ABT_eventual_create failed\n", __func__);
        goto error;
    }


    /* Lock parent_obj */
    while (1) {
        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {
            break;
        }
        usleep(1000);
    }
    lock_parent = 1;

    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_lock\n", __func__);
        goto done;
    }
    /* Insert it into the file task list */
    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);
    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {
        fprintf(stderr,"  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto done;
    }
    parent_obj->task_cnt++;
    parent_obj->pool_ptr = &aid->pool;
    /* Check if its parent has valid object */
    if (parent_obj->is_obj_valid != 1) {
        if (NULL != parent_obj->create_task) {
            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);
        }
        else {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s parent task not created\n", __func__);
            goto error; 
        }
    }
    else {
        if (async_task->async_obj->is_col_meta == true) 
            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);
        else
            add_task_to_queue(&aid->qhead, async_task, REGULAR);
    }

    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {
        fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
        goto error;
    }
    lock_parent = 0;
    #ifdef ENABLE_TIMING
    struct timeval now_time;
    gettimeofday(&now_time, NULL);
    printf("  [ASYNC VOL TIMING] %-24s \t  create time   : %f\n",
		 __func__, get_elapsed_time(&args->create_time, &now_time));
    #endif
    if (aid->ex_delay == false) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);
    }

    /* Wait if blocking is needed */
    if (is_blocking == 1) {
        if (get_n_running_task_in_queue(async_task) == 0)
            push_task_to_abt_pool(&aid->qhead, aid->pool);

        if (H5TSmutex_release() < 0) {
            fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_release failed\n", __func__);
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s waiting to finish all previous tasks\n", __func__);
        fflush(stderr);
        #endif
        if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_eventual_wait\n", __func__);
            goto error; 
        }
        #ifdef ENABLE_DBG_MSG
        fprintf(stderr,"  [ASYNC VOL DBG] %s finished all previous tasks, proceed\n", __func__);
        fflush(stderr);
        #endif
        while (acquired == false) {
            if (H5TSmutex_acquire(&acquired) < 0) {
                fprintf(stderr,"  [ASYNC VOL ERROR] %s H5TSmutex_acquire failed\n", __func__);
                goto done;
            }
        }
    }

    #ifdef ENABLE_TIMING
    gettimeofday(&now_time, NULL);
    fprintf(stderr,"  [ASYNC VOL TIMING] leaving %s, time=%ld.%06ld\n", __func__, now_time.tv_sec, now_time.tv_usec);
    #endif
    #ifdef ENABLE_DBG_MSG
    fprintf(stderr,"  [ASYNC VOL DBG] leaving %s \n", __func__);
    #endif

done:
    fflush(stdout);
    return 1;
error:
    if (lock_parent == 1) {
        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) 
            fprintf(stderr, "  [ASYNC VOL ERROR] %s with ABT_mutex_unlock\n", __func__);
    }
    if (NULL != args) free(args);
    return -1;
} // End async_object_optional



