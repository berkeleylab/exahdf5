#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *fout_h, *fout_c, *fout_d;
void proc_func(char *in, int len);

int main(int argc, const char *argv[])
{
    FILE *stream;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int i;

    const char *in_file = argv[1];
    if (in_file == NULL) {
        printf("No input file! Exiting...\n");
        goto done;
    }
 
    stream = fopen(in_file, "r");
    if (stream == NULL) {
        printf("No input file! Exiting...\n");
        goto done;
    }

    fout_h = fopen("out_abt_gen.h", "w");
    if (fout_h == NULL) {
        printf("Output file failed! Exiting...\n");
        goto done;
    }
    fout_c = fopen("out_abt_gen.c", "w");
    if (fout_c == NULL) {
        printf("Output file failed! Exiting...\n");
        goto done;
    }
    fout_d = fopen("out_abt_gen_def.h", "w");
    if (fout_d == NULL) {
        printf("Output file failed! Exiting...\n");
        goto done;
    }
 
 
    while ((read = getline(&line, &len, stream)) != -1) {
        /* printf("Retrieved line of length %u :\n", read); */
        /* printf("%s", line); */
        if (strstr(line, "request") != NULL) {
            continue;
        }

        proc_func(line, read);
        /* printf("\n\n"); */
    }
 
    free(line);
    fclose(stream);
    fclose(fout_c);
    fclose(fout_h);
    fclose(fout_d);


done:
    return 0;
}

#define MAX_ARG       64
#define MAX_NAME_LEN  64

void print_args_list(FILE *fout, int nargs, char arg_types[MAX_ARG][MAX_NAME_LEN], 
                     char arg_names[MAX_ARG][MAX_NAME_LEN], int *arg_is_ptr)
{
    int i, j;
    for (i = 0; i < nargs; i++) {
        fprintf(fout, "%s", arg_types[i]);
        fprintf(fout, " ");
        for (j = 0; j < arg_is_ptr[i]; j++) 
            fprintf(fout, "*");
        fprintf(fout, "%s", arg_names[i]);
        if (i != nargs - 1) 
            fprintf(fout, ", ");
    }
    return;
}


void proc_func(char *in, int len)
{
    char funcname[MAX_NAME_LEN] = {0};
    char arg_types[MAX_ARG][MAX_NAME_LEN] = {0};
    char arg_names[MAX_ARG][MAX_NAME_LEN] = {0};
    int  arg_is_ptr[MAX_ARG] = {0};
    int  nargs = 0, start = 0, ret_type = 0;

    // Skip "static "
    int i = 7, j = 0;
    if (in[i] == 'v') {
        // void*
        i += 6;
        ret_type = 1;
    }
    else if (in[i] == 'h') {
        // herr_t
        // get func name
        i += 7;
        ret_type = 2;
    }
    else {
        printf("unexpected char %c for func return type\n", in[i]);
        return;
    }
    if (in[i] != 'H' || in[i+1] != '5') {
        printf("Error with parsing function name, shold start with H5\n");
        return;
    }

    for (j = 0; j+i < len; j++) {
        if (in[i+j] == '(') {
            strncpy(funcname, &in[i], j);
            funcname[j] = 0;
            j++;
            i += j;
            break;
        }
    }
    /* printf("function name: [%s]\n", funcname); */

    // get argument list
    for (j = 0; i + j < len; j++) {
        if (in[i+j] == ' ') {
            if (strcmp(arg_types[nargs], "const")== 0) {
                strcat(arg_types[nargs], " ");
                strncat(arg_types[nargs], &in[i], j);
                arg_types[nargs][j+7] = 0;
            }
            else {
                strncpy(arg_types[nargs], &in[i], j);
                arg_types[nargs][j] = 0;
            }
            while (i+j < len && in[i+j] == ' ') j++;
            i += j;
            j = 0;
        }
        if (in[i+j] == ',' || in[i+j] == ')') {
            while(in[i] == '*') {arg_is_ptr[nargs]++;i++;j--;};
            strncpy(arg_names[nargs], &in[i], j);
            arg_names[nargs][j] = 0;
            j++;
            while (i+j < len && in[i+j] == ' ') j++;
            i += j;
            j = 0;
            /* printf("[%s] - %d[%s]\n", arg_types[nargs], arg_is_ptr[nargs], arg_names[nargs]); */
            nargs++;
        }
    }


    int skip_h5_prefix = 11; // skip "H5VL_async_"

    /* ====== Define structure ====== */
    int is_dwrite = 0, is_attr_create = 0;
    char sname[MAX_NAME_LEN];
    sprintf(sname, "async_%s_args_t", &funcname[skip_h5_prefix]);
    
    if (strstr(sname, "dataset_write") != NULL || strstr(sname, "attr_write") != NULL) 
        is_dwrite = 1;

    if (strstr(sname, "attr_create") != NULL || strstr(sname, "attr_open") != NULL) 
        is_attr_create = 1;

    fprintf(fout_d, "typedef struct %s {\n", sname);

    for (i = 0; i < nargs; i++) {
        if (strstr(arg_types[i], "const char") != NULL) 
            fprintf(fout_d, "    %-25s", "char");
        else if (strstr(arg_types[i], "const H5VL_loc_params_t") != NULL) 
            fprintf(fout_d, "    %-25s", "H5VL_loc_params_t");
        else if (is_dwrite == 1 && strstr(arg_types[i], "const void") != NULL) 
            fprintf(fout_d, "    %-25s", "void");
        else
            fprintf(fout_d, "    %-25s", arg_types[i]);
        for (j = 0; j < arg_is_ptr[i]; j++) 
            fprintf(fout_d, "*");

        /* if (strcmp(arg_types[i], "va_list") == 0) */ 
        /*     fprintf(fout_d, "*%s;\n", arg_names[i]); */
        /* else */
            fprintf(fout_d, "%s;\n", arg_names[i]);
    }

    if (is_dwrite == 1) 
        fprintf(fout_d, "    hbool_t                   buf_free;\n");

    /* if (is_attr_create == 1) */ 
    /*     fprintf(fout_d, "    hsize_t                   attr_size;\n"); */

    fprintf(fout_d, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_d, "    struct timeval create_time;\n");
    fprintf(fout_d, "    struct timeval start_time;\n");
    fprintf(fout_d, "    #endif\n");


    fprintf(fout_d, "} %s;\n\n", sname);

    /* ====== async_dataset_write_fn ====== */
    int is_create = 0, is_close = 0, is_commit = 0, is_attr_close = 0;
    int is_file_create = 0, is_file_close = 0, is_group_close = 0, is_dset_close = 0, is_raw_data = 0;
    char fnname[MAX_NAME_LEN];
    char tmpfuncname[MAX_NAME_LEN];
    char H5VLname[MAX_NAME_LEN];
    sprintf(fnname, "async_%s_fn", &funcname[skip_h5_prefix]);
    sprintf(tmpfuncname, "%s", &funcname[skip_h5_prefix]);
    sprintf(H5VLname, "H5VL%s", &funcname[skip_h5_prefix]);

    if (strstr(funcname, "close") != NULL ) {
        is_close = 1;
        if (strstr(funcname, "file") != NULL ) 
            is_file_close = 1;
        if (strstr(funcname, "group") != NULL ) 
            is_group_close = 1;
        if (strstr(funcname, "dataset") != NULL ) 
            is_dset_close = 1;
        if (strstr(funcname, "attr") != NULL ) 
            is_attr_close = 1;
    }

    if (strstr(funcname, "create") != NULL || strstr(funcname, "open") != NULL) {
        is_create = 1;
        if (strstr(funcname, "file") != NULL ) 
            is_file_create = 1;
    }

    if (strstr(funcname, "commit") != NULL ) 
        is_commit = 1;
    /* fprintf(fout_h, "static void %s(void *foo);\n", fnname); */

    fprintf(fout_c, "static void\n"); 
    fprintf(fout_c, "%s(void *foo)\n", fnname);
    fprintf(fout_c, "{\n");

    int use_obj = 0;
    if (is_create == 1 && strstr(funcname, "link_create") == NULL && strstr(funcname, "object_open") == NULL) 
        use_obj = 1;
    if (use_obj == 1) 
        fprintf(fout_c, "    void *obj;\n");
    else if (is_close == 1)
        fprintf(fout_c, "    herr_t ret_value;\n");
    fprintf(fout_c, "    hbool_t acquired = false;\n");
    fprintf(fout_c, "    int is_lock = 0, sleep_time = 500;\n    unsigned int attempt_count, new_attempt_count;\n");
    /* fprintf(fout_c, "    pthread_t attempt_thread;\n"); */
    /* fprintf(fout_c, "    int i;\n"); */
    fprintf(fout_c, "    hbool_t is_lib_state_restored = false;\n");
    fprintf(fout_c, "    ABT_pool *pool_ptr;\n");
    /* fprintf(fout_c, "    cpu_set_t cpuset;\n"); */
    /* fprintf(fout_c, "    pthread_t thread = pthread_self();\n"); */
    if (is_file_create == 1) {
        fprintf(fout_c, "    H5VL_async_info_t *info = NULL;\n");
        fprintf(fout_c, "    hid_t under_fapl_id = -1;\n");
    }
    fprintf(fout_c, "    async_task_t *task = (async_task_t*)foo;\n");
    /* fprintf(fout_c, "    // TODO: vary\n"); */
    fprintf(fout_c, "    %s *args = (%s*)(task->args);\n", sname, sname);
    fprintf(fout_c, "\n");


    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    struct timeval now_time;\n");
    fprintf(fout_c, "    struct timeval timer1;\n");
    fprintf(fout_c, "    struct timeval timer2;\n");
    fprintf(fout_c, "    struct timeval timer3;\n");
    fprintf(fout_c, "    struct timeval timer4;\n");
    fprintf(fout_c, "    struct timeval timer5;\n");
    fprintf(fout_c, "    struct timeval timer6;\n");
    fprintf(fout_c, "    struct timeval timer7;\n");
    fprintf(fout_c, "    struct timeval timer8;\n");
    fprintf(fout_c, "    struct timeval timer9;\n");
    fprintf(fout_c, "    gettimeofday(&args->start_time, NULL);\n");
    fprintf(fout_c, "    #endif\n\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT LOG] Argobots execute %%s start, time=%%ld.%%06ld\\n\", __func__, args->start_time.tv_sec, args->start_time.tv_usec);\n");
    fprintf(fout_c, "    #endif\n");

    fprintf(fout_c, "    #ifdef ENABLE_LOG\n");
    fprintf(fout_c, "    fprintf(stdout,\"  [ASYNC ABT LOG] entering %%s\\n\", __func__);\n");
    fprintf(fout_c, "    fflush(stdout);\n");
    fprintf(fout_c, "    #endif\n");


    fprintf(fout_c, "    assert(args);\n");
    fprintf(fout_c, "    assert(task);\n");
    fprintf(fout_c, "    assert(task->async_obj);\n");
    fprintf(fout_c, "    assert(task->async_obj->magic == ASYNC_MAGIC);\n");
    fprintf(fout_c, "\n");


    /* fprintf(fout_c, "    test_affinity();\n"); */
    /* fprintf(fout_c, "    async_set_affinity();\n"); */
    /* if (is_file_create) */ 
    /*     fprintf(fout_c, "    async_get_affinity();\n"); */

    /* fprintf(fout_c, "    CPU_ZERO(&cpuset);\n"); */
    /* fprintf(fout_c, "    for (i = 0; i < 10; i++)\n"); */
    /* fprintf(fout_c, "        CPU_SET(i, &cpuset);\n"); */
    /* fprintf(fout_c, "    if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset) != 0)\n"); */
    /* fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT ERROR] %%s cannot set affinity\\n\", __func__);\n"); */

    /* fprintf(fout_c, "    if (pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset) != 0)\n"); */
    /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s cannot get affinity\\n\", __func__);\n"); */
    /* fprintf(fout_c, "    for (i = 0; i < CPU_SETSIZE; i++)\n"); */
    /* fprintf(fout_c, "        if (CPU_ISSET(i, &cpuset))\n"); */
    /* fprintf(fout_c, "            fprintf(stderr, \"    CPU %%d\\n\", i);\n"); */
    /* fprintf(fout_c, "    fflush(stderr);\n"); */

    fprintf(fout_c, "    pool_ptr = task->async_obj->pool_ptr;\n\n");


    int idx = 0, is_link_create, is_obj_open;
    if (strstr(funcname, "link_create") != NULL) {
        idx = 1;
        is_link_create = 1;
    }

    if (strstr(funcname, "object_open") != NULL) {
        is_obj_open = 1;
    }

    if (strstr(funcname, "dataset_read") != NULL || strstr(funcname, "dataset_write") != NULL) 
        is_raw_data = 1;
    else
        is_raw_data = 0;

    if (is_file_create != 1) {
        fprintf(fout_c, "    /* Update the dependent parent object if it is NULL */\n");
        fprintf(fout_c, "    if (NULL == args->%s) {\n", arg_names[idx]);
        /* fprintf(fout_c, "        assert(task->parent_obj);\n"); */
        fprintf(fout_c, "        if (NULL != task->parent_obj->under_object) {\n");
        fprintf(fout_c, "            args->%s = task->parent_obj->under_object;\n", arg_names[idx]);
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        else {\n");

        fprintf(fout_c, "            #ifdef ENABLE_DBG_MSG\n");
        fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s parent object is NULL, re-insert to pool\\n\", __func__);\n");
        fprintf(fout_c, "            #endif\n");
        fprintf(fout_c, "            if (ABT_thread_create(*task->async_obj->pool_ptr, task->func, task, ABT_THREAD_ATTR_NULL, &task->abt_thread) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC ABT ERROR] %%s ABT_thread_create failed for %%p\\n\", __func__, task->func);\n");
        fprintf(fout_c, "            }\n\n");
        fprintf(fout_c, "            return;\n");

        /* fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s parent object is NULL\\n\", __func__);\n"); */
        /* fprintf(fout_c, "            goto done;\n"); */
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "\n");
    }


    int connector_loc = 0;
    int connector_loc2 = -1;
    if (strcmp(arg_names[1], "loc_params") == 0) {
        connector_loc = 1;
    }
    if (strcmp(tmpfuncname, "link_create") == 0 ) {
        connector_loc = 2;
    }
    else if (strcmp(tmpfuncname, "link_copy") == 0 || strcmp(tmpfuncname, "link_move") == 0 ) {
        connector_loc = 3;
    }
    else if (strcmp(tmpfuncname, "file_create") == 0 || strcmp(tmpfuncname, "file_open") == 0) {
        // H5VL_pass_through_file_create   H5VL_pass_through_file_open   don't need native_under_vol_id_g
        connector_loc = -1;
    }
    else if (strcmp(tmpfuncname, "object_copy") == 0 ) {
        connector_loc = 6;
    }

    
    /* if (is_raw_data) { */
        /* fprintf(fout_c, "    /1* Delay first read or write operation of a group, so that a series of *1/\n"); */
        /* fprintf(fout_c, "    /1* I/O operations on the main thread are not blocked by background I/O thread *1/\n"); */
        /* fprintf(fout_c, "    if (task->parent_obj->is_raw_io_delay == false) {\n"); */
        /* fprintf(fout_c, "     usleep(sleep_time*10);\n"); */
        /* fprintf(fout_c, "         task->parent_obj->is_raw_io_delay = true;\n"); */
        /* fprintf(fout_c, "    }\n"); */
    /* } */

    // aquire HDF5 library mutex
    fprintf(fout_c, "    #ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT DBG] %%s: trying to aquire global lock\\n\", __func__);\n");
    fprintf(fout_c, "    fflush(stderr);\n");
    fprintf(fout_c, "    #endif\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer1, NULL);\n");
    fprintf(fout_c, "    double time1 = get_elapsed_time(&args->start_time, &timer1);\n");
    fprintf(fout_c, "    #endif\n\n");


    /* fprintf(fout_c, "    sleep_time = 1000\n\n"); */
    fprintf(fout_c, "    while (acquired == false) {\n");

    fprintf(fout_c, "        #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "        gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "        #endif\n");
    /* fprintf(fout_c, "        if (H5TSmutex_get_attempt_thread(&attempt_thread) < 0) {\n"); */
    /* fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_get_attempt_count failed\\n\", __func__);\n"); */
    /* fprintf(fout_c, "            goto done;\n"); */
    /* fprintf(fout_c, "        }\n"); */

    /* fprintf(fout_c, "        #ifdef ENABLE_TIMING\n"); */
    /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT DBG] %%s attempt thread is %%u, my thread is %%u, time=%%ld.%%06ld\\n\", __func__, attempt_thread, pthread_self(), now_time.tv_sec, now_time.tv_usec);\n"); */
    /* fprintf(fout_c, "        #endif\n"); */

    fprintf(fout_c, "        if (async_instance_g->ex_delay == false && H5TSmutex_get_attempt_count(&attempt_count) < 0) {\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_get_attempt_count failed\\n\", __func__);\n");
    fprintf(fout_c, "            goto done;\n");
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "        #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "        gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT DBG] %%s lock count = %%d, time=%%ld.%%06ld\\n\", __func__, attempt_count, now_time.tv_sec, now_time.tv_usec);\n");
    fprintf(fout_c, "        #endif\n");
    fprintf(fout_c, "        if (H5TSmutex_acquire(&acquired) < 0) {\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_acquire failed\\n\", __func__);\n");
    fprintf(fout_c, "            goto done;\n");
    fprintf(fout_c, "        }\n");

//3/30
    fprintf(fout_c, "        if (false == acquired) {\n");
    fprintf(fout_c, "            #ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT DBG] %%s lock NOT acquired, wait\\n\", __func__);\n");
    fprintf(fout_c, "            #endif\n");
    fprintf(fout_c, "            if(sleep_time > 0) usleep(sleep_time);\n");
    fprintf(fout_c, "            continue;\n");
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "        #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "        gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT DBG] %%s lock SUCCESSFULLY acquired, time=%%ld.%%06ld\\n\", __func__, now_time.tv_sec, now_time.tv_usec);\n");
    fprintf(fout_c, "        #endif\n");

    // Check the count again after sleep for a short amount of time
    fprintf(fout_c, "        if(async_instance_g->ex_delay == false && task->async_obj->file_async_obj->attempt_check_cnt %% ASYNC_ATTEMPT_CHECK_INTERVAL == 0) {\n");
    fprintf(fout_c, "            if(sleep_time > 0) usleep(sleep_time);\n");
    fprintf(fout_c, "            if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {\n");
    fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_get_attempt_count failed\\n\", __func__);\n");
    fprintf(fout_c, "                goto done;\n");
    fprintf(fout_c, "            }\n");
    fprintf(fout_c, "            #ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "            #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "            gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT DBG] %%s after wait lock count = %%d, time=%%ld.%%06ld\\n\", __func__, new_attempt_count, now_time.tv_sec, now_time.tv_usec);\n");
    fprintf(fout_c, "            #endif\n");
    fprintf(fout_c, "            #endif\n");
    fprintf(fout_c, "            if (new_attempt_count > attempt_count) {\n");
    fprintf(fout_c, "                if (H5TSmutex_release() < 0) {\n");
    fprintf(fout_c, "                    fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_release failed\\n\", __func__);\n");
    fprintf(fout_c, "                }\n");
    fprintf(fout_c, "                #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "                gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC ABT DBG] %%s lock YIELD to main thread, time=%%ld.%%06ld\\n\", __func__, now_time.tv_sec, now_time.tv_usec);\n");
    fprintf(fout_c, "                #endif\n");
    /* fprintf(fout_c, "                usleep(1000000);\n"); */
    fprintf(fout_c, "                acquired = false;\n");
    /* fprintf(fout_c, "                continue;\n"); */
    fprintf(fout_c, "            }\n");
    fprintf(fout_c, "            else {\n");
    fprintf(fout_c, "                break;\n");
    fprintf(fout_c, "            }\n");
    fprintf(fout_c, "            attempt_count = new_attempt_count;\n");
    fprintf(fout_c, "            task->async_obj->file_async_obj->attempt_check_cnt++;\n");
    fprintf(fout_c, "            task->async_obj->file_async_obj->attempt_check_cnt %%= ASYNC_ATTEMPT_CHECK_INTERVAL;\n");
    fprintf(fout_c, "        }\n");

    fprintf(fout_c, "    }\n"); // end while (acquired == false)

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer2, NULL);\n");
    fprintf(fout_c, "    double time2 = get_elapsed_time(&timer1, &timer2);\n");
    fprintf(fout_c, "    #endif\n\n");


    fprintf(fout_c, "    #ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT DBG] %%s: global lock acquired\\n\", __func__);\n");
    fprintf(fout_c, "    fflush(stderr);\n");
    fprintf(fout_c, "    #endif\n\n");

    if (is_create == 1 ) {
        if (is_file_create) {
            fprintf(fout_c, "    async_instance_g->start_abt_push = false;\n\n");
        }

        fprintf(fout_c, "    if (1 == task->async_obj->is_obj_valid) {\n");
        fprintf(fout_c, "        #ifdef ENABLE_TIMING\n");
        fprintf(fout_c, "        gettimeofday(&now_time, NULL);\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT DBG] %%s releasing global lock, time=%%ld.%%06ld\\n\", __func__, now_time.tv_sec, now_time.tv_usec);\n");
        fprintf(fout_c, "        #endif\n");
        fprintf(fout_c, "        if (H5TSmutex_release() < 0) {\n");
        fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_release failed\\n\", __func__);\n");
        fprintf(fout_c, "        }\n");


        fprintf(fout_c, "        return;\n");
        fprintf(fout_c, "    }\n");
    }


    fprintf(fout_c, "    /* Aquire async obj mutex and set the obj */\n");
    fprintf(fout_c, "    assert(task->async_obj->obj_mutex);\n");
    fprintf(fout_c, "    assert(task->async_obj->magic == ASYNC_MAGIC);\n");
    fprintf(fout_c, "    while (1) {\n");
    fprintf(fout_c, "        if (ABT_mutex_trylock(task->async_obj->obj_mutex) == ABT_SUCCESS) {\n");
    /* fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT DBG] %%s %%p locked\\n\", __func__, task->async_obj->obj_mutex);\n"); */
    fprintf(fout_c, "            break;\n");
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "        else {\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT DBG] %%s error with try_lock\\n\", __func__);\n");
    fprintf(fout_c, "            break;\n");
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "        usleep(1000);\n");
    fprintf(fout_c, "    }\n");
    fprintf(fout_c, "    is_lock = 1;\n");
    fprintf(fout_c, "\n\n");


    fprintf(fout_c, "    // Restore previous library state\n");
    fprintf(fout_c, "    assert(task->h5_state);\n");
    fprintf(fout_c, "    if (H5VLrestore_lib_state(task->h5_state) < 0) {\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5VLrestore_lib_state failed\\n\", __func__);\n");
    fprintf(fout_c, "        goto done;\n");
    fprintf(fout_c, "    }\n");
    fprintf(fout_c, "    is_lib_state_restored = true;\n\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer3, NULL);\n");
    fprintf(fout_c, "    double time3 = get_elapsed_time(&timer2, &timer3);\n");
    fprintf(fout_c, "    #endif\n\n");

    if (is_file_create) {
        fprintf(fout_c, "    /* Get copy of our VOL info from FAPL */\n");
        fprintf(fout_c, "    H5Pget_vol_info(args->fapl_id, (void **)&info);\n\n");
        fprintf(fout_c, "    /* Copy the FAPL */\n");
        fprintf(fout_c, "    under_fapl_id = H5Pcopy(args->fapl_id);\n\n");
        fprintf(fout_c, "    /* Set the VOL ID and info for the underlying FAPL */\n");
        fprintf(fout_c, "    if (info) {\n");
        fprintf(fout_c, "        H5Pset_vol(under_fapl_id, info->under_vol_id, info->under_vol_info);\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    else {\n");
        fprintf(fout_c, "        hid_t under_vol_id;\n");
        fprintf(fout_c, "        H5Pget_vol_id(args->fapl_id, &under_vol_id);\n");
        fprintf(fout_c, "        H5Pset_vol(under_fapl_id, under_vol_id, NULL);\n");
        fprintf(fout_c, "    }\n");
    }


    if (use_obj == 1) 
        fprintf(fout_c, "    if ((obj = %s(", H5VLname);
    else if (is_close == 1) {
        fprintf(fout_c, "    if ( (ret_value = %s(", H5VLname);
    }
    else
        fprintf(fout_c, "    if ( %s(", H5VLname);
    int has_va_list = 0;
    for (i = 0; i < nargs; i++) {
        if (strcmp(arg_types[i], "va_list") == 0) 
            has_va_list = 1;
        if (strstr(arg_names[i], "param") != NULL) {
            /* fprintf(fout_c, "&"); */
            /* fprintf(fout_c, "&", arg_names[i]); */
        }
        if (strstr(arg_names[i], "fapl_id") != NULL) {
            fprintf(fout_c, "under_fapl_id, ");
            continue;
        }
        fprintf(fout_c, "args->%s", arg_names[i]);
        if (i != nargs - 1) 
            fprintf(fout_c, ", ");
        if (i == connector_loc || i == connector_loc2) 
            fprintf(fout_c, "task->under_vol_id, ");
    }

    if (is_create == 1 && is_link_create != 1) 
        fprintf(fout_c, ")) == NULL ) {\n");
    else if (is_close == 1) 
        fprintf(fout_c, ")) < 0 ) {\n");
    else
        fprintf(fout_c, ") < 0 ) {\n");

    if (is_create == 0) {
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s %s failed\\n\", __func__);\n", H5VLname);
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");
    }
    else {
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s %s failed\\n\", __func__);\n", H5VLname);
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");
    }

    if (has_va_list == 1) {
        fprintf(fout_c, "\n    /* va_end is needed as arguments is copied previously */\n");
        fprintf(fout_c, "    va_end(args->arguments);\n");
    }
    fprintf(fout_c, "\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer4, NULL);\n");
    fprintf(fout_c, "    double time4 = get_elapsed_time(&timer3, &timer4);\n");
    fprintf(fout_c, "    #endif\n\n");

    /* if (is_create == 1 || is_commit == 1) { */
    if (use_obj) {
        if (is_link_create == 1) 
            fprintf(fout_c, "    task->async_obj->under_object = args->obj;\n");
        else if (is_create == 1) 
            fprintf(fout_c, "    task->async_obj->under_object = obj;\n");
        fprintf(fout_c, "    task->async_obj->is_obj_valid = 1;\n");
        if (is_link_create == 0 && is_obj_open == 0 || strstr(funcname, "attr_create") != NULL) 
            fprintf(fout_c, "    task->async_obj->create_task = NULL;\n");
        fprintf(fout_c, "\n");
    }
    /* fprintf(fout_c, "    if (push_children_to_pool(task) != 1) {\n"); */
    /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s push_children_to_pool failed\\n\", __func__);\n"); */
    /* fprintf(fout_c, "        goto done;\n"); */
    /* fprintf(fout_c, "    }\n"); */
    fprintf(fout_c, "\n\n\n");

    fprintf(fout_c, "    #ifdef ENABLE_LOG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT LOG] Argobots execute %%s success\\n\", __func__);\n");
    fprintf(fout_c, "    #endif\n\n");

    if (is_file_create) {
        fprintf(fout_c, "    // Increase file open ref count\n");
        fprintf(fout_c, "    if (ABT_mutex_lock(async_instance_mutex_g) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC ABT ERROR] with ABT_mutex_lock\\n\");\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    };\n");

        fprintf(fout_c, "    async_instance_g->nfopen++;\n");

        fprintf(fout_c, "    if (ABT_mutex_unlock(async_instance_mutex_g) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC ABT ERROR] with ABT_mutex_ulock\\n\");\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    };\n");
    }

    if (is_file_close) {
        fprintf(fout_c, "    // Decrease file open ref count\n");
        fprintf(fout_c, "    if (ABT_mutex_lock(async_instance_mutex_g) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC ABT ERROR] with ABT_mutex_lock\\n\");\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    };\n");

        fprintf(fout_c, "    if (async_instance_g->nfopen > 0)  async_instance_g->nfopen--;\n");

        fprintf(fout_c, "    if (ABT_mutex_unlock(async_instance_mutex_g) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC ABT ERROR] with ABT_mutex_ulock\\n\");\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    };\n");
    }


    fprintf(fout_c, "\n");
    fprintf(fout_c, "done:\n");

    /* // Debug only */
    /* fprintf(fout_c, "check_tasks_object_valid(task->async_obj);\n"); */

    fprintf(fout_c, "    fflush(stdout);\n");

    if (is_file_create == 1) {
        fprintf(fout_c, "    if(under_fapl_id > 0)    H5Pclose(under_fapl_id);\n");
        fprintf(fout_c, "    if(NULL != info)         H5VL_async_info_free(info);\n");
    }

    // Free resources previously allocated
    for (i = 0; i < nargs; i++) {
        if (strstr(arg_types[i], "char") != NULL) {
            fprintf(fout_c, "    free(args->%s);\n", arg_names[i]);
            fprintf(fout_c, "    args->%s = NULL;\n", arg_names[i]);
        }
        else if (strstr(arg_names[i], "pl_id") != NULL || strstr(arg_names[i], "plist_id") != NULL) {
            fprintf(fout_c, "    if(args->%s > 0)", arg_names[i]);
            fprintf(fout_c, "    H5Pclose(args->%s);\n", arg_names[i]);
        }
        else if (strstr(arg_names[i], "space_id") ) { 
            fprintf(fout_c, "    if(args->%s > 0)", arg_names[i]);
            fprintf(fout_c, "    H5Sclose(args->%s);\n", arg_names[i]);
        }
        else if (strstr(arg_names[i], "type_id") ) {
            fprintf(fout_c, "    if(args->%s > 0)", arg_names[i]);
            fprintf(fout_c, "    H5Tclose(args->%s);\n", arg_names[i]);
        }
        else if (strstr(arg_names[i], "loc_param") != NULL) {
            fprintf(fout_c, "    free_loc_param((H5VL_loc_params_t*)args->%s);\n", arg_names[i]);
        }
    }

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer5, NULL);\n");
    fprintf(fout_c, "    double time5 = get_elapsed_time(&timer4, &timer5);\n");
    fprintf(fout_c, "    #endif\n\n");


    fprintf(fout_c, "    if (is_lock == 1) {\n");
    fprintf(fout_c, "        if (ABT_mutex_unlock(task->async_obj->obj_mutex) != ABT_SUCCESS) \n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT ERROR] %%s ABT_mutex_unlock failed\\n\", __func__);\n");
    /* fprintf(fout_c, "        else\n"); */
    /* fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC ABT DBG] %%s %%p unlocked\\n\", __func__, task->async_obj->obj_mutex);\n"); */
    fprintf(fout_c, "    }\n\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer6, NULL);\n");
    fprintf(fout_c, "    double time6 = get_elapsed_time(&timer5, &timer6);\n");
    fprintf(fout_c, "    #endif\n\n");



    fprintf(fout_c, "    ABT_eventual_set(task->eventual, NULL, 0);\n");
    fprintf(fout_c, "    task->in_abt_pool = 0;\n");
    fprintf(fout_c, "    task->is_done = 1;\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer7, NULL);\n");
    fprintf(fout_c, "    double time7 = get_elapsed_time(&timer6, &timer7);\n");
    fprintf(fout_c, "    #endif\n\n");


    fprintf(fout_c, "    if(is_lib_state_restored && H5VLreset_lib_state() < 0)\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5VLreset_lib_state failed\\n\", __func__);\n");

    fprintf(fout_c, "    if (NULL != task->h5_state && H5VLfree_lib_state(task->h5_state) < 0) \n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5VLfree_lib_state failed\\n\", __func__);\n");
    fprintf(fout_c, "    task->h5_state = NULL;\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer8, NULL);\n");
    fprintf(fout_c, "    double time8 = get_elapsed_time(&timer7, &timer8);\n");
    fprintf(fout_c, "    #endif\n\n");



    // release HDF5 library mutex
    fprintf(fout_c, "#ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT DBG] %%s releasing global lock\\n\", __func__);\n");
    fprintf(fout_c, "#endif\n");
    fprintf(fout_c, "    if (acquired == true && H5TSmutex_release() < 0) {\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_release failed\\n\", __func__);\n");
    fprintf(fout_c, "    }\n");

    if (is_create || is_raw_data) {
        fprintf(fout_c, "    if (async_instance_g && NULL != async_instance_g->qhead.queue )\n");
        fprintf(fout_c, "       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);\n");
    }
    else {
        fprintf(fout_c, "    if (async_instance_g && NULL != async_instance_g->qhead.queue && async_instance_g->start_abt_push)\n");
        fprintf(fout_c, "       push_task_to_abt_pool(&async_instance_g->qhead, *pool_ptr);\n");
    }

    if (strstr(funcname, "dataset_write") != NULL) 
        fprintf(fout_c, "    if (args->buf_free == true) free(args->buf);\n");

    if (strstr(funcname, "attr_write") != NULL) 
        fprintf(fout_c, "    free(args->buf);\n");
    /* fprintf(fout_c, "    if (H5TSmutex_get_attempt_count(&new_attempt_count) < 0) {\n"); */
    /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT ERROR] %%s H5TSmutex_get_attempt_count failed\\n\", __func__);\n"); */
    /* fprintf(fout_c, "        goto done;\n"); */
    /* fprintf(fout_c, "    }\n"); */
    /* fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC ABT DBG] %%s after execution lock count = %%d\\n\", __func__, new_attempt_count);\n"); */
    /* fprintf(fout_c, "    if (new_attempt_count > attempt_count) {\n"); */
    /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC ABT DBG] %%s lock acquired while another thread attempted to lock, sleep\\n\", __func__);\n"); */
    /* fprintf(fout_c, "        usleep(2000);\n"); */
    /* fprintf(fout_c, "    }\n"); */

    /* if (is_file_close == 1) { */
    /*     fprintf(fout_c, "    free_file_async_obj(task->async_obj);\n"); */
    /* } */
    /* else if (is_group_close == 1) { */
    /*     fprintf(fout_c, "    if(ret_value >= 0) { remove_tasks_of_closed_object(task->async_obj); H5VL_async_free_obj(task->async_obj); task->async_obj = NULL; }\n"); */
    /* } */
    /* else if (is_close == 1) { */
    /*     /1* fprintf(fout_c, "    if(ret_value >= 0) { remove_tasks_of_closed_object(task->async_obj); H5VL_async_free_obj(task->async_obj); task->async_obj = NULL; }\n"); *1/ */
    /*     fprintf(fout_c, "    if(ret_value >= 0) { H5VL_async_free_obj(task->async_obj); task->async_obj = NULL; }\n"); */
    /* } */

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&timer9, NULL);\n");
    fprintf(fout_c, "    double exec_time   = get_elapsed_time(&args->start_time, &timer9);\n");
    fprintf(fout_c, "    double total_time  = get_elapsed_time(&args->create_time, &timer9);\n");
    fprintf(fout_c, "    double wait_time   = total_time - exec_time;\n");


    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\ttotal time      : %%f\\n\", __func__, total_time);\n");
    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t  wait time     : %%f\\n\", __func__, wait_time);\n");
    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t  execute time  : %%f\\n\", __func__, exec_time);\n");
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time1       : %%f\\n\", __func__, time1);\n"); */
    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time2       : %%f\\n\", __func__, time2);\n");
    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time3       : %%f\\n\", __func__, time3);\n");
    fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time4(n.vol): %%f\\n\", __func__, time4);\n");
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time5       : %%f\\n\", __func__, time5);\n"); */
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time6       : %%f\\n\", __func__, time6);\n"); */
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time7       : %%f\\n\", __func__, time7);\n"); */
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time8       : %%f\\n\", __func__, time8);\n"); */
    /* fprintf(fout_c, "    printf(\"  [ASYNC ABT TIMING] %%-24s \\t    time9       : %%f\\n\", __func__, time9);\n"); */
    fprintf(fout_c, "    fflush(stdout);\n");
    fprintf(fout_c, "    #endif\n");

    /* // Debug only */
    /* fprintf(fout_c, "check_tasks_object_valid(task->async_obj);\n"); */

    fprintf(fout_c, "    return;\n");
    fprintf(fout_c, "} // End %s\n", fnname);
    fprintf(fout_c, "\n");





    /* ====== async_dataset_write ====== */
    char abt_name_nb[MAX_NAME_LEN];
    sprintf(abt_name_nb, "async_%s", &funcname[skip_h5_prefix]);

    int is_write=0, is_read = 0, is_raw_write = 0, is_raw_read = 0, is_attr_write = 0, is_attr_read = 0, is_file_optional = 0, is_dset_create = 0, is_dset_open = 0;
    is_attr_create = 0;
    is_raw_data = 0;
    if (strstr(abt_name_nb, "create") != NULL || strstr(abt_name_nb, "write") != NULL || strstr(abt_name_nb, "copy") != NULL) 
        is_write = 1;

    if (strstr(abt_name_nb, "read") != NULL) 
        is_read = 1;

    if (strstr(abt_name_nb, "dataset_write") != NULL) 
        is_raw_write = 1;
    if (strstr(abt_name_nb, "dataset_read") != NULL) 
        is_raw_read = 1;

    if (strstr(abt_name_nb, "attr_write") != NULL) 
        is_attr_write = 1;

    if (strstr(abt_name_nb, "dataset_create") != NULL ) 
        is_dset_create = 1;

    if (strstr(abt_name_nb, "dataset_open") != NULL ) 
        is_dset_open = 1;
    /* if (strstr(abt_name_nb, "attr_create") != NULL || strstr(abt_name_nb, "attr_open") != NULL) */ 
    /*     is_attr_create = 1; */

    if (strstr(abt_name_nb, "attr_read") != NULL) 
        is_attr_read = 1;

    if (strstr(abt_name_nb, "dataset_read") != NULL || strstr(abt_name_nb, "dataset_write") != NULL) 
        is_raw_data = 1;

    if (strstr(abt_name_nb, "datatype_commit") != NULL ) {
        is_create = 1;
    }

    if (strstr(abt_name_nb, "file_optional") != NULL) {
        is_file_optional = 1;
    }

    if (is_create == 1) {
        fprintf(fout_c, "static H5VL_async_t*\n");
    }
    else
        fprintf(fout_c, "static herr_t\n");

    /* if (is_create == 1 && is_link_create != 1) */ 
        fprintf(fout_c, "%s(int is_blocking, async_instance_t* aid, ", abt_name_nb);
    /* else */
    /*     fprintf(fout_c, "%s(int is_blocking, async_instance_t* aid, ", abt_name_nb); */

    int async_obj_loc = 0;
    int async_obj_loc2 = -1;
    if (strcmp(abt_name_nb, "async_link_create") == 0) {
        async_obj_loc = 1;
    }
    else if (strcmp(abt_name_nb, "async_link_copy") == 0 || strcmp(abt_name_nb, "async_link_move") == 0) {
        async_obj_loc = 0;
        async_obj_loc2 = 2;
    }
    else if (strcmp(abt_name_nb, "async_object_copy") == 0 ) {
        async_obj_loc = 0;
        async_obj_loc2 = 3;
    }
    else if (strstr(abt_name_nb, "file_open") != NULL || strstr(abt_name_nb, "file_create") != NULL) 
        async_obj_loc = -1;

    for (i = 0; i < nargs; i++) {
        if (i == async_obj_loc) {
            fprintf(fout_c, "H5VL_async_t *parent_obj, ");
            continue;
        }
        if (i == async_obj_loc2) {
            fprintf(fout_c, "H5VL_async_t *parent_obj2, ");
            continue;
        }
        fprintf(fout_c, "%s", arg_types[i]);
        fprintf(fout_c, " ");
        for (j = 0; j < arg_is_ptr[i]; j++) 
            fprintf(fout_c, "*");
        /* if (strstr(arg_names[i], "loc_param") != NULL) { */
        /*     fprintf(fout_c, "const *"); */
        /* } */
        fprintf(fout_c, "%s", arg_names[i]);
        if (i != nargs - 1) 
            fprintf(fout_c, ", ");
    }
    fprintf(fout_c, ")\n");


    fprintf(fout_c, "{\n");
    /* fprintf(fout_c, "    herr_t                     ret;\n"); */
    if (is_file_create == 1) {
        /* fprintf(fout_c, "    H5VL_async_info_t *info = NULL;\n"); */
        fprintf(fout_c, "    hid_t under_vol_id;\n");
    }

    /* if (is_raw_data == 0) */ 
    if (is_create == 1) 
        fprintf(fout_c, "    H5VL_async_t *async_obj = NULL;\n");

    if (is_read == 1) {
        fprintf(fout_c, "    // For implicit mode (env var), make all read to be blocking\n");
        fprintf(fout_c, "    if(aid->env_async) is_blocking = 1;\n");
    }

    if (is_raw_write == 1) {
        fprintf(fout_c, "    hbool_t enable_async = false;\n");
        fprintf(fout_c, "    hsize_t buf_size, cp_size_limit = 0;\n");
    }
    fprintf(fout_c, "    async_task_t *async_task = NULL;\n");
    fprintf(fout_c, "    H5RQ_token_int_t *token = NULL;\n");

    /* if (async_obj_loc != -1) { */
        /* fprintf(fout_c, "    // TODO: vary\n"); */
    /*     fprintf(fout_c, "    async_task_t *parent_obj = (async_task_t*)obj;\n"); */
    /* } */

    fprintf(fout_c, "    %s *args = NULL;\n", sname);
    if (is_file_create == 1) 
        fprintf(fout_c, "    int lock_self;\n");
    if (is_file_create != 1) {
        fprintf(fout_c, "    int lock_parent;\n");
    }
    fprintf(fout_c, "    hbool_t acquired = false;\n");
    fprintf(fout_c, "\n");

    fprintf(fout_c, "    #ifdef ENABLE_LOG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC VOL LOG] entering %%s\\n\", __func__);\n");
    fprintf(fout_c, "    fflush(stderr);\n");
    fprintf(fout_c, "    #endif\n");
    fprintf(fout_c, "\n");

    fprintf(fout_c, "    assert(aid);\n");
    if (is_file_create != 1) {
        fprintf(fout_c, "    assert(parent_obj);\n");
        fprintf(fout_c, "    assert(parent_obj->magic == ASYNC_MAGIC);\n");
    }
    fprintf(fout_c, "\n");

    if (is_file_create == 1 ) {
        /* fprintf(fout_c, "    /1* Get copy of our VOL info from FAPL *1/\n"); */
        /* fprintf(fout_c, "    H5Pget_vol_info(fapl_id, (void **)&info);\n\n"); */
        fprintf(fout_c, "    H5Pget_vol_id(fapl_id, &under_vol_id);\n\n");
    }

    fprintf(fout_c, "    if ((args = (%s*)calloc(1, sizeof(%s))) == NULL) {\n", sname, sname);
    fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with calloc\\n\", __func__);\n");
    fprintf(fout_c, "        goto error;\n");
    fprintf(fout_c, "    }\n");

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&args->create_time, NULL);\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC VOL TIMING] entering %%s, time=%%ld.%%06ld\\n\", __func__, args->create_time.tv_sec, args->create_time.tv_usec);\n");
    fprintf(fout_c, "    fflush(stderr);\n");
    fprintf(fout_c, "    #endif\n");


    /* Get async prop */
    if (is_raw_write == 1) {
        fprintf(fout_c, "    if (H5Pget_dxpl_async(plist_id, &enable_async) < 0) { \n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with H5Pget_dxpl_async\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");

        fprintf(fout_c, "    if (H5Pget_dxpl_async_cp_limit(plist_id, &cp_size_limit) < 0) { \n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with H5Pget_dxpl_async\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    if (cp_size_limit > 0) { enable_async = true; }\n");

        fprintf(fout_c, "    if (aid->env_async) enable_async = true;\n");

        fprintf(fout_c, "    if (enable_async == true && cp_size_limit == 0) cp_size_limit = ULONG_MAX;\n\n");
    }


    if (is_create == 1) {
        fprintf(fout_c, "    /* create a new async object */ \n");
        if (is_file_create != 1) 
            fprintf(fout_c, "    if ((async_obj = H5VL_async_new_obj(NULL, parent_obj->under_vol_id)) == NULL) { \n");
        else
            fprintf(fout_c, "    if ((async_obj = H5VL_async_new_obj(NULL, under_vol_id)) == NULL) { \n");
            /* fprintf(fout_c, "    if ((async_obj = H5VL_async_new_obj(NULL, info->under_vol_id)) == NULL) { \n"); */
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with calloc\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    async_obj->magic = ASYNC_MAGIC;\n");

        fprintf(fout_c, "    if (ABT_mutex_create(&(async_obj->obj_mutex)) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_create\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");

        if (is_file_create != 1) {
            fprintf(fout_c, "    async_obj->file_task_list_head = parent_obj->file_task_list_head;\n");
            fprintf(fout_c, "    async_obj->file_async_obj      = parent_obj->file_async_obj;\n");
            fprintf(fout_c, "    async_obj->is_col_meta = parent_obj->is_col_meta;\n");
        }
        else {
            fprintf(fout_c, "    async_obj->file_async_obj      = async_obj;\n");
            fprintf(fout_c, "    if (ABT_mutex_create(&(async_obj->file_task_list_mutex)) != ABT_SUCCESS) {\n");
            fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_create\\n\", __func__);\n");
            fprintf(fout_c, "        goto error;\n");
            fprintf(fout_c, "    }\n");
        }
        fprintf(fout_c, "    async_obj->pool_ptr = &aid->pool;\n");
    }

    fprintf(fout_c, "    /* create a new task and insert into its file task list */ \n");
    fprintf(fout_c, "    if ((async_task = (async_task_t*)calloc(1, sizeof(async_task_t))) == NULL) { \n");
    fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with calloc\\n\", __func__);\n");
    fprintf(fout_c, "        goto error;\n");
    fprintf(fout_c, "    }\n");
    fprintf(fout_c, "    if (ABT_mutex_create(&(async_task->task_mutex)) != ABT_SUCCESS) {\n");
    fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_create\\n\", __func__);\n");
    fprintf(fout_c, "        goto error;\n");
    fprintf(fout_c, "    }\n");
    /* fprintf(fout_c, "    // TODO: vary\n"); */
    fprintf(fout_c, "\n");

    if (strstr(abt_name_nb, "link_copy") != NULL || strstr(abt_name_nb, "link_move") != NULL|| strstr(abt_name_nb, "object_copy") != NULL) {
         for (i = 0; i < nargs; i++) {
            if (strstr(arg_names[i], "src_obj") != NULL) 
                fprintf(fout_c, "    args->%-16s = parent_obj->under_object;\n", arg_names[i]);
            else if (strstr(arg_names[i], "dst_obj") != NULL) 
                fprintf(fout_c, "    args->%-16s = parent_obj2->under_object;\n", arg_names[i]);
            else if (strstr(arg_names[i], "loc_param") != NULL) {
                /* fprintf(fout_c, "    memcpy(&args->%s, %s, sizeof(*%s));\n", arg_names[i], arg_names[i], arg_names[i]); */
                fprintf(fout_c, "    args->%s = (H5VL_loc_params_t*)calloc(1, sizeof(*%s));\n", arg_names[i], arg_names[i]);
                fprintf(fout_c, "    dup_loc_param(args->%s, %s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_types[i], "char") != NULL) 
                fprintf(fout_c, "    args->%-16s = strdup(%s);\n", arg_names[i], arg_names[i]);
            else if (strstr(arg_names[i], "pl_id") != NULL || strstr(arg_names[i], "plist_id") != NULL) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Pcopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_names[i], "space_id") ) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Scopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_names[i], "type_id") ) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Tcopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else {
                fprintf(fout_c, "    args->%-16s = %s;\n", arg_names[i], arg_names[i]);
            }
        }
    }
    else {
        i=0;
        /* fprintf(fout_c, "    // TODO: vary\n"); */
        if (is_file_create != 1 && is_link_create != 1) {
            fprintf(fout_c, "    args->%-16s = parent_obj->under_object;\n", arg_names[0]);
            i=1;
        }


        for (; i < nargs; i++) {
            if (strcmp(arg_types[i], "va_list") == 0) {
                fprintf(fout_c, "    va_copy(args->%s, %s);\n", arg_names[i], arg_names[i]);
                continue;
            }
            else if (is_attr_write == 1 && strstr(arg_names[i], "buf") != NULL) {
                continue;
            }
            if (strstr(arg_names[i], "obj") != NULL) 
                fprintf(fout_c, "    args->obj              = parent_obj->under_object;\n");
            else if (strstr(arg_types[i], "char") != NULL) 
                fprintf(fout_c, "    args->%-16s = strdup(%s);\n", arg_names[i], arg_names[i]);
            else if (strstr(arg_names[i], "loc_param") != NULL) {
                /* fprintf(fout_c, "    memcpy(&args->%s, %s, sizeof(*%s));\n", arg_names[i], arg_names[i], arg_names[i]); */
                fprintf(fout_c, "    args->%s = (H5VL_loc_params_t*)calloc(1, sizeof(*%s));\n", arg_names[i], arg_names[i]);
                fprintf(fout_c, "    dup_loc_param(args->%s, %s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_names[i], "pl_id") != NULL || strstr(arg_names[i], "plist_id") != NULL) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Pcopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_names[i], "space_id") ) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Scopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else if (strstr(arg_names[i], "type_id") ) {
                fprintf(fout_c, "    if(%s > 0)\n", arg_names[i]);
                fprintf(fout_c, "        args->%s = H5Tcopy(%s);\n", arg_names[i], arg_names[i]);
            }
            else if (is_raw_write == 1 && strstr(arg_names[i], "buf") != NULL) {
                fprintf(fout_c, "    args->%-16s = (void*)%s;\n", arg_names[i], arg_names[i]);
            }
            else
                fprintf(fout_c, "    args->%-16s = %s;\n", arg_names[i], arg_names[i]);
        }
    }

    fprintf(fout_c, "\n");
    fprintf(fout_c, "    if (req) {\n");
    fprintf(fout_c, "        token = H5RQ__new_token();\n");
    fprintf(fout_c, "        if (token == NULL) {\n");
    fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC VOL ERROR] %%s token is NULL!\\n\", __func__);\n");
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "        else {\n");
    fprintf(fout_c, "            token->task = async_task;\n");
    fprintf(fout_c, "            async_task->token = token;\n");
    fprintf(fout_c, "            *req = (void*)token;\n");
    if (is_raw_write == 1) {
        fprintf(fout_c, "            cp_size_limit = 0;\n");
    }
    fprintf(fout_c, "        }\n");
    fprintf(fout_c, "    }\n\n");

    /* if (is_attr_create == 1) { */
    /*     hsize_t npts = H5Sget_simple_extent_npoints(mem_type_id); */
    /*     args->attr_size = ; */
    /* } */

    if (is_attr_write == 1) {
        // copy attr buf for ASYNC_VOL_ATTR_CP_SIZE_LIMIT
        fprintf(fout_c, "    if (NULL == (args->buf = malloc(ASYNC_VOL_ATTR_CP_SIZE_LIMIT))) {\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s malloc failed!\\n\", __func__);\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    memcpy(args->buf, buf, ASYNC_VOL_ATTR_CP_SIZE_LIMIT);\n");
    }
 
    if (is_raw_write == 1) {
        // copy data buf if its within the size limit

        fprintf(fout_c, "    if (cp_size_limit > 0) { \n");
        /* fprintf(fout_c, "    if (cp_size_limit != ULONG_MAX && cp_size_limit > 0) { \n"); */
        fprintf(fout_c, "        if (parent_obj->has_dset_size && args->file_space_id == H5S_ALL && parent_obj->dset_size > 0) { \n");
        fprintf(fout_c, "            buf_size = parent_obj->dset_size;\n");
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        else { \n");
        fprintf(fout_c, "            if (parent_obj->dtype_size > 0) {\n");
        fprintf(fout_c, "                if (H5VLasync_get_data_nelem(args->dset, args->file_space_id, parent_obj->under_vol_id, &buf_size) < 0) {\n");
        fprintf(fout_c, "                    fprintf(stderr,\"  [ASYNC VOL ERROR] %%s H5VLasync_get_data_nelem failed\\n\", __func__);\n");
        fprintf(fout_c, "                    goto done;\n");
        fprintf(fout_c, "                }\n");
        fprintf(fout_c, "                buf_size *= parent_obj->dtype_size;\n");
        fprintf(fout_c, "            }\n");
        fprintf(fout_c, "            else\n");
        fprintf(fout_c, "                if (H5VLasync_get_data_size(args->dset, args->file_space_id, parent_obj->under_vol_id, &buf_size) < 0) {\n");
        fprintf(fout_c, "                    fprintf(stderr,\"  [ASYNC VOL ERROR] %%s H5VLasync_get_data_size failed\\n\", __func__);\n");
        fprintf(fout_c, "                    goto done;\n");
        fprintf(fout_c, "                }\n");
        fprintf(fout_c, "        }\n");


        fprintf(fout_c, "        if (buf_size <= cp_size_limit) { \n");
        fprintf(fout_c, "            if (NULL == (args->buf = malloc(buf_size))) {\n");
        fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC VOL ERROR] %%s malloc failed!\\n\", __func__);\n");
        fprintf(fout_c, "                goto done;\n");
        fprintf(fout_c, "            }\n");
        fprintf(fout_c, "            memcpy(args->buf, buf, buf_size);\n");
        fprintf(fout_c, "            args->buf_free = true;\n");
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        else { \n");
        fprintf(fout_c, "            enable_async = false;\n");
        fprintf(fout_c, "            fprintf(stdout,\"  [ASYNC VOL] %%s buf size [%%llu] is larger than cp_size_limit [%%llu], using synchronous write\\n\", __func__, buf_size, cp_size_limit);\n");
        fprintf(fout_c, "        } \n");
        fprintf(fout_c, "    }\n\n");
    }
        
    fprintf(fout_c, "\n");
    fprintf(fout_c, "    // Retrieve current library state\n");
    fprintf(fout_c, "    if ( H5VLretrieve_lib_state(&async_task->h5_state) < 0) {\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s H5VLretrieve_lib_state failed\\n\", __func__);\n");
    fprintf(fout_c, "        goto done;\n");
    fprintf(fout_c, "    }\n\n");

    fprintf(fout_c, "    async_task->func       = %s; \n", fnname);
    fprintf(fout_c, "    async_task->args       = args;\n");
    if (is_write == 1 || is_close == 1) 
        fprintf(fout_c, "    async_task->op         = WRITE;\n");
    else
        fprintf(fout_c, "    async_task->op         = READ;\n");

    if (is_file_create == 1) {
        fprintf(fout_c, "    async_task->under_vol_id  = under_vol_id;\n");
        /* fprintf(fout_c, "    async_task->under_vol_id  = info->under_vol_id;\n"); */
        /* fprintf(fout_c, "    assert(async_task->under_vol_id);    // TODO\n"); */
    }
    else
        fprintf(fout_c, "    async_task->under_vol_id  = parent_obj->under_vol_id;\n");

    // File create does not have parent, has async_obj
    // others all have parent, creates also has aync_obj, rest don't
    // creates have async_obj, 
    // async_obj: all creates
    // parents: all except file creates

    if (is_create == 1 && is_link_create != 1) 
        fprintf(fout_c, "    async_task->async_obj  = async_obj;\n");
    else
        fprintf(fout_c, "    async_task->async_obj  = parent_obj;\n");

    if (is_file_create != 1) {
        /* fprintf(fout_c, "    // TODO: vary\n"); */
        fprintf(fout_c, "    async_task->parent_obj = parent_obj;\n");
    }

    fprintf(fout_c, "    if (ABT_eventual_create(0, &async_task->eventual) != ABT_SUCCESS) {\n");
    fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s ABT_eventual_create failed\\n\", __func__);\n");
    fprintf(fout_c, "        goto error;\n");
    fprintf(fout_c, "    }\n");
    fprintf(fout_c, "\n");

    if (is_file_create == 1) {
        fprintf(fout_c, "    /* Lock async_obj */\n");
        fprintf(fout_c, "    while (1) {\n");
        fprintf(fout_c, "        if (async_obj->obj_mutex && ABT_mutex_trylock(async_obj->obj_mutex) == ABT_SUCCESS) {\n");
    /* fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p locked\\n\", __func__, async_obj->obj_mutex);\n"); */
        fprintf(fout_c, "            break;\n");
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        else \n");
        fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC VOL DBG] %%s error with try_lock\\n\", __func__);\n");
        fprintf(fout_c, "        usleep(1000);\n");
        fprintf(fout_c, "    }\n");

        fprintf(fout_c, "    lock_self = 1;\n");
        fprintf(fout_c, "\n");
    }
    if (is_create == 1) {
        /* fprintf(fout_c, "    // TODO: vary\n"); */
        fprintf(fout_c, "    async_obj->create_task = async_task;\n");
        fprintf(fout_c, "    async_obj->under_vol_id = async_task->under_vol_id;\n");
    }

    if (is_dset_create == 1) {
        fprintf(fout_c, "    async_obj->dtype_size = H5Tget_size(type_id);\n");
        fprintf(fout_c, "    async_obj->dset_size = async_obj->dtype_size * H5Sget_simple_extent_npoints(space_id);\n");
        fprintf(fout_c, "    if (async_obj->dset_size > 0)\n");
        fprintf(fout_c, "        async_obj->has_dset_size = true;\n");
    }

    fprintf(fout_c, "\n");

    // async_obj: all creates
    // parents: all except file creates
    if (is_file_create != 1) {
        fprintf(fout_c, "    /* Lock parent_obj */\n");
        fprintf(fout_c, "    while (1) {\n");
        fprintf(fout_c, "        if (parent_obj->obj_mutex && ABT_mutex_trylock(parent_obj->obj_mutex) == ABT_SUCCESS) {\n");
    /* fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p locked\\n\", __func__, parent_obj->obj_mutex);\n"); */
        fprintf(fout_c, "            break;\n");
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        usleep(1000);\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    lock_parent = 1;\n");
        fprintf(fout_c, "\n");

        fprintf(fout_c, "    if (ABT_mutex_lock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s with ABT_mutex_lock\\n\", __func__);\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");

        fprintf(fout_c, "    /* Insert it into the file task list */\n");
        fprintf(fout_c, "    DL_APPEND2(parent_obj->file_task_list_head, async_task, file_list_prev, file_list_next);\n");

        fprintf(fout_c, "    if (ABT_mutex_unlock(parent_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");

        fprintf(fout_c, "    parent_obj->task_cnt++;\n");
        fprintf(fout_c, "    parent_obj->pool_ptr = &aid->pool;\n");
    }
    else {
        fprintf(fout_c, "    if (ABT_mutex_lock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s with ABT_mutex_lock\\n\", __func__);\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    /* Insert it into the file task list */\n");
        fprintf(fout_c, "    DL_APPEND2(async_obj->file_task_list_head, async_task, file_list_prev, file_list_next);\n");

        fprintf(fout_c, "    if (ABT_mutex_unlock(async_obj->file_async_obj->file_task_list_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        fprintf(fout_c, "        goto done;\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    async_obj->task_cnt++;\n");
        fprintf(fout_c, "    async_obj->pool_ptr = &aid->pool;\n");
        /* if (is_write == 1) */ 
        fprintf(fout_c, "    H5Pget_coll_metadata_write(fapl_id, &async_obj->is_col_meta);\n");
        /* else */
        /*     fprintf(fout_c, "    H5Pget_all_coll_metadata_ops(fapl_id, &async_obj->is_col_meta);\n"); */
        
    }

    if (is_file_create != 1) {
        fprintf(fout_c, "    /* Check if its parent has valid object */\n");
        fprintf(fout_c, "    if (parent_obj->is_obj_valid != 1) {\n");
        fprintf(fout_c, "        if (NULL != parent_obj->create_task) {\n");

        fprintf(fout_c, "            add_task_to_queue(&aid->qhead, async_task, DEPENDENT);\n");
        /* fprintf(fout_c, "            execute_parent_task_recursive(parent_obj->create_task);\n\n"); */
        /* fprintf(fout_c, "            add_task_to_queue(&aid->qhead, async_task, REGULAR);\n"); */
        /* fprintf(fout_c, "            if (add_task_to_parent(parent_obj->create_task, async_task) != 1) {\n"); */
        /* fprintf(fout_c, "                fprintf(stderr,\"  [ASYNC VOL ERROR] %%s add_task_to_parent failed\\n\", __func__);\n"); */
        /* fprintf(fout_c, "                goto error; \n"); */
        /* fprintf(fout_c, "            }\n"); */
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "        else {\n");
        fprintf(fout_c, "            fprintf(stderr,\"  [ASYNC VOL ERROR] %%s parent task not created\\n\", __func__);\n");
        fprintf(fout_c, "            goto error; \n");
        fprintf(fout_c, "        }\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "    else {\n");
        /* fprintf(fout_c, "        /1* TODO: Add to task queue *1/\n"); */

        if (is_raw_data == 0) {
            /* if (is_close == 1) */ 
            /*     fprintf(fout_c, "        async_obj = parent_obj;\n"); */
            fprintf(fout_c, "        if (async_task->async_obj->is_col_meta == true) \n");
        }
        else {
            fprintf(fout_c, "        H5FD_mpio_xfer_t xfer_mode;\n");
            fprintf(fout_c, "        H5Pget_dxpl_mpio(plist_id, &xfer_mode);\n");
            fprintf(fout_c, "        if (xfer_mode == H5FD_MPIO_COLLECTIVE) \n");
        }
        fprintf(fout_c, "            add_task_to_queue(&aid->qhead, async_task, COLLECTIVE);\n");
        fprintf(fout_c, "        else\n");
        fprintf(fout_c, "            add_task_to_queue(&aid->qhead, async_task, REGULAR);\n");
        fprintf(fout_c, "    }\n");
        fprintf(fout_c, "\n");
        fprintf(fout_c, "    if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");
        /* fprintf(fout_c, "    else\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p unlocked\\n\", __func__, parent_obj->obj_mutex);\n"); */
        fprintf(fout_c, "    lock_parent = 0;\n");
    }
    else {
        /* fprintf(fout_c, "    add_task_to_queue(&aid->qhead, async_task, DEPENDENT);\n"); */
        fprintf(fout_c, "    add_task_to_queue(&aid->qhead, async_task, REGULAR);\n");
        /* fprintf(fout_c, "    /1* Add task to pool directly, as it does not has a dependent parent pending *1/\n"); */
        /* fprintf(fout_c, "    if(ABT_task_create(aid->pool, async_task->func, async_task, &async_task->abt_task) != ABT_SUCCESS) {\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s ABT_task_create failed\\n\", __func__);\n"); */
        /* fprintf(fout_c, "        goto error; \n"); */
        /* fprintf(fout_c, "    }\n"); */
        /* fprintf(fout_c, "    if(ABT_thread_create(aid->pool, async_task->func, async_task, ABT_THREAD_ATTR_NULL, &async_task->abt_thread) != ABT_SUCCESS) {\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s ABT_thread_create failed\\n\", __func__);\n"); */
        /* fprintf(fout_c, "        goto error; \n"); */
        /* fprintf(fout_c, "    }\n"); */

        fprintf(fout_c, "    if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) {\n");
        fprintf(fout_c, "        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        fprintf(fout_c, "        goto error;\n");
        fprintf(fout_c, "    }\n");
        /* fprintf(fout_c, "    else\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p unlocked\\n\", __func__, async_obj->obj_mutex);\n"); */
        fprintf(fout_c, "    lock_self = 0;\n");
        fprintf(fout_c, "\n");
    }

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    struct timeval now_time;\n");
    fprintf(fout_c, "    gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "    printf(\"  [ASYNC VOL TIMING] %%-24s \\t  create time   : %%f\\n\",\n\t\t __func__, get_elapsed_time(&args->create_time, &now_time));\n");
    fprintf(fout_c, "    #endif\n");

    char *spaces="";
    /* if (is_create) { */
    /*     fprintf(fout_c, "    usleep(1000);\n"); */
    /* } */

    /* if (1 == is_close ) { */
    /* if (1 == is_group_close ) { */
    if (is_file_create || is_file_optional) {
        fprintf(fout_c, "%s    if (get_n_running_task_in_queue(async_task) == 0)\n", spaces);
        fprintf(fout_c, "%s        push_task_to_abt_pool(&aid->qhead, aid->pool);\n", spaces);
    }
    else {

        fprintf(fout_c, "%s    if (aid->ex_delay == false) {\n", spaces);
        fprintf(fout_c, "%s        if (get_n_running_task_in_queue(async_task) == 0)\n", spaces);
        fprintf(fout_c, "%s            push_task_to_abt_pool(&aid->qhead, aid->pool);\n", spaces);
        fprintf(fout_c, "%s    }\n\n", spaces);
        if (1 == is_close ) {
            fprintf(fout_c, "%s    else {\n", spaces);
            /* if (1 == is_file_close) { */
            /*     fprintf(fout_c, "%s        if (aid->ex_fclose) {\n", spaces); */
            /* } */
            if (1 == is_group_close) {
                fprintf(fout_c, "%s        if (aid->ex_gclose) {\n", spaces);
            }
            else if (1 == is_dset_close) {
                fprintf(fout_c, "%s        if (aid->ex_dclose) {\n", spaces);
            }
            fprintf(fout_c, "%s            if (get_n_running_task_in_queue(async_task) == 0)\n", spaces);
            fprintf(fout_c, "%s                push_task_to_abt_pool(&aid->qhead, aid->pool);\n\n", spaces);
            /* 1 == is_file_close */
            if (1 == is_group_close || 1 == is_dset_close) {
                fprintf(fout_c, "%s        }\n", spaces);
            }
            fprintf(fout_c, "%s    }\n\n", spaces);
            fprintf(fout_c, "%s    aid->start_abt_push = true;\n", spaces);
        }

    }

    // H5*close close is blocking so we can finishe all tasks before a file is closed
    /* if (is_raw_data == 0 && 1 != is_close) { */
    if (is_raw_data == 0 ) {
    /* if (is_raw_data == 0 && 1 != is_file_close && 1 != is_group_close) { */
        fprintf(fout_c, "    /* Wait if blocking is needed */\n");
        fprintf(fout_c, "    if (is_blocking == 1) {\n");
        spaces="    ";
    }

    // No blokcing if token is not null
    /* if (1 == is_file_close ) { */
    /*     fprintf(fout_c, "    /1* Wait if blocking is needed *1/\n"); */
    /*     fprintf(fout_c, "    if (token && is_blocking == 1) {\n"); */
    /*     spaces="    "; */
    /* } */

    
    if (is_raw_write == 1) {
        fprintf(fout_c, "    if (is_blocking == 1 ||enable_async == false) {\n");
        spaces="    ";
    }
    else if (is_raw_data == 1) {
        fprintf(fout_c, "    if (is_blocking == 1) {\n");
        spaces="    ";
    }

    fprintf(fout_c, "%s    if (get_n_running_task_in_queue(async_task) == 0)\n", spaces);
    fprintf(fout_c, "%s        push_task_to_abt_pool(&aid->qhead, aid->pool);\n\n", spaces);
    fprintf(fout_c, "%s    if (H5TSmutex_release() < 0) {\n", spaces);
    fprintf(fout_c, "%s        fprintf(stderr,\"  [ASYNC VOL ERROR] %%s H5TSmutex_release failed\\n\", __func__);\n", spaces);
    fprintf(fout_c, "%s    }\n", spaces);

    fprintf(fout_c, "%s    #ifdef ENABLE_DBG_MSG\n", spaces);
    fprintf(fout_c, "%s    fprintf(stderr,\"  [ASYNC VOL DBG] %%s waiting to finish all previous tasks\\n\", __func__);\n", spaces);
    fprintf(fout_c, "%s    fflush(stderr);\n", spaces);
    fprintf(fout_c, "%s    #endif\n", spaces);
    fprintf(fout_c, "%s    if (ABT_eventual_wait(async_task->eventual, NULL) != ABT_SUCCESS) {\n", spaces);
    fprintf(fout_c, "%s        fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_eventual_wait\\n\", __func__);\n", spaces);
    fprintf(fout_c, "%s        goto error; \n", spaces);
    fprintf(fout_c, "%s    }\n", spaces);

    fprintf(fout_c, "%s    #ifdef ENABLE_DBG_MSG\n", spaces);
    fprintf(fout_c, "%s    fprintf(stderr,\"  [ASYNC VOL DBG] %%s finished all previous tasks, proceed\\n\", __func__);\n", spaces);
    fprintf(fout_c, "%s    fflush(stderr);\n", spaces);
    fprintf(fout_c, "%s    #endif\n", spaces);

    fprintf(fout_c, "%s    while (acquired == false) {\n", spaces);
    fprintf(fout_c, "%s        if (H5TSmutex_acquire(&acquired) < 0) {\n", spaces);
    fprintf(fout_c, "%s            fprintf(stderr,\"  [ASYNC VOL ERROR] %%s H5TSmutex_acquire failed\\n\", __func__);\n", spaces);
    fprintf(fout_c, "%s            goto done;\n", spaces);
    fprintf(fout_c, "%s        }\n", spaces);
    fprintf(fout_c, "%s    }\n", spaces);


    if (is_raw_data == 0 ) 
    /* if (is_raw_data == 0 && 1 != is_close) */ 
    /* if (is_raw_data == 0 && 1 != is_file_close && 1 != is_group_close) */ 
        fprintf(fout_c, "    }\n");

    /* if (1 == is_file_close ) */ 
    /*     fprintf(fout_c, "    }\n"); */

    if (is_raw_data == 1) 
        fprintf(fout_c, "    }\n");

    fprintf(fout_c, "\n");


    /* if (is_file_create == 1) { */
    /*     fprintf(fout_c, "    /1* Release copy of our VOL info *1/\n"); */
    /*     fprintf(fout_c, "    H5VL_async_info_free(info);\n\n"); */
    /* } */

    fprintf(fout_c, "    #ifdef ENABLE_TIMING\n");
    fprintf(fout_c, "    gettimeofday(&now_time, NULL);\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC VOL TIMING] leaving %%s, time=%%ld.%%06ld\\n\", __func__, now_time.tv_sec, now_time.tv_usec);\n");
    fprintf(fout_c, "    #endif\n");

    // async_obj: all creates
    // parents: all except file creates
    fprintf(fout_c, "    #ifdef ENABLE_DBG_MSG\n");
    fprintf(fout_c, "    fprintf(stderr,\"  [ASYNC VOL DBG] leaving %%s \\n\", __func__);\n");
    fprintf(fout_c, "    #endif\n");
    fprintf(fout_c, "\n");

    // Debug only
    /* fprintf(fout_c, "check_tasks_object_valid(async_task->async_obj);\n"); */

    fprintf(fout_c, "done:\n");
    fprintf(fout_c, "    fflush(stdout);\n");

    if (is_create == 1) 
        fprintf(fout_c, "    return async_obj;\n");
    else
        fprintf(fout_c, "    return 1;\n");
    fprintf(fout_c, "error:\n");
    if (is_file_create != 1) {
        fprintf(fout_c, "    if (lock_parent == 1) {\n");
        fprintf(fout_c, "        if (ABT_mutex_unlock(parent_obj->obj_mutex) != ABT_SUCCESS) \n");
        fprintf(fout_c, "            fprintf(stderr, \"  [ASYNC VOL ERROR] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        /* fprintf(fout_c, "    else\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p unlocked\\n\", __func__, parent_obj->obj_mutex);\n"); */
        fprintf(fout_c, "    }\n");
    }
    if (is_file_create == 1) {
        fprintf(fout_c, "    if (lock_self == 1) {\n");
        fprintf(fout_c, "        if (ABT_mutex_unlock(async_obj->obj_mutex) != ABT_SUCCESS) \n");
        fprintf(fout_c, "            fprintf(stderr, \"  [ASYNC VOL DBG] %%s with ABT_mutex_unlock\\n\", __func__);\n");
        /* fprintf(fout_c, "    else\n"); */
        /* fprintf(fout_c, "        fprintf(stderr,\"  [ASYNC VOL DBG] %%s %%p unlocked\\n\", __func__, async_obj->obj_mutex);\n"); */
        fprintf(fout_c, "    }\n");
    }

    /* fprintf(fout_c, "    if (NULL != async_task) { free(async_task); }\n"); */
    fprintf(fout_c, "    if (NULL != args) free(args);\n");
    /* if (is_file_create) { */
    /*     fprintf(fout_c, "    if (NULL != info)       { H5VL_async_info_free(info); }\n"); */
    /* } */



    if (is_create == 1) 
        fprintf(fout_c, "    return NULL;\n");
    else
        fprintf(fout_c, "    return -1;\n");
    fprintf(fout_c, "} // End %s\n\n", abt_name_nb);

    fprintf(fout_c, "\n\n");

}
