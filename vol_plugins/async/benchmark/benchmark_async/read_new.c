#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>

#include "hdf5.h"
#include "h5_vol_external_async_native.h"

#define NDIM 1

void print_usage(const char *exename)
{
    printf("Usage: ./%s n_groups n_dset_per_group n_mb_per_dset n_attr_per_grp n_byte_per_attr file_path sleep_time\n", exename);
}

double get_elapsed_time_double(struct timeval *tstart, struct timeval *tend)
{
    return (double)(((tend->tv_sec-tstart->tv_sec)*1000000LL + tend->tv_usec-tstart->tv_usec) / 1000000.0);
}

int main(int argc, char *argv[])
{
    int n_grp, n_dset_per_grp, n_attr_per_grp, n_mb_per_dset, n_byte_per_attr, i, j, k, t, m, sleep_time;
    char *filepath = NULL;
    char filename[128] = {0};
    char grp_name[128];
    char dset_name[128];
    char attr_name[128];
    herr_t status;
    hid_t  fid, **dset_ids = NULL, *grp_ids = NULL, dcpl, **attr_ids = NULL, async_fapl, async_dxpl;
    struct timeval  timer1;
    struct timeval  timer2;
    struct timeval  timer3;
    double total_time;

    hsize_t dims[1] = {1};                       /* Dimension sizes */
    void *data = NULL, *attr_data = NULL;

    int nthread = 1;
    async_fapl  = H5Pcreate (H5P_FILE_ACCESS);
    async_dxpl  = H5Pcreate (H5P_DATASET_XFER);
    H5Pset_vol_async(async_fapl, nthread);
    H5Pset_dxpl_async(async_dxpl, true);

    if (argc != 8) {
        print_usage(argv[0]);
        goto exit;
    }
    else {
        n_grp            = atol(argv[1]);
        n_dset_per_grp   = atoi(argv[2]);
        n_mb_per_dset    = atoi(argv[3]);
        n_attr_per_grp   = atoi(argv[4]);
        n_byte_per_attr  = atoi(argv[5]);
        filepath         = argv[6];
        sleep_time       = atoi(argv[7]);
        sprintf(filename, "%s/async_%dgrp_%ddsets_%dMB_%dattr_%dB.h5", 
                filepath, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp, n_byte_per_attr);
    }

    printf("Reading from %s, %d groups, %d dset (%dMB) per group, %d attr (%dB) per group, sleep %ds\n", 
            filename, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp, n_byte_per_attr, sleep_time);

    hsize_t nint = n_mb_per_dset * 1048576 / sizeof(int);
    dims[0] = nint;

    fid = H5Fopen(filename, H5F_ACC_RDONLY, async_fapl);
    if (fid < 0) {
        printf("Error opening file %s\n", filename);
        goto exit;
    }
    if (dims[0] > 0) 
        data = malloc(dims[0] * sizeof(int));
    if (n_byte_per_attr > 0) 
        attr_data = malloc(n_byte_per_attr);

    grp_ids  = (hid_t*)calloc(n_grp, sizeof(hid_t));
    dset_ids = (hid_t**)calloc(n_grp, sizeof(hid_t*));
    attr_ids = (hid_t**)calloc(n_grp, sizeof(hid_t*)); 
    for (i = 0; i < n_grp; i++) {
        dset_ids[i] = (hid_t*)calloc(n_dset_per_grp, sizeof(hid_t));
        attr_ids[i] = (hid_t*)calloc(n_attr_per_grp, sizeof(hid_t));
    }

    gettimeofday(&timer1, 0);

    // Open and read first timestep
    i = 0;
    sprintf(grp_name, "Group%d", i);
    if((grp_ids[i] = H5Gopen(fid, grp_name, H5P_DEFAULT)) < 0)
        printf("Error opening group [%s]\n", grp_name);

    for (k = 0; k < n_attr_per_grp; k++) {
        sprintf(attr_name, "Attr%d", k);
        if((attr_ids[i][k] = H5Aopen(grp_ids[i], attr_name, H5P_DEFAULT)) < 0)
            printf("Error opening attr [%s]\n", attr_name);
    }

    for (j = 0; j < n_dset_per_grp; j++) {
        sprintf(dset_name, "Dset%d", j);
        if((dset_ids[i][j] = H5Dopen(grp_ids[i], dset_name, H5P_DEFAULT)) < 0)
            printf("Error opening dset [%s]\n", dset_name);
    }

    for (k = 0; k < n_attr_per_grp; k++) {
        if(H5Aread(attr_ids[i][k], H5T_NATIVE_INT, attr_data) < 0)
            printf("Error reading attr [%s]\n", attr_name);
    }

    for (j = 0; j < n_dset_per_grp; j++) {
        if (nint > 0 && H5Dread(dset_ids[i][j], H5T_NATIVE_INT, H5S_ALL, H5S_ALL, async_dxpl, data) < 0)
            printf("Error reading dset [%s]\n", dset_name);
    }

    gettimeofday(&timer2, 0);
    total_time = get_elapsed_time_double(&timer1, &timer2);
    printf("Group %d read time: %.4f\n", i, total_time);

    // Prefetch next timestep
    for (i = 1; i < n_grp; i++) {
        gettimeofday(&timer2, 0);

        sprintf(grp_name, "Group%d", i);
        if((grp_ids[i] = H5Gopen(fid, grp_name, H5P_DEFAULT)) < 0)
            printf("Error opening group [%s]\n", grp_name);

        for (k = 0; k < n_attr_per_grp; k++) {
            sprintf(attr_name, "Attr%d", k);
            if((attr_ids[i][k] = H5Aopen(grp_ids[i], attr_name, H5P_DEFAULT)) < 0)
                printf("Error opening attr [%s]\n", attr_name);
        }

        for (j = 0; j < n_dset_per_grp; j++) {
            sprintf(dset_name, "Dset%d", j);
            if((dset_ids[i][j] = H5Dopen(grp_ids[i], dset_name, H5P_DEFAULT)) < 0)
                printf("Error opening dset [%s]\n", dset_name);
        }

        // Read
        for (k = 0; k < n_attr_per_grp; k++) {
            if(H5Aread(attr_ids[i][k], H5T_NATIVE_INT, attr_data) < 0)
                printf("Error reading attr [%s]\n", attr_name);
        }

        for (j = 0; j < n_dset_per_grp; j++) {
            if (nint > 0 && H5Dread(dset_ids[i][j], H5T_NATIVE_INT, H5S_ALL, H5S_ALL, async_dxpl, data) < 0)
                printf("Error reading dset [%s]\n", dset_name);
        }

        gettimeofday(&timer3, 0);
        total_time = get_elapsed_time_double(&timer2, &timer3);
        printf("Group %d prefetch time: %.4f\n", i, total_time);

        // Fake compute time
        if (sleep_time > 0) {
            usleep(sleep_time*1000000);
        }

        gettimeofday(&timer2, 0);
        t = i;
        if (i == 1) 
            t = 0;
        for (; t <= i ; t++) {
            for (k = 0; k < n_attr_per_grp; k++) {
                H5Aclose(attr_ids[t][k]);
                /* printf("attr %d %d closed\n", t, k); */
            }

            for (j = 0; j < n_dset_per_grp; j++) 
                H5Dclose(dset_ids[t][j]);

            H5Gclose(grp_ids[t]);
        }
        gettimeofday(&timer3, 0);
        total_time = get_elapsed_time_double(&timer2, &timer3);
        printf("Group %d close time: %.4f\n", i, total_time);
        H5Fwait(fid);
    } 


    gettimeofday(&timer2, 0);
    total_time = get_elapsed_time_double(&timer1, &timer2);
    printf("Total read time:%.4f\n", total_time);

    if (dims[0] > 0) 
        free(data);
    if (n_byte_per_attr > 0) 
        free(attr_data);
    for (i = 0; i < n_grp; i++) {
        if (dset_ids[i]) 
            free(dset_ids[i]);
        if (attr_ids[i]) 
            free(attr_ids[i]);
    }
    if (dset_ids) 
        free(dset_ids);
    if (attr_ids) 
    free(attr_ids);
    free(grp_ids);

    status = H5Pclose(async_fapl);
    status = H5Pclose(async_dxpl);
    status = H5Fclose(fid);

exit:
    return 0;
}

