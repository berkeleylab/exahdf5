#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <sched.h>
#include <pthread.h>

#include "hdf5.h"
#include "h5_vol_external_async_native.h"

#define NDIM 1

void print_usage(const char *exename)
{
    printf("Usage: ./%s n_groups n_dset_per_group n_attr_per_grp n_mb_per_dset file_path sleep_time\n", exename);
}

double get_elapsed_time_double(struct timeval *tstart, struct timeval *tend)
{
    return (double)(((tend->tv_sec-tstart->tv_sec)*1000000LL + tend->tv_usec-tstart->tv_usec) / 1000000.0);
}

int main(int argc, char *argv[])
{
    int n_grp, n_dset_per_grp, n_attr_per_grp, n_mb_per_dset, i, j, k, sleep_time;
    char *filepath = NULL;
    char filename[128] = {0};
    char grp_name[128];
    char **dset_name;
    char **attr_name;
    herr_t   status;
    hid_t    fid, *dset, *grp, dcpl, sid, attr_sid, *attr, async_fapl, async_dxpl;
    struct timeval  timer0;
    struct timeval  timer1;
    struct timeval  timer2;
    struct timeval  timer3;
    struct timeval  timer4;
    double total_time;

    hsize_t dims[1] = {1};                       /* Dimension sizes */
    hsize_t max_dims[1] = {H5S_UNLIMITED};       /* Maximum dimension sizes */
    hsize_t chunk_dims[1] = {2};                 /* Chunk dimension sizes */
    hsize_t attr_dims[1] = {1};

    int nthread = 1;
    async_fapl  = H5Pcreate (H5P_FILE_ACCESS);
    async_dxpl  = H5Pcreate (H5P_DATASET_XFER);
    H5Pset_vol_async(async_fapl, nthread);
    H5Pset_dxpl_async(async_dxpl, true);

    if (argc < 7) {
        print_usage(argv[0]);
        goto exit;
    }
    else {
        n_grp            = atol(argv[1]);
        n_dset_per_grp   = atoi(argv[2]);
        n_attr_per_grp  = atoi(argv[3]);
        n_mb_per_dset    = atoi(argv[4]);
        filepath         = argv[5];
        sleep_time       = atoi(argv[6]);
        sprintf(filename, "%s/async_%dgrp_%ddsets_%dMB_%dattr.h5", 
                filepath, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp);
    }

    /* int cpu; */
    /* cpu = sched_getcpu(); */
    /* fprintf(stderr, "Benchmark thread is running on CPU %d, thread %u\n", cpu, pthread_self()); */

    printf("Writing to %s, %d groups, %d dset (%dMB) per group, %d attr per group, sleep %d s\n", 
            filename, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp, sleep_time);

    hsize_t nint = n_mb_per_dset * 1048576 / sizeof(int);
    dims[0] = nint;

    sid = H5Screate_simple(1, dims, NULL);

    /* Create the data space for the attribute. */
    attr_sid = H5Screate_simple(1, attr_dims, NULL);

    void *data = malloc(dims[0] * sizeof(int));
    grp  = (hid_t*)calloc(n_grp, sizeof(hid_t));
    dset = (hid_t*)calloc(n_dset_per_grp, sizeof(hid_t));
    attr = (hid_t*)calloc(n_attr_per_grp, sizeof(hid_t)); 

    gettimeofday(&timer1, 0);

    // Create a file
    /* printf("Before file %d create start, time=%ld.%06ld\n", i, timer1.tv_sec, timer1.tv_usec); */
    /* fflush(stdout); */
    fid = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, async_fapl);
    if (fid < 0) {
        printf("Error creating file %s\n", filename);
        goto exit;
    }

    gettimeofday(&timer0, 0);
    total_time = get_elapsed_time_double(&timer1, &timer0);
    printf("File %d create time: %.4f\n", i, total_time);
    /* fflush(stdout); */

    attr_name = (char**)malloc(n_attr_per_grp*sizeof(char*));
    dset_name = (char**)malloc(n_dset_per_grp*sizeof(char*));
    for (k = 0; k < n_attr_per_grp; k++) {
        attr_name[k] = (char*)malloc(32);
        sprintf(attr_name[k], "Attr%d", k);
    }
    for (j = 0; j < n_dset_per_grp; j++) {
        dset_name[j] = (char*)malloc(32);
        sprintf(dset_name[j], "Dset%d", j);
    }

    for (i = 0; i < n_grp; i++) {
        gettimeofday(&timer1, 0);
        /* printf("Before group %d create start, time=%ld.%06ld\n", i, timer1.tv_sec, timer1.tv_usec); */
        /* fflush(stdout); */
        sprintf(grp_name, "Group%d", i);
        if((grp[i] = H5Gcreate(fid, grp_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0)
            printf("Error creating group [%s]\n", grp_name);

        for (k = 0; k < n_attr_per_grp; k++) 
            if((attr[k] = H5Acreate(grp[i], attr_name[k], H5T_NATIVE_INT, attr_sid, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                printf("Error creating attr [%s]\n", attr_name[k]);

        for (j = 0; j < n_dset_per_grp; j++) {
            if((dset[j] = H5Dcreate(grp[i], dset_name[j], H5T_NATIVE_INT, sid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                printf("Error creating dset [%s]\n", dset_name[j]);
        }

        gettimeofday(&timer2, 0);
        total_time = get_elapsed_time_double(&timer1, &timer2);
        printf("Group %d create time: %.4f\n", i, total_time);
        /* printf("Group %d write start\n", i); */
        /* fflush(stdout); */

        for (k = 0; k < n_attr_per_grp; k++) {
            if(H5Awrite(attr[k], H5T_NATIVE_INT, &k) < 0)
                printf("Error writing attr [%s]\n", attr_name[k]);
        }

        for (j = 0; j < n_dset_per_grp; j++) {
            if (nint > 0 && H5Dwrite(dset[j], H5T_NATIVE_INT, H5S_ALL, H5S_ALL, async_dxpl, data) < 0)
                printf("Error writing dset [%s]\n", dset_name[j]);
        }

        gettimeofday(&timer3, 0);
        total_time = get_elapsed_time_double(&timer2, &timer3);
        printf("Group %d write time: %.4f\n", i, total_time);
        /* printf("Group %d close start\n", i); */
        /* fflush(stdout); */

        if (sleep_time > 0) 
            usleep(sleep_time*1000000);

        gettimeofday(&timer3, 0);
        for (j = 0; j < n_dset_per_grp; j++) 
            H5Dclose(dset[j]);

        for (k = 0; k < n_attr_per_grp; k++) 
            H5Aclose(attr[k]);

        H5Gclose(grp[i]);

        gettimeofday(&timer4, 0);
        total_time = get_elapsed_time_double(&timer3, &timer4);
        printf("Group %d close time: %.4f\n", i, total_time);
        /* fflush(stdout); */
    } 

    status = H5Fwait(fid);

    gettimeofday(&timer3, 0);
    total_time = get_elapsed_time_double(&timer0, &timer3);
    printf("Total write time: %.4f\n", total_time);

    for (k = 0; k < n_attr_per_grp; k++) 
        free(attr_name[k]);
    for (j = 0; j < n_dset_per_grp; j++) 
        free(dset_name[j]);
    free(attr_name);
    free(dset_name);
    free(data);
    free(grp);
    free(dset);
    free(attr);

    status = H5Sclose(sid);
    status = H5Sclose(attr_sid);
    status = H5Pclose(async_fapl);
    status = H5Pclose(async_dxpl);
    status = H5Fclose(fid);

exit:
    return 0;
}

