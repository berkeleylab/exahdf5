#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>

#include "mpi.h"
#include "hdf5.h"

#define NDIM 1

void print_usage(const char *exename)
{
    printf("Usage: ./%s n_groups n_dset_per_group n_attr_per_grp file_path sleep_time\n", exename);
}

double get_elapsed_time_double(struct timeval *tstart, struct timeval *tend)
{
    return (double)(((tend->tv_sec-tstart->tv_sec)*1000000LL + tend->tv_usec-tstart->tv_usec) / 1000000.0);
}

int main(int argc, char *argv[])
{
    int proc_num, my_rank;
    int n_grp, n_dset_per_grp, n_attr_per_grp, n_mb_per_dset, i, j, k, sleep_time;
    char *filepath = NULL;
    char filename[128] = {0};
    char grp_name[128];
    char dset_name[128];
    char attr_name[128];
    herr_t status;
    hid_t  fid, dset, grp, dcpl, attr_id, fapl;
    struct timeval  timer1;
    struct timeval  timer2;
    double total_time;

    hsize_t dims[1] = {1};                       /* Dimension sizes */
    int nthread = 1;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    fapl  = H5Pcreate (H5P_FILE_ACCESS);
    H5Pset_fapl_mpio(fapl, MPI_COMM_WORLD, MPI_INFO_NULL);

    if (argc != 7) {
        print_usage(argv[0]);
        goto exit;
    }
    else {
        n_grp            = atol(argv[1]);
        n_dset_per_grp   = atoi(argv[2]);
        n_attr_per_grp   = atoi(argv[3]);
        n_mb_per_dset    = atoi(argv[4]);
        filepath         = argv[5];
        sleep_time       = atoi(argv[6]);
        sprintf(filename, "%s/%dgrp_%ddsets_%dMB_%dattr.h5", 
                filepath, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp);
    }

    if (my_rank == 0) 
        printf("Reading from %s, %d groups, %d dset (%dMB) per group, %d attr per dset\n", 
                filename, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp);

    hsize_t nint = n_mb_per_dset * 1048576 / sizeof(int);
    dims[0] = nint;

    fid = H5Fopen(filename, H5F_ACC_RDONLY, fapl);
    if (fid < 0) {
        printf("Error creating file %s\n", filename);
        goto exit;
    }

    void *data = malloc(dims[0] * sizeof(int));

    gettimeofday(&timer1, 0);
    for (i = 0; i < n_grp; i++) {
        sprintf(grp_name, "Group%d", i);
        if((grp = H5Gopen(fid, grp_name, H5P_DEFAULT)) < 0)
            printf("Error opening group [%s]\n", grp_name);

        for (j = 0; j < n_dset_per_grp; j++) {
            sprintf(dset_name, "Dset%d", j);
            if((dset = H5Dopen(grp, dset_name, H5P_DEFAULT)) < 0)
                printf("Error opening dset [%s]\n", dset_name);

            if (H5Dread(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, data)< 0)
                printf("Error reading dset [%s]\n", dset_name);

            for (k = 0; k < n_attr_per_grp; k++) {
                sprintf(attr_name, "Attr%d", k);
                if((attr_id = H5Aopen(grp, attr_name, H5P_DEFAULT)) < 0)
                    printf("Error opening attr [%s]\n", attr_name);
                if(H5Aread(attr_id, H5T_NATIVE_INT, &k) < 0)
                    printf("Error reading attr [%s]\n", attr_name);
                H5Aclose(attr_id);
            }
            H5Dclose(dset);
        }
        H5Gclose(grp);

        if (sleep_time > 0) 
            usleep(sleep_time*1000000);
        
    } 

    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&timer2, 0);
    total_time = get_elapsed_time_double(&timer1, &timer2);
    if (my_rank == 0) 
        printf("Total read time:%.4f\n", total_time);

    free(data);

    status = H5Pclose(fapl);
    status = H5Fclose(fid);

    MPI_Finalize();
exit:
    return 0;
}

