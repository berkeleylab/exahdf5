#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>

#include "hdf5.h"

#define NDIM 1

void print_usage(const char *exename)
{
    printf("Usage: ./%s n_groups n_dset_per_group n_mb_per_dset n_attr_per_grp n_byte_per_attr file_path\n", exename);
}

double get_elapsed_time_double(struct timeval *tstart, struct timeval *tend)
{
    return (double)(((tend->tv_sec-tstart->tv_sec)*1000000LL + tend->tv_usec-tstart->tv_usec) / 1000000.0);
}

int main(int argc, char *argv[])
{
    int n_grp, n_dset_per_grp, n_attr_per_grp, n_mb_per_dset, n_byte_per_attr, i, j, k, sleep_time;
    char *filepath = NULL;
    char filename[128] = {0};
    char grp_name[128];
    char dset_name[128];
    char attr_name[128];
    herr_t   status;
    hid_t    fid, dset, grp, dcpl, sid, attr_sid, attr_id, fapl, dxpl;
    struct timeval  timer1;
    struct timeval  timer2;
    double total_time;

    hsize_t dims[1] = {1};                       /* Dimension sizes */
    hsize_t attr_dims[1] = {1};

    int nthread = 1;
    fapl  = H5Pcreate (H5P_FILE_ACCESS);

    if (argc < 8) {
        print_usage(argv[0]);
        goto exit;
    }
    else {
        n_grp            = atol(argv[1]);
        n_dset_per_grp   = atoi(argv[2]);
        n_mb_per_dset    = atoi(argv[3]);
        n_attr_per_grp   = atoi(argv[4]);
        n_byte_per_attr  = atoi(argv[5]);
        filepath         = argv[6];
        sleep_time       = atoi(argv[7]);
        sprintf(filename, "%s/%dgrp_%ddsets_%dMB_%dattr_%dB.h5", 
                filepath, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp, n_byte_per_attr);
    }

    printf("Writing to %s, %d groups, %d dset (%dMB) per group, %d attr (%dB) per dset\n", 
            filename, n_grp, n_dset_per_grp, n_mb_per_dset, n_attr_per_grp, n_byte_per_attr);

    hsize_t nint = n_mb_per_dset * 1048576 / sizeof(int);
    dims[0] = nint;

    sid = H5Screate_simple(1, dims, NULL);

    attr_dims[0] = n_byte_per_attr / sizeof(int);
    attr_sid = H5Screate_simple(1, attr_dims, NULL);

    fid = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
    if (fid < 0) {
        printf("Error creating file %s\n", filename);
        goto exit;
    }

    int *data = malloc(dims[0] * sizeof(int));
    for (i = 0; i < nint; i++) 
        data[i] = i;

    gettimeofday(&timer1, 0);
    for (i = 0; i < n_grp; i++) {
        sprintf(grp_name, "Group%d", i);
        if((grp = H5Gcreate(fid, grp_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0)
            printf("Error creating group [%s]\n", grp_name);

        for (j = 0; j < n_dset_per_grp; j++) {
            sprintf(dset_name, "Dset%d", j);
            if((dset = H5Dcreate(grp, dset_name, H5T_NATIVE_INT, sid, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                printf("Error creating dset [%s]\n", dset_name);

            if (H5Dwrite(dset, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, (void*)data)< 0)
                printf("Error writing dset [%s]\n", dset_name);
            H5Dclose(dset);
        }

        for (k = 0; k < n_attr_per_grp; k++) {
            sprintf(attr_name, "Attr%d", k);
            if((attr_id = H5Acreate2(grp, attr_name, H5T_NATIVE_INT, attr_sid, H5P_DEFAULT, H5P_DEFAULT)) < 0)
                printf("Error creating attr [%s]\n", attr_name);
            if(H5Awrite(attr_id, H5T_NATIVE_INT, (void*)data) < 0)
                printf("Error writing attr [%s]\n", attr_name);
            H5Aclose(attr_id);
        }

        H5Gclose(grp);
        /* H5Fflush(fid, H5F_SCOPE_GLOBAL); */

        if (sleep_time > 0) sleep(sleep_time);
    } 
    gettimeofday(&timer2, 0);
    total_time = get_elapsed_time_double(&timer1, &timer2);
    printf("Total write time:%.4f\n", total_time);

    free(data);

    status = H5Sclose(sid);
    status = H5Sclose(attr_sid);
    status = H5Pclose(fapl);
    status = H5Fclose(fid);

exit:
    return 0;
}

